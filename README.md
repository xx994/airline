# Java Swing-航空登机系统

#### 一、介绍

软件工程G34项目
本系统为使用java swing开发的航空登机系统，界面美观使用。

#### 二、运行环境
Java JDK版本：15.0.2

#### 三、使用教程
用户登机部分请直接运行**src\frame**目录下的Init文件
管理员部分请运行**src\frame**目录下的Init_Admin文件

**用户模块使用流程**
首先请你点击continue并输入您的体温(35.5-37-3度之间为正常）

您有3种方式可以登录
1. 第一个按钮 INPUT THE BOOKING NUMBER:
>输入1326854870743后点击continue，该订单有4个航班

>或者输入1326854870748后点击continue，该订单有2个航班

>或者输入1326854870746后点击continue，该订单有1个航班

2. 第二个按钮 ENTER THE ID NUMBER:
>输入360124200102040017后点击continue，输入WU，然后再次确认，该订单有4个航班

>或者输入430102197606046442后点击continue，输入QIU，然后再次确认，该订单有3个航班

>或者输入522323198705037737点击continue，输入DU，然后再次确认，该订单有1个航班


3. 第三个按钮SCAN THE ID DOCUMENT，直接点击continue，该订单有4个航班

**注：选择航班时，航班数大于2可以点击右上角的下一页进行切换**

**注：已经完成全部流程的航班将设为已登机状态，再次check in会提示用户禁止操作，如：第三个按钮的第一项航班**


4.  选择任意航班后点击右下角的continue
5.  选择任意座位和食物，最后将显示您的所有选择
6.  在确认界面点击continue，进入支付界面随后继续选择下一步
7.  此时您已经登机成功，也可以在管理员面板找到您的登机记录

**管理员模块使用流程**
1. 运行src\frame目录下的Init_Admin文件
2. 输入账号123456后点击COUNTINUE，然后输入密码000000后再次确认
3. 点击航班左侧的按钮来选择要查看的航班，随后点击COUNTINUE来查看
4. 或者在上方搜索栏直接搜索，点击CONFIRM后点击COUNTINUE来查看

#### 四、功能实现


| 序号  | 已经实现的功能          |
|-----|------------------|
| 1   | 输入订单号进行check in  |
| 2   | 输入ID和姓进行check in |
| 3   | 扫描ID证件进行check in |
| 4   | 选择要check in的指定订单 |
| 5   | 显示所有航班摘要         |
| 6   | 选择您的座位           |
| 7   | 选择您的食物           |
| 8   | 确认您的登机信息         |
| 9   | 管理员查看登机乘客信息      |
| 10  | 支付额外费用           |
| 11  | 打印登机牌            |
| 12  | 展示行李标签使用方式       |
| 13  | 登录管理员账户          |
| 14  | 乘客添加备注信息         |
| 15  | 管理员查看备注信息        |
| 16  | 判断体温信息           |
| 17  | 判断VIP价格          |
| 18  | 对VIP用户进行折扣       |



#### 五、部分图片展示

![起始页](resource/image.png)

![登录页](resource/image4.png)

![另一个登录](resource/image7.png)

![航班选择](resource/image1.png)

![座位选择](resource/image2.png)

![打印登机牌](resource/image5.png)

![乘客信息](resource/image6.png)