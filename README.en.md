# Java Swing - Airline Boarding System

#### 1. Introduction

Software engineering G34 project
This system is an aviation boarding system developed by Java swing, with beautiful interface.

#### 2. Operating environment

Java JDK version: 15.0.2
#### 3. Using tutorials

1. For the user boarding part, Please run the **init.java** file under **src\frame** directory.
2. For the administrator part, please run **init_Admin.java** file under **src\frame** directory.

**USER MODULE USAGE PROCESS**

First, please click continue and enter your body temperature. (normal between 35.5-37-3 degrees)

You have three ways to log in
1. The first button _INPUT THE BOOKING NUMBER_:
>Enter 1326854870743 and click continue. The order has 4 flights.

>Or enter 1326854870748 and click continue. The order has two flights.

>Or enter 1326854870746 and click continue. The order has one flight.

2. The second button _ENTER THE ID NUMBER_:
>Enter 360124200102040017, click continue, enter Wu, and then confirm again. The order has four flights.

>Or enter 430102197606046442, click continue, enter Qiu, and then confirm again. The order has three flights.

>Or enter 522323198705037737, click continue, enter Du, and then confirm again that the order has one flight.

3. The third button _SCAN THE ID DOCUMENT_: Click continue directly. The order has four flights.

**Note: when selecting a flight, if the number of flights is greater than 2, you can click the next page in the upper right corner to switch.**

**Note: the flight that has completed all processes will be set to the boarding status, and check in again will prompt the user to prohibit operations, such as the first flight of the third button.**


4. Select any flight and click _continue_ in the lower right corner.
5. Choose any seat and food, and all your choices will be displayed at the end.
6. Click _continue_ in the confirmation interface to enter the payment interface, and then continue to select next step.
7. Now that you have successfully boarded, you can also find your boarding record in the administrator panel.

**ADMINISTRATOR MODULE USAGE PROCESS**
1. Run **init_Admin.java** file under **src\frame** directory.
2. Enter the account number **_123456_**, click count, then enter the password **_000000_** and confirm again
3. Click the button on the left of the flight to select the flight to view, and then click count to view it
4. Or search directly in the upper search bar, click confirm and then click count to view

#### 4. Function realization


| Number | Functions Implemented                                       |
|--------|-------------------------------------------------------------|
| 1      | Scan ID certificate for check in                            |
| 2      | enter ID and last name to check in                          |
| 3      | Scan ID certificate for check in                            |
| 4      | select the specified order to check in                      |
| 5      | display all flight summaries                                |
| 6      | choose your seat                                            |
| 7      | choose your food                                            |
| 8      | confirm your boarding information                           |
| 9      | the administrator checks the boarding passenger information |
| 10     | payment of additional fees                                  |
| 11     | print boarding pass                                         |
| 12     | show how baggage tags are used                              |
| 13     | login to administrator account                              |
| 14     | passengers add remarks                                      |
| 15     | the administrator views the remarks                         |
| 16     | judging body temperature information                        |
| 17     | judge VIP price                                             |
| 18     | discount for VIP users                                      |



#### 5. Partial picture display

![home page](resource/image.png)

![Login page](resource/image4.png)

![Another login](resource/image7.png)

![Flight selection](resource/image1.png)

![Seat selection](resource/image2.png)

![Print boarding pass](resource/image5.png)

![Passenger information](resource/image6.png)