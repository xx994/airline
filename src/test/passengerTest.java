package test;

import org.junit.Test;

import model.passenger;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
/**
 * Title             : passengerTest.java
 * Description : This is a test class.
 */
public class passengerTest {
    passenger model;

    /**
     * setup bufore test
     */
    @Before
    public void setup(){
        model = new passenger();
    }

    /**
     * a test
     */
    @Test
    public void CheckRightID1(){
        assertEquals(1,model.isPassenger("360124200102040017"));
    }

    /**
     * a test
     */
    @Test
    public void CheckRightID2(){
        assertEquals(1,model.isPassenger("430102197606046442"));
    }

    /**
     * a test
     */
    @Test
    public void CheckRightID3(){
        assertEquals(1,model.isPassenger("360124200102040017"));
    }

    /**
     * a test
     */
    @Test
    public void CheckWrongID1(){
        assertEquals(0,model.isPassenger("522323198705037738"));
    }

    /**
     * a test
     */
    @Test
    public void CheckWrongID2(){
        assertEquals(0,model.isPassenger("52c3d1qfq8"));
    }

    /**
     * tear up after test
     */
    @After
    public void tearup(){
        model = null;
    }
}
