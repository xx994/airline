package model;
import tools.*;
import java.io.*;
//We use the file name of "flight number. TXT" to create a TXT file for storing flight information, such as "ba0570. TXT".
//eachFlight class enables us to view the information in the flight file and change the remaining number of seats on the flight
/**
 * Title             : eachFlight.java
 * Description : This class is all methods for reading and modifying the information of each flight,
 * such as boardingTime,COMPANY,GATE,seat and so on.
 */
public class eachFlight {
    /**
     * This method is used to read the boarding time of flight.
     * @param content This is the flight number of the flight chosen to read information
     */
    public static String boardingTime(String content){//Add the "flight number" to be queried in parentheses, and this function returns the boarding time of the flight
        String boarding_time=null;
        lineReader lr = new lineReader();
        File file = new File("resource/"+content+".txt");
        if(file.exists()) {
          String boardingTime_line = lr.readLine("resource/" + content + ".txt", "BOARDINGTIME");
           String[] sp = boardingTime_line.split("\\s+");
           boarding_time=sp[1];
        }else{
            System.out.println("There is no flight called "+content);
        }
        return boarding_time;
    }
    /**
     * This method is used to know the name of company of the flight.
     * @param content This is the flight number of the flight chosen to read information
     */
    public static String COMPANY(String content){//Add the "flight number" to be queried in parentheses, and the function returns the airline to which the flight belongs
        String COMPANY="";
        lineReader lr = new lineReader();
        File file = new File("resource/"+content+".txt");
        if(file.exists()) {
            String COMPANY_line = lr.readLine("resource/" + content + ".txt", "COMPANY");
            String[] sp = COMPANY_line.split("\\s+");
            COMPANY=sp[1];
            int i = 2;
            while(i<sp.length) {
                COMPANY = COMPANY + " "+sp[i];
                i = i + 1;
            }
        }else{
            System.out.println("There is no flight called "+content);
        }
        return COMPANY;
    }
    /**
     * This method is used to know the gate number of the flight.
     * @param content This is the flight number of the flight chosen to read information
     */
    public static String GATE(String content){//Add the "flight number" to be queried in parentheses, and this function returns the boarding slogan of the flight
        String GATE_number=null;
        lineReader lr = new lineReader();
        File file = new File("resource/"+content+".txt");
        if(file.exists()) {
            String GATE_line = lr.readLine("resource/" + content + ".txt", "GATE");
            String[] sp = GATE_line.split("\\s+");
            GATE_number=sp[1];
        }else{
            System.out.println("There is no flight called "+content);
        }
        return GATE_number;
    }
    /**
     * This method is used to know the name of the image of the flight's destination.
     * @param content This is the flight number of the flight chosen to read information
     */
    public static String IMG(String content){//Add the "flight number" to be queried in parentheses, and this function returns the "picture name" of the arrival destination of the flight.

        String img="";
        lineReader lr = new lineReader();
        File file = new File("resource/"+content+".txt");
        if(file.exists()) {
            String img_line = lr.readLine("resource/" + content + ".txt", "IMG");
            String[] sp = img_line.split("\\s+");
            img=sp[1];
            int i = 2;
            while(i<sp.length) {
                img = img + " "+sp[i];
                i = i + 1;
            }
        }else{
            System.out.println("There is no flight called "+content);
        }
        return img;
    }
    /**
     * This method is used to know the departure of the flight.
     * @param content This is the departure of the flight chosen to read information
     */
    public static String DEPARTURE(String content){//Add the "flight number" to be queried in parentheses, and this function returns the origin of the flight
        String DEPARTURE=null;
        lineReader lr = new lineReader();
        File file = new File("resource/"+content+".txt");
        if(file.exists()) {
            String DEPARTURE_line = lr.readLine("resource/" + content + ".txt", "DEPARTURE");
            String[] sp = DEPARTURE_line.split("\\s+");
            DEPARTURE=sp[1];
        }else{
            System.out.println("There is no flight called "+content);
        }
        return DEPARTURE;
    }
    /**
     * This method is used to know the destination of the flight.
     * @param content This is the destination of the flight chosen to read information
     */
    public static String DESTINATION(String content){//Add the "flight number" to be queried in parentheses, and this function returns the destination of the flight
        String DESTINATION=null;
        lineReader lr = new lineReader();
        File file = new File("resource/"+content+".txt");
        if(file.exists()) {
            String DESTINATION_line = lr.readLine("resource/" + content + ".txt", "DESTINATION");
            String[] sp = DESTINATION_line.split("\\s+");
            DESTINATION=sp[1];
        }else{
            System.out.println("There is no flight called "+content);
        }
        return DESTINATION;
    }
    /**
     * This method is used to know the departs time of the flight.
     * @param content This is the departs time of the flight chosen to read information
     */
    public static String DEPARTS(String content){//Add the "flight number" to be queried in parentheses, and this function returns the departure time of the flight
        String DEPARTS=null;
        lineReader lr = new lineReader();
        File file = new File("resource/"+content+".txt");
        if(file.exists()) {
            String DEPARTS_line = lr.readLine("resource/" + content + ".txt", "DEPARTS");
            String[] sp = DEPARTS_line.split("\\s+");
            DEPARTS=sp[1];
        }else{
            System.out.println("There is no flight called "+content);
        }
        return DEPARTS;
    }
    /**
     * This method is used to know the arrives time of the flight.
     * @param content This is the arrives time of the flight chosen to read information
     */
    public static String ARRIVES(String content){//Add the "flight number" to be queried in parentheses, and this function returns the arrival time of the flight
        String ARRIVES=null;
        lineReader lr = new lineReader();
        File file = new File("resource/"+content+".txt");
        if(file.exists()) {
            String ARRIVES_line = lr.readLine("resource/" + content + ".txt", "ARRIVES");
            String[] sp = ARRIVES_line.split("\\s+");
            ARRIVES=sp[1];
        }else{
            System.out.println("There is no flight called "+content);
        }
        return ARRIVES;
    }
    /**
     * This method is used to know the delay state of the flight.
     * @param content This is the delay state of the flight chosen to read information
     */
    public static String DELAY(String content){//Add the "flight number" to be queried in parentheses, and the function returns a value. If it is 0, it means the flight takes off late; if it is 1, it means the flight takes off normally
        String DELAY=null;
        lineReader lr = new lineReader();
        File file = new File("resource/"+content+".txt");
        if(file.exists()) {
            String DELAY_line = lr.readLine("resource/" + content + ".txt", "DELAY");
            String[] sp = DELAY_line.split("\\s+");
            DELAY=sp[1];
        }else{
            System.out.println("There is no flight called "+content);
        }
        return DELAY;
    }


    /**
     * This function can check whether the selected seat has been selected, and add the number of seat columns (numbers 01-20) of "flight number", "seat rows (letters seata-seatf)" to be queried in brackets“
     * If the returned value is 1, it means that the seat is empty and can be selected. If the returned value is 0, it means that the action has been selected and can no longer be selected.
     * @param content This is the flight number of the flight chosen to read information
     * @param letter This is the seat letter
     * @param number This is the seat number
     */
    public static int seat(String content,String letter,String number){
        int judgement = 0;
        lineReader lr = new lineReader();
        File file = new File("resource/"+content+".txt");
        if(file.exists()) {
            String seat_line = lr.readLine("resource/" + content + ".txt", letter);
            String[] sp = seat_line.split("\\s+");
            int i=0;
            while(i<sp.length){
                if(sp[i].equals(number)){
                    judgement = 1;
                    break;
                }
                i=i+1;
            }
        }else{
            System.out.println("There is no flight called "+content);
        }
        return judgement;
    }

    /**
     * This function can be used to select seats. Add the number of seat columns (numbers 01-20) of the selected "flight number" and "seat rows (letters seata-seatf)" to the variables in brackets“     * If the returned value is 1, it means that the seat is empty and can be selected. If the returned value is 0, it means that the action has been selected and can no longer be selected.
     * After the seat is selected, the data "01-20" in the text will be replaced with "chosen"
     * @param content This is the flight number of the flight chosen to read information
     * @param letter This is the seat letter
     * @param number This is the seat number
     */
    public static void seat_choose(String content,String letter,String number){
        lineReader lr = new lineReader();
        lineWriter lw = new lineWriter();
        int lineNum=lr.findLine("resource/"+content+".txt",letter);
        String reLine = lw.lineRewrite("resource/"+content+".txt",lineNum,number,"chosen");
        lw.writeLine("resource/"+content+".txt",lineNum,reLine);
    }
}