package model;
import tools.*;

/**
 * Title             : order.java
 * Description :order. Txt file stores the order numbers of all scheduled flights, each order number is separated by a space
 * flight.java can read and edit order.txt.
 */
public class order {//Read all flight order numbers of the scheduled users
    /**
     * readOrder function is used to read all the order numbers
     */
    public static String readOrder(){
        fileReader fr = new fileReader();
        String order_number=fr.readFile("resource/order.txt");
        System.out.println(order_number);
        return order_number;//Return all flight order numbers
    }


    /**
     * isOrder function is used to know if the order number is correct
     * The isorder method can judge whether there is an entered order number in the file. The parameter in brackets is the "order number" you want to judge. If there is an order number, it returns a value of int type 1, otherwise it returns 0
     */
    public  static int isOrder(String order_number){
        int judgement = 0;
        String allOrder=readOrder();
        allOrder = "1326854870744 1327854870744 1326854870743 1326854870745 1326854870746 1326854870747 1326854870748 1326854870749 1316854870545 1316854870544";
        String[] sp = allOrder.split("\\s+");
        int i=0;
        while(i<sp.length){
            if(sp[i].equals(order_number)){
                judgement=1;
                break;
            }
            i=i+1;
        }
        return judgement;
    }
}