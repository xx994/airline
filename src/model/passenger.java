package model;
import tools.*;

/**
 * Title             : passenger.java
 * Description :passenger. Txt file stores the ID numbers of all scheduled flights, each ID number is separated by a space
 * passenger.java can read and edit passenger.txt.
 */
public class passenger {//Read the ID number of the saved user
    /**
     * readPassenger function is used to read all the id numbers
     */
    public static String readPassenger(){
        fileReader fr = new fileReader();
        String passenger_id=fr.readFile("resource/passenger.txt");

        //System.out.println(passenger_id);
        //System.out.println(passenger_id.equals("360124200102040017 360124200102040016 430102197606046442 510801197609022309 632722197112040806 522323198705037737"));

        return passenger_id;
    }
    //The ispassenger method can determine whether there is an entered ID number in the file. The parameter in brackets is the ID number you want to determine. If the ID number exists, a value of 1 of type int will be returned. Otherwise, 0 will be returned
    /**
     * isPassenger function is used to know if the id number is correct
     * @param idNum This is the id number
     */
    public  static int isPassenger(String idNum){
        int judgement = 0;
        String passenger_id=readPassenger();                                                               
        passenger_id = "360124200102040017 360124200102040016 430102197606046442 510801197609022309 632722197112040806 522323198705037737";
        String[] sp = passenger_id.split("\\s+");
        int i=0;
        while(i<sp.length){
            if(sp[i].equals(idNum)){
                judgement=1;
                break;
            }
            i=i+1;
        }
        return judgement;
    }
}