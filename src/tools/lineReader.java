package tools;
import java.io.*;

/**
 * Title             : lineReader.java
 * Description : Tools in the linereader class can help read the contents of a specific line in a single text
 */
public class lineReader{

    /**
     * The readLine function can help read the content of a specific line in the text. Add "text path" and "query keyword" in parentheses. This function returns all the "information" of the line where the keyword is located
     * @param txtPath this is path to read file
     * @param idNum this is the content you enter to find the keyword's position
     */
    public static String readLine(String txtPath,String idNum){
        String result=null;
        try{
            FileReader fr =new FileReader(new File(txtPath));
            BufferedReader br = new BufferedReader(fr);
            String line = br.readLine();
            while(line!=null){
                if(line.contains(idNum)){
                    result=line;
                    break;
                }else{
                    line=br.readLine();
                }
            }
        }catch (FileNotFoundException e){
            e.printStackTrace();
        }catch (IOException e){
            e.printStackTrace();
        }
        return result;
    }


    /**
     * readLine_ by_ The number function can read the content of the specific number of lines in the text, add "text path" and "number of lines of query content" in parentheses, and the function returns all the "information" of the line where the keyword is located
     * @param txtPath this is path to read file
     * @param lineNum which line
     */
    public static String readLine_by_number(String txtPath,int lineNum){
        String result=null;
        try{
            FileReader fr =new FileReader(new File(txtPath));
            BufferedReader br = new BufferedReader(fr);
            String line = br.readLine();
            int i = 1;
            while(line!=null){
                if(i==lineNum){
                    result=line;
                    break;
                }else{
                    line=br.readLine();
                    i=i+1;
                }
            }
        }catch (FileNotFoundException e){
            e.printStackTrace();
        }catch (IOException e){
            e.printStackTrace();
        }
        return result;
    }


    /**
     * The findline function can help lock the number of lines where a certain information is located. Add "file path" and "query keyword" in parentheses. This function returns the "number of lines" where the keyword is located
     * @param txtPath this is path to read file
     * @param idNum the special information
     */
    public static int findLine(String txtPath,String idNum){
        int num=1;
        try{
            FileReader fr =new FileReader(new File(txtPath));
            BufferedReader br = new BufferedReader(fr);
            String line = br.readLine();
            while(line!=null){
                if(line.contains(idNum)){
                    break;
                }else{
                    num=num+1;
                    line=br.readLine();
                }
            }
        }catch (FileNotFoundException e){
            e.printStackTrace();
        }catch (IOException e){
            e.printStackTrace();
        }
        return num;
    }
}