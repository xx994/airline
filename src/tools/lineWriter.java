package tools;
import java.io.*;

/**
 * Title             : lineWriter.java
 * Description : The tools in linewriter can help us make changes to the contents of a specific line in a single file
 */
public class lineWriter {

    /**
     * The writeline function can help us modify the information. Add "text path", "which line of data you want to modify" and "new content" in brackets
     * @param path this is path to read file
     * @param lineNo which line
     * @param newStr new content
     */
        public static void writeLine(String path,int lineNo,String newStr) {
            String temp = "";
            try {
                File file = new File(path);
                FileInputStream fis = new FileInputStream(file);
                InputStreamReader isr = new InputStreamReader(fis);
                BufferedReader br = new BufferedReader(isr);
                StringBuffer buf = new StringBuffer();

                // Save the contents before the line
                for (int j = 1; (temp = br.readLine()) != null; j++) {
                    if (j == lineNo) {
                        buf = buf.append(newStr);
                    } else {
                        buf = buf.append(temp);
                    }
                    buf = buf.append(System.getProperty("line.separator"));
                }

                br.close();
                FileOutputStream fos = new FileOutputStream(file);
                PrintWriter pw = new PrintWriter(fos);
                pw.write(buf.toString().toCharArray());
                pw.flush();
                pw.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    /**
     * The linerewrite function can help us to modify a specified data of a certain line. Add the parameter "text path" in parentheses to rewrite the content, and "old content" and "new content" in the first few lines
     * @param path this is path to read file
     * @param lineNum which line
     * @param content old content
     * @param new_content new content
     */
        public static String lineRewrite(String path,int lineNum,String content,String new_content){
            lineReader lr = new lineReader();
    String t1 = lr.readLine_by_number(path,lineNum);
    String[]t2 = t1.split("\\s+");
    int i=0;
    while(i<t2.length){
        if(t2[i].equals(content)){
            t2[i]=new_content;
            break;
        }
        i=i+1;
    }
    String t3=t2[0];
    int j=1;
    while(j<t2.length){
        t3=t3+" "+t2[j];
        j=j+1;
    }
    return t3;
}
    }
