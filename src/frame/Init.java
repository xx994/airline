package frame;

import frame.Main_Frame;

import java.awt.*;
/**
 * Title             : Init.java
 * Description : This class is the startup class for launching the user interface.
 */
public class Init {
    public static CardLayout cardLayout = new CardLayout();

    public CardLayout getCardLayout()
    {
        return cardLayout;
    }

    public static void main(String[] args) {

        new Main_Frame().buildMainFrame(cardLayout);
        System.out.println("Please input ID!");
    }

}




