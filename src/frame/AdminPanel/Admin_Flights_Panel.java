package frame.AdminPanel;

import utils.BackGroundImagePanle;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
/**
 * Title             : Admin_Flights_Panel.java
 * Description : This class build the panel for administer to choose flight.
 */
public class Admin_Flights_Panel extends JPanel {

    public  static String flight_num="";

    public  Admin_Flights_Panel(){
        //Set background color
        setBackground(new Color(72,46,115));
        //Set initial panel
        JPanel panel = new JPanel();
        panel.setBackground(new Color(72,46,115));
        panel.setPreferredSize(new Dimension(1200,680));
        panel.setLayout(null);
        add(panel);

        JTextField InputArea = new JTextField("", 30);
        //Appearance design
        InputArea.setBounds(450,60,300,50);
        InputArea.setFont(new Font (Font.DIALOG,Font.BOLD, 40));
        InputArea.setHorizontalAlignment(JTextField.CENTER);
        InputArea.setForeground(Color.WHITE);
        InputArea.setBackground(new Color(72,46,115));
        InputArea.setEditable(true);
        InputArea.setBorder(BorderFactory.createLineBorder(Color.WHITE, 2));


        JLabel Head = new JLabel("SEARCH OR SELECT FLIGHTS");
        Head.setBounds(184,0,816,50);
        //Appearance design
        Head.setFont(new Font(Font.DIALOG,Font.BOLD,29));//设置文字字体
        Head.setForeground(Color.white);//设置文字的颜色
        Head.setOpaque(true);
        Head.setBackground(new Color(72,46,115));
        Head.setPreferredSize(new Dimension(800,80));
        Head.setHorizontalAlignment(JLabel.CENTER);

        panel.add(Head);
        panel.add(InputArea);

        JPanel middlePanel = new JPanel();
        middlePanel.setBounds(50,120,1100,500);

        GridBagLayout gbl = new GridBagLayout();
        GridBagConstraints gbc = new GridBagConstraints();
        middlePanel.setLayout(gbl);
        gbc.fill=GridBagConstraints.BOTH;

        JLabel information11 = new JLabel();
        information11.setPreferredSize(new Dimension(110,100));
        information11.setOpaque(true);
        information11.setBackground(new Color(232, 232, 232));
        gbc.weightx=0.1;    // Specify the assignment area of the component
        gbc.weighty=0.2;
        gbc.gridwidth=1;
        gbc.gridheight=1;
        gbc.gridx=0;
        gbc.gridy=0;
        gbl.setConstraints(information11,gbc);
        middlePanel.add(information11);
        JLabel information12 = new JLabel("Flights",SwingConstants.CENTER);
        information12.setPreferredSize(new Dimension(220,100));
        information12.setFont(new Font(Font.DIALOG,Font.PLAIN,28));//Set text font
        information12.setForeground(Color.black);//Sets the color of the text
        information12.setOpaque(true);
        information12.setBackground(new Color(232, 232, 232));
        gbc.weightx=0.2;    // Specify the assignment area of the component
        gbc.weighty=0.2;
        gbc.gridwidth=1;
        gbc.gridheight=1;
        gbc.gridx=1;
        gbc.gridy=0;
        gbl.setConstraints(information12,gbc);
        middlePanel.add(information12);
        JLabel information13 = new JLabel("Destination",SwingConstants.CENTER);
        information13.setPreferredSize(new Dimension(220,100));
        information13.setFont(new Font(Font.DIALOG,Font.PLAIN,28));//Set text font
        information13.setForeground(Color.black);//Sets the color of the text
        information13.setOpaque(true);
        information13.setBackground(new Color(232, 232, 232));
        gbc.weightx=0.2;    // Specify the assignment area of the component
        gbc.weighty=0.2;
        gbc.gridwidth=1;
        gbc.gridheight=1;
        gbc.gridx=2;
        gbc.gridy=0;
        gbl.setConstraints(information13,gbc);
        middlePanel.add(information13);
        JLabel information14 = new JLabel("Departs",SwingConstants.CENTER);
        information14.setPreferredSize(new Dimension(220,100));
        information14.setFont(new Font(Font.DIALOG,Font.PLAIN,28));//Set text font
        information14.setForeground(Color.black);//Sets the color of the text
        information14.setOpaque(true);
        information14.setBackground(new Color(232, 232, 232));
        gbc.weightx=0.2;    // Specify the assignment area of the component
        gbc.weighty=0.2;
        gbc.gridwidth=1;
        gbc.gridheight=1;
        gbc.gridx=3;
        gbc.gridy=0;
        gbl.setConstraints(information14,gbc);
        middlePanel.add(information14);
        JLabel information15 = new JLabel("State",SwingConstants.CENTER);
        information15.setPreferredSize(new Dimension(330,100));
        information15.setFont(new Font(Font.DIALOG,Font.PLAIN,28));//Set text font
        information15.setForeground(Color.black);//Sets the color of the text
        information15.setOpaque(true);
        information15.setBackground(new Color(232, 232, 232));
        gbc.weightx=0.3;    // Specify the assignment area of the component
        gbc.weighty=0.2;
        gbc.gridwidth=1;
        gbc.gridheight=1;
        gbc.gridx=4;
        gbc.gridy=0;
        gbl.setConstraints(information15,gbc);
        middlePanel.add(information15);

        JLabel information21 = new JLabel();
        information21.setPreferredSize(new Dimension(110,100));
        information21.setOpaque(true);
        information21.setBackground(new Color(252, 250, 250));
        JButton button1 = new JButton();
        button1.setSize(110,100);
        button1.setBackground(new Color(96, 62, 151));
        button1.setVisible(true);
        information21.add(button1);
        gbc.weightx=0.1;    // Specify the assignment area of the component
        gbc.weighty=0.2;
        gbc.gridwidth=1;
        gbc.gridheight=1;
        gbc.gridx=0;
        gbc.gridy=1;
        gbl.setConstraints(information21,gbc);
        middlePanel.add(information21);
        JLabel information22 = new JLabel("BA0342",SwingConstants.CENTER);
        information22.setPreferredSize(new Dimension(220,100));
        information22.setFont(new Font(Font.DIALOG,Font.PLAIN,28));//Set text font
        information22.setForeground(Color.black);//Sets the color of the text
        information22.setOpaque(true);
        information22.setBackground(new Color(252, 250, 250));
        gbc.weightx=0.2;    // Specify the assignment area of the component
        gbc.weighty=0.2;
        gbc.gridwidth=1;
        gbc.gridheight=1;
        gbc.gridx=1;
        gbc.gridy=1;
        gbl.setConstraints(information22,gbc);
        middlePanel.add(information22);
        JLabel information23 = new JLabel("Milan",SwingConstants.CENTER);
        information23.setPreferredSize(new Dimension(220,100));
        information23.setFont(new Font(Font.DIALOG,Font.PLAIN,28));
        information23.setForeground(Color.black);
        information23.setOpaque(true);
        information23.setBackground(new Color(252, 250, 250));
        gbc.weightx=0.2;
        gbc.weighty=0.2;
        gbc.gridwidth=1;
        gbc.gridheight=1;
        gbc.gridx=2;
        gbc.gridy=1;
        gbl.setConstraints(information23,gbc);
        middlePanel.add(information23);
        JLabel information24 = new JLabel("7:00AM",SwingConstants.CENTER);
        information24.setPreferredSize(new Dimension(220,100));
        information24.setFont(new Font(Font.DIALOG,Font.PLAIN,28));
        information24.setForeground(Color.black);
        information24.setOpaque(true);
        information24.setBackground(new Color(252, 250, 250));
        gbc.weightx=0.2;
        gbc.weighty=0.2;
        gbc.gridwidth=1;
        gbc.gridheight=1;
        gbc.gridx=3;
        gbc.gridy=1;
        gbl.setConstraints(information24,gbc);
        middlePanel.add(information24);
        JLabel information25 = new JLabel("DELAYED",SwingConstants.CENTER);
        information25.setPreferredSize(new Dimension(330,100));
        information25.setFont(new Font(Font.DIALOG,Font.PLAIN,28));
        information25.setForeground(Color.black);
        information25.setOpaque(true);
        information25.setBackground(new Color(252, 250, 250));
        gbc.weightx=0.3;
        gbc.weighty=0.2;
        gbc.gridwidth=1;
        gbc.gridheight=1;
        gbc.gridx=4;
        gbc.gridy=1;
        gbl.setConstraints(information25,gbc);
        middlePanel.add(information25);

        JLabel information31 = new JLabel();
        information31.setPreferredSize(new Dimension(110,100));
        information31.setOpaque(true);
        information31.setBackground(new Color(232, 232, 232));
        JButton button2 = new JButton();
        button2.setSize(110,100);
        button2.setBackground(new Color(96, 62, 151));
        button2.setVisible(true);
        information31.add(button2);
        gbc.weightx=0.1;
        gbc.weighty=0.2;
        gbc.gridwidth=1;
        gbc.gridheight=1;
        gbc.gridx=0;
        gbc.gridy=2;
        gbl.setConstraints(information31,gbc);
        middlePanel.add(information31);
        JLabel information32 = new JLabel("BA0570",SwingConstants.CENTER);
        information32.setPreferredSize(new Dimension(220,100));
        information32.setFont(new Font(Font.DIALOG,Font.PLAIN,28));
        information32.setForeground(Color.black);
        information32.setOpaque(true);
        information32.setBackground(new Color(232, 232, 232));
        gbc.weightx=0.2;
        gbc.weighty=0.2;
        gbc.gridwidth=1;
        gbc.gridheight=1;
        gbc.gridx=1;
        gbc.gridy=2;
        gbl.setConstraints(information32,gbc);
        middlePanel.add(information32);
        JLabel information33 = new JLabel("Milan",SwingConstants.CENTER);
        information33.setPreferredSize(new Dimension(220,100));
        information33.setFont(new Font(Font.DIALOG,Font.PLAIN,28));
        information33.setForeground(Color.black);
        information33.setOpaque(true);
        information33.setBackground(new Color(232, 232, 232));
        gbc.weightx=0.2;
        gbc.weighty=0.2;
        gbc.gridwidth=1;
        gbc.gridheight=1;
        gbc.gridx=2;
        gbc.gridy=2;
        gbl.setConstraints(information33,gbc);
        middlePanel.add(information33);
        JLabel information34 = new JLabel("1:00PM",SwingConstants.CENTER);
        information34.setPreferredSize(new Dimension(220,100));
        information34.setFont(new Font(Font.DIALOG,Font.PLAIN,28));
        information34.setForeground(Color.black);
        information34.setOpaque(true);
        information34.setBackground(new Color(232, 232, 232));
        gbc.weightx=0.2;
        gbc.weighty=0.2;
        gbc.gridwidth=1;
        gbc.gridheight=1;
        gbc.gridx=3;
        gbc.gridy=2;
        gbl.setConstraints(information34,gbc);
        middlePanel.add(information34);
        JLabel information35 = new JLabel("ON TIME",SwingConstants.CENTER);
        information35.setPreferredSize(new Dimension(330,100));
        information35.setFont(new Font(Font.DIALOG,Font.PLAIN,28));
        information35.setForeground(Color.black);
        information35.setOpaque(true);
        information35.setBackground(new Color(232, 232, 232));
        gbc.weightx=0.3;
        gbc.weighty=0.2;
        gbc.gridwidth=1;
        gbc.gridheight=1;
        gbc.gridx=4;
        gbc.gridy=2;
        gbl.setConstraints(information35,gbc);
        middlePanel.add(information35);

        JLabel information41 = new JLabel();
        information41.setPreferredSize(new Dimension(110,100));
        information41.setOpaque(true);
        information41.setBackground(new Color(252, 250, 250));
        JButton button3 = new JButton();
        button3.setSize(110,100);
        button3.setBackground(new Color(96, 62, 151));
        button3.setVisible(true);
        information41.add(button3);
        gbc.weightx=0.1;
        gbc.weighty=0.2;
        gbc.gridwidth=1;
        gbc.gridheight=1;
        gbc.gridx=0;
        gbc.gridy=3;
        gbl.setConstraints(information41,gbc);
        middlePanel.add(information41);
        JLabel information42 = new JLabel("BA0580",SwingConstants.CENTER);
        information42.setPreferredSize(new Dimension(220,100));
        information42.setFont(new Font(Font.DIALOG,Font.PLAIN,28));
        information42.setForeground(Color.black);
        information42.setOpaque(true);
        information42.setBackground(new Color(252, 250, 250));
        gbc.weightx=0.2;
        gbc.weighty=0.2;
        gbc.gridwidth=1;
        gbc.gridheight=1;
        gbc.gridx=1;
        gbc.gridy=3;
        gbl.setConstraints(information42,gbc);
        middlePanel.add(information42);
        JLabel information43 = new JLabel("Pairs",SwingConstants.CENTER);
        information43.setPreferredSize(new Dimension(220,100));
        information43.setFont(new Font(Font.DIALOG,Font.PLAIN,28));
        information43.setForeground(Color.black);
        information43.setOpaque(true);
        information43.setBackground(new Color(252, 250, 250));
        gbc.weightx=0.2;
        gbc.weighty=0.2;
        gbc.gridwidth=1;
        gbc.gridheight=1;
        gbc.gridx=2;
        gbc.gridy=3;
        gbl.setConstraints(information43,gbc);
        middlePanel.add(information43);
        JLabel information44 = new JLabel("8:00AM",SwingConstants.CENTER);
        information44.setPreferredSize(new Dimension(220,100));
        information44.setFont(new Font(Font.DIALOG,Font.PLAIN,28));
        information44.setForeground(Color.black);
        information44.setOpaque(true);
        information44.setBackground(new Color(252, 250, 250));
        gbc.weightx=0.2;
        gbc.weighty=0.2;
        gbc.gridwidth=1;
        gbc.gridheight=1;
        gbc.gridx=3;
        gbc.gridy=3;
        gbl.setConstraints(information44,gbc);
        middlePanel.add(information44);
        JLabel information45 = new JLabel("ON TIME",SwingConstants.CENTER);
        information45.setPreferredSize(new Dimension(330,100));
        information45.setFont(new Font(Font.DIALOG,Font.PLAIN,28));
        information45.setForeground(Color.black);
        information45.setOpaque(true);
        information45.setBackground(new Color(252, 250, 250));
        gbc.weightx=0.3;
        gbc.weighty=0.2;
        gbc.gridwidth=1;
        gbc.gridheight=1;
        gbc.gridx=4;
        gbc.gridy=3;
        gbl.setConstraints(information45,gbc);
        middlePanel.add(information45);

        JLabel information51 = new JLabel();
        information51.setPreferredSize(new Dimension(110,100));
        information51.setOpaque(true);
        information51.setBackground(new Color(232, 232, 232));
        JButton button4 = new JButton();
        button4.setSize(110,100);
        button4.setBackground(new Color(96, 62, 151));
        button4.setVisible(true);
        information51.add(button4);
        gbc.weightx=0.1;
        gbc.weighty=0.2;
        gbc.gridwidth=1;
        gbc.gridheight=1;
        gbc.gridx=0;
        gbc.gridy=4;
        gbl.setConstraints(information51,gbc);
        middlePanel.add(information51);
        JLabel information52 = new JLabel("BA0341",SwingConstants.CENTER);
        information52.setPreferredSize(new Dimension(220,100));
        information52.setFont(new Font(Font.DIALOG,Font.PLAIN,28));
        information52.setForeground(Color.black);
        information52.setOpaque(true);
        information52.setBackground(new Color(232, 232, 232));
        gbc.weightx=0.2;
        gbc.weighty=0.2;
        gbc.gridwidth=1;
        gbc.gridheight=1;
        gbc.gridx=1;
        gbc.gridy=4;
        gbl.setConstraints(information52,gbc);
        middlePanel.add(information52);
        JLabel information53 = new JLabel("Pairs",SwingConstants.CENTER);
        information53.setPreferredSize(new Dimension(220,100));
        information53.setFont(new Font(Font.DIALOG,Font.PLAIN,28));
        information53.setForeground(Color.black);
        information53.setOpaque(true);
        information53.setBackground(new Color(232, 232, 232));
        gbc.weightx=0.2;
        gbc.weighty=0.2;
        gbc.gridwidth=1;
        gbc.gridheight=1;
        gbc.gridx=2;
        gbc.gridy=4;
        gbl.setConstraints(information53,gbc);
        middlePanel.add(information53);
        JLabel information54 = new JLabel("5:00PM",SwingConstants.CENTER);
        information54.setPreferredSize(new Dimension(220,100));
        information54.setFont(new Font(Font.DIALOG,Font.PLAIN,28));
        information54.setForeground(Color.black);
        information54.setOpaque(true);
        information54.setBackground(new Color(232, 232, 232));
        gbc.weightx=0.2;
        gbc.weighty=0.2;
        gbc.gridwidth=1;
        gbc.gridheight=1;
        gbc.gridx=3;
        gbc.gridy=4;
        gbl.setConstraints(information54,gbc);
        middlePanel.add(information54);
        JLabel information55 = new JLabel("DELAYED",SwingConstants.CENTER);
        information55.setPreferredSize(new Dimension(330,100));
        information55.setFont(new Font(Font.DIALOG,Font.PLAIN,28));
        information55.setForeground(Color.black);
        information55.setOpaque(true);
        information55.setBackground(new Color(232, 232, 232));
        gbc.weightx=0.3;
        gbc.weighty=0.2;
        gbc.gridwidth=1;
        gbc.gridheight=1;
        gbc.gridx=4;
        gbc.gridy=4;
        gbl.setConstraints(information55,gbc);
        middlePanel.add(information55);

        panel.add(middlePanel);

        JButton button_confirm = new JButton("CONFIRM");
        button_confirm.setFont(new Font(Font.DIALOG,Font.PLAIN,28));
        button_confirm.setForeground(Color.white);
        button_confirm.setBackground(new Color(250,50,50));
        button_confirm.setSize(200,50);
        button_confirm.setVisible(true);
        JLabel panel_confirm = new JLabel();
        panel_confirm.setBounds(800,60,200,50);

        panel_confirm.setOpaque(true);
        panel_confirm.setBackground(new Color(250,50,50));
        panel_confirm.setPreferredSize(new Dimension(800,80));
        panel_confirm.setHorizontalAlignment(JLabel.CENTER);
        panel_confirm.add(button_confirm);

        panel.add(panel_confirm);



        button_confirm.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                button1.setBackground(new Color(96, 84, 146));
                button2.setBackground(new Color(96 ,84, 146));
                button3.setBackground(new Color(96 ,84, 146));
                button4.setBackground(new Color(96 ,84, 146));
                flight_num=InputArea.getText();
            }


        });

        button1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                button1.setBackground(new Color(104, 210, 128));
                button2.setBackground(new Color(96 ,84, 146));
                button3.setBackground(new Color(96 ,84, 146));
                button4.setBackground(new Color(96 ,84, 146));
                flight_num="BA0342";
            }


        });

        button2.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                button2.setBackground(new Color(104, 210, 128));
                button1.setBackground(new Color(96 ,84, 146));
                button3.setBackground(new Color(96 ,84, 146));
                button4.setBackground(new Color(96 ,84, 146));
                flight_num="BA0570";

            }


        });

        button3.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                button3.setBackground(new Color(104, 210, 128));
                button1.setBackground(new Color(96 ,84, 146));
                button2.setBackground(new Color(96 ,84, 146));
                button4.setBackground(new Color(96 ,84, 146));
                flight_num="BA0580";
            }


        });

        button4.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                button4.setBackground(new Color(104, 210, 128));
                button1.setBackground(new Color(96 ,84, 146));
                button2.setBackground(new Color(96 ,84, 146));
                button3.setBackground(new Color(96 ,84, 146));
                flight_num="BA3041";
            }


        });

    }

}
