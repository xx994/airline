package frame.AdminPanel;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import model.*;
/**
 * Title             : Admin_Information.java
 * Description : This class build the panel for administer to check the information of chosen flight.
 */
public class Admin_Information extends JPanel {

    int passenger_num=0;
    int pageNum = 1 ;
    public Admin_Information(String Flight_num){
        //设置背景颜色
        setBackground(new Color(72,46,115));
        //设置初始面板
        JPanel panel = new JPanel();
        panel.setBackground(new Color(72,46,115));
        panel.setPreferredSize(new Dimension(1200,680));
        add(panel);

        flight f = new flight();
        passenger_order po = new passenger_order();
        passenger_num=f.administer_findEachFlight_Num(Flight_num);
        ArrayList<String> information = new ArrayList<>();
        information = f.administer_findEachFlight_Information(Flight_num);


        CardLayout c = new CardLayout();
        panel.setLayout(c);

        JPanel p1 = new JPanel();
        p1.setBackground(new Color(72,46,115));
        p1.setPreferredSize(new Dimension(1200,680));
        p1.setLayout(null);


        JLabel Head1 = new JLabel(Flight_num);
        Head1.setBounds(450,10,300,50);
        //外观设计
        Head1.setFont(new Font(Font.DIALOG,Font.BOLD,29));//设置文字字体
        Head1.setForeground(Color.white);//设置文字的颜色
        Head1.setOpaque(true);
        Head1.setBackground(new Color(96, 62,151));
        Head1.setPreferredSize(new Dimension(300,50));
        Head1.setHorizontalAlignment(JLabel.CENTER);

        p1.add(Head1);

        JButton button_right = new JButton("NEXT");
        button_right.setFont(new Font(Font.DIALOG,Font.BOLD,29));//设置文字字体
        button_right.setForeground(Color.white);//设置文字的颜色
        button_right.setBackground(new Color(96, 62,151));
        button_right.setSize(200,50);
        button_right.setVisible(true);
        JLabel label_right = new JLabel();
        label_right.setBounds(800,10,200,50);
        //外观设计
        label_right.setOpaque(true);
        label_right.setBackground(new Color(96, 62,151));
        label_right.setHorizontalAlignment(JLabel.CENTER);
        label_right.add(button_right);

        p1.add(label_right);


        JPanel middlePanel1 = new JPanel();
        middlePanel1.setBounds(50,80,1100,500);


        GridBagLayout gbl1 = new GridBagLayout();
        GridBagConstraints gbc1 = new GridBagConstraints();
        middlePanel1.setLayout(gbl1);
        gbc1.fill=GridBagConstraints.BOTH;


            JLabel information11 = new JLabel("Name",SwingConstants.CENTER);
            information11.setPreferredSize(new Dimension(220, 50));
            information11.setOpaque(true);
            information11.setFont(new Font(Font.DIALOG, Font.PLAIN, 20));//设置文字字体
            information11.setForeground(Color.black);//设置文字的颜色
            information11.setBackground(new Color(252, 250, 250));
            gbc1.weightx = 0.2;    // 指定组件的分配区域
            gbc1.weighty = 0.1;
            gbc1.gridwidth = 1;
            gbc1.gridheight = 1;
            gbc1.gridx = 0;
            gbc1.gridy = 0;
            gbl1.setConstraints(information11, gbc1);
            middlePanel1.add(information11);
            JLabel information12 = new JLabel("State", SwingConstants.CENTER);
            information12.setPreferredSize(new Dimension(220, 50));
            information12.setFont(new Font(Font.DIALOG, Font.PLAIN, 20));//设置文字字体
            information12.setForeground(Color.black);//设置文字的颜色
            information12.setOpaque(true);
            information12.setBackground(new Color(252, 250, 250));
            gbc1.weightx = 0.2;    // 指定组件的分配区域
            gbc1.weighty = 0.1;
            gbc1.gridwidth = 1;
            gbc1.gridheight = 1;
            gbc1.gridx = 1;
            gbc1.gridy = 0;
            gbl1.setConstraints(information12, gbc1);
            middlePanel1.add(information12);
            JLabel information13 = new JLabel("Seats", SwingConstants.CENTER);
            information13.setPreferredSize(new Dimension(220, 50));
            information13.setFont(new Font(Font.DIALOG, Font.PLAIN, 20));//设置文字字体
            information13.setForeground(Color.black);//设置文字的颜色
            information13.setOpaque(true);
            information13.setBackground(new Color(252, 250, 250));
            gbc1.weightx = 0.2;    // 指定组件的分配区域
            gbc1.weighty = 0.1;
            gbc1.gridwidth = 1;
            gbc1.gridheight = 1;
            gbc1.gridx = 2;
            gbc1.gridy = 0;
            gbl1.setConstraints(information13, gbc1);
            middlePanel1.add(information13);
            JLabel information14 = new JLabel("Foods", SwingConstants.CENTER);
            information14.setPreferredSize(new Dimension(220, 50));
            information14.setFont(new Font(Font.DIALOG, Font.PLAIN, 20));//设置文字字体
            information14.setForeground(Color.black);//设置文字的颜色
            information14.setOpaque(true);
            information14.setBackground(new Color(252, 250, 250));
            gbc1.weightx = 0.2;    // 指定组件的分配区域
            gbc1.weighty = 0.1;
            gbc1.gridwidth = 1;
            gbc1.gridheight = 1;
            gbc1.gridx = 3;
            gbc1.gridy = 0;
            gbl1.setConstraints(information14, gbc1);
            middlePanel1.add(information14);
            JLabel information15 = new JLabel("Note", SwingConstants.CENTER);
            information15.setPreferredSize(new Dimension(220, 50));
            information15.setFont(new Font(Font.DIALOG, Font.PLAIN, 20));//设置文字字体
            information15.setForeground(Color.black);//设置文字的颜色
            information15.setOpaque(true);
            information15.setBackground(new Color(252, 250, 250));
            gbc1.weightx = 0.2;    // 指定组件的分配区域
            gbc1.weighty = 0.1;
            gbc1.gridwidth = 1;
            gbc1.gridheight = 1;
            gbc1.gridx = 4;
            gbc1.gridy = 0;
            gbl1.setConstraints(information15, gbc1);
            middlePanel1.add(information15);

            int LENGTH = 0;


                String Name1_1 = "";
                String State1_1 ="";
                String Seats1_1 ="";
                String Foods1_1 ="";
                String Note1_1 = "";
        if(passenger_num>=1) {
            String[] information1_1 = information.get(0).split("\\s+");
            Name1_1 = po.findName(information1_1[0]);
            State1_1 = information1_1[5];
            if(!information1_1[3].equals("LETTERUNCHOSEN")&&!information1_1[4].equals("NUMBERUNCHOSEN")) {
                Seats1_1 = information1_1[3] + information1_1[4];
            }
            if(!information1_1[2].equals("FOODUNCHOSEN")){
                Foods1_1 = information1_1[2];
            }
            if(information1_1.length>9) {
                LENGTH=information1_1.length;
                while(LENGTH>=9) {
                    Note1_1 = information1_1[LENGTH-2]+" "+Note1_1;
                    LENGTH=LENGTH-1;
                }
            }else{
                if (information1_1[7].equals("NOTE")) {
                    Note1_1 = "";
                } else {
                    Note1_1 = information1_1[7];
                }
            }
        }
                JLabel name1_1 = new JLabel(Name1_1,SwingConstants.CENTER);
                name1_1.setPreferredSize(new Dimension(220, 45));
                name1_1.setOpaque(true);
                name1_1.setFont(new Font(Font.DIALOG, Font.PLAIN, 20));//设置文字字体
                name1_1.setForeground(Color.black);//设置文字的颜色
                name1_1.setBackground(new Color(232, 232, 232));
                gbc1.weightx = 0.2;    // 指定组件的分配区域
                gbc1.weighty = 0.09;
                gbc1.gridwidth = 1;
                gbc1.gridheight = 1;
                gbc1.gridx = 0;
                gbc1.gridy = 1;
                gbl1.setConstraints(name1_1, gbc1);
                middlePanel1.add(name1_1);
                JLabel state1_1 = new JLabel(State1_1, SwingConstants.CENTER);
                state1_1.setPreferredSize(new Dimension(220, 45));
                state1_1.setFont(new Font(Font.DIALOG, Font.PLAIN, 20));//设置文字字体
                state1_1.setForeground(Color.black);//设置文字的颜色
                state1_1.setOpaque(true);
                state1_1.setBackground(new Color(232, 232, 232));
                gbc1.weightx = 0.2;    // 指定组件的分配区域
                gbc1.weighty = 0.09;
                gbc1.gridwidth = 1;
                gbc1.gridheight = 1;
                gbc1.gridx = 1;
                gbc1.gridy = 1;
                gbl1.setConstraints(state1_1, gbc1);
                middlePanel1.add(state1_1);
                JLabel seats1_1 = new JLabel(Seats1_1, SwingConstants.CENTER);
                seats1_1.setPreferredSize(new Dimension(220, 45));
                seats1_1.setFont(new Font(Font.DIALOG, Font.PLAIN, 20));//设置文字字体
                seats1_1.setForeground(Color.black);//设置文字的颜色
                seats1_1.setOpaque(true);
                seats1_1.setBackground(new Color(232, 232, 232));
                gbc1.weightx = 0.2;    // 指定组件的分配区域
                gbc1.weighty = 0.09;
                gbc1.gridwidth = 1;
                gbc1.gridheight = 1;
                gbc1.gridx = 2;
                gbc1.gridy = 1;
                gbl1.setConstraints(seats1_1, gbc1);
                middlePanel1.add(seats1_1);
                JLabel foods1_1 = new JLabel(Foods1_1, SwingConstants.CENTER);
                foods1_1.setPreferredSize(new Dimension(220, 45));
                foods1_1.setFont(new Font(Font.DIALOG, Font.PLAIN, 20));//设置文字字体
                foods1_1.setForeground(Color.black);//设置文字的颜色
                foods1_1.setOpaque(true);
                foods1_1.setBackground(new Color(232, 232, 232));
                gbc1.weightx = 0.2;    // 指定组件的分配区域
                gbc1.weighty = 0.09;
                gbc1.gridwidth = 1;
                gbc1.gridheight = 1;
                gbc1.gridx = 3;
                gbc1.gridy = 1;
                gbl1.setConstraints(foods1_1, gbc1);
                middlePanel1.add(foods1_1);
                JLabel note1_1 = new JLabel(Note1_1, SwingConstants.CENTER);
                note1_1.setPreferredSize(new Dimension(220, 45));
                note1_1.setFont(new Font(Font.DIALOG, Font.PLAIN, 20));//设置文字字体
                note1_1.setForeground(Color.black);//设置文字的颜色
                note1_1.setOpaque(true);
                note1_1.setBackground(new Color(232, 232, 232));
                gbc1.weightx = 0.2;    // 指定组件的分配区域
                gbc1.weighty = 0.09;
                gbc1.gridwidth = 1;
                gbc1.gridheight = 1;
                gbc1.gridx = 4;
                gbc1.gridy = 1;
                gbl1.setConstraints(note1_1, gbc1);
                middlePanel1.add(note1_1);

        String Name1_2 = "";
        String State1_2 ="";
        String Seats1_2 ="";
        String Foods1_2 ="";
        String Note1_2 = "";
        if(passenger_num>=2) {
            String[] information1_2 = information.get(1).split("\\s+");
            Name1_2 = po.findName(information1_2[0]);
            State1_2 = information1_2[5];
            if(!information1_2[3].equals("LETTERUNCHOSEN")&&!information1_2[4].equals("NUMBERUNCHOSEN")) {
                Seats1_2 = information1_2[3] + information1_2[4];
            }
            if(!information1_2[2].equals("FOODUNCHOSEN")){
                Foods1_2 = information1_2[2];
            }
            if(information1_2.length>9) {
                LENGTH=information1_2.length;
                while(LENGTH>=9) {
                    Note1_2 = information1_2[LENGTH-2]+" "+Note1_2;
                    LENGTH=LENGTH-1;
                }
            }else{
                if (information1_2[7].equals("NOTE")) {
                    Note1_2 = "";
                } else {
                    Note1_2 = information1_2[7];
                }
            }
        }
            JLabel name1_2 = new JLabel(Name1_2,SwingConstants.CENTER);
            name1_2.setPreferredSize(new Dimension(220, 45));
            name1_2.setOpaque(true);
            name1_2.setFont(new Font(Font.DIALOG, Font.PLAIN, 20));//设置文字字体
            name1_2.setForeground(Color.black);//设置文字的颜色
            name1_2.setBackground(new Color(252, 250, 250));
            gbc1.weightx = 0.2;    // 指定组件的分配区域
            gbc1.weighty = 0.09;
            gbc1.gridwidth = 1;
            gbc1.gridheight = 1;
            gbc1.gridx = 0;
            gbc1.gridy = 2;
            gbl1.setConstraints(name1_2, gbc1);
            middlePanel1.add(name1_2);
            JLabel state1_2 = new JLabel(State1_2, SwingConstants.CENTER);
            state1_2.setPreferredSize(new Dimension(220, 45));
            state1_2.setFont(new Font(Font.DIALOG, Font.PLAIN, 20));//设置文字字体
            state1_2.setForeground(Color.black);//设置文字的颜色
            state1_2.setOpaque(true);
            state1_2.setBackground(new Color(252, 250, 250));
            gbc1.weightx = 0.2;    // 指定组件的分配区域
            gbc1.weighty = 0.09;
            gbc1.gridwidth = 1;
            gbc1.gridheight = 1;
            gbc1.gridx = 1;
            gbc1.gridy = 2;
            gbl1.setConstraints(state1_2, gbc1);
            middlePanel1.add(state1_2);
            JLabel seats1_2 = new JLabel(Seats1_2, SwingConstants.CENTER);
            seats1_2.setPreferredSize(new Dimension(220, 45));
            seats1_2.setFont(new Font(Font.DIALOG, Font.PLAIN, 20));//设置文字字体
            seats1_2.setForeground(Color.black);//设置文字的颜色
            seats1_2.setOpaque(true);
            seats1_2.setBackground(new Color(252, 250, 250));
            gbc1.weightx = 0.2;    // 指定组件的分配区域
            gbc1.weighty = 0.09;
            gbc1.gridwidth = 1;
            gbc1.gridheight = 1;
            gbc1.gridx = 2;
            gbc1.gridy = 2;
            gbl1.setConstraints(seats1_2, gbc1);
            middlePanel1.add(seats1_2);
            JLabel foods1_2 = new JLabel(Foods1_2, SwingConstants.CENTER);
            foods1_2.setPreferredSize(new Dimension(220, 45));
            foods1_2.setFont(new Font(Font.DIALOG, Font.PLAIN, 20));//设置文字字体
            foods1_2.setForeground(Color.black);//设置文字的颜色
            foods1_2.setOpaque(true);
            foods1_2.setBackground(new Color(252, 250, 250));
            gbc1.weightx = 0.2;    // 指定组件的分配区域
            gbc1.weighty = 0.09;
            gbc1.gridwidth = 1;
            gbc1.gridheight = 1;
            gbc1.gridx = 3;
            gbc1.gridy = 2;
            gbl1.setConstraints(foods1_2, gbc1);
            middlePanel1.add(foods1_2);
            JLabel note1_2 = new JLabel(Note1_2, SwingConstants.CENTER);
            note1_2.setPreferredSize(new Dimension(220, 45));
            note1_2.setFont(new Font(Font.DIALOG, Font.PLAIN, 20));//设置文字字体
            note1_2.setForeground(Color.black);//设置文字的颜色
            note1_2.setOpaque(true);
            note1_2.setBackground(new Color(252, 250, 250));
            gbc1.weightx = 0.2;    // 指定组件的分配区域
            gbc1.weighty = 0.09;
            gbc1.gridwidth = 1;
            gbc1.gridheight = 1;
            gbc1.gridx = 4;
            gbc1.gridy = 2;
            gbl1.setConstraints(note1_2, gbc1);
            middlePanel1.add(note1_2);

        String Name1_3 = "";
        String State1_3 ="";
        String Seats1_3 ="";
        String Foods1_3 ="";
        String Note1_3 = "";
        if(passenger_num>=3) {
            String[] information1_3 = information.get(2).split("\\s+");
            Name1_3 = po.findName(information1_3[0]);
            State1_3 = information1_3[5];
            if(!information1_3[3].equals("LETTERUNCHOSEN")&&!information1_3[4].equals("NUMBERUNCHOSEN")) {
                Seats1_3 = information1_3[3] + information1_3[4];
            }
            if(!information1_3[2].equals("FOODUNCHOSEN")){
                Foods1_3 = information1_3[2];
            }
            if(information1_3.length>9) {
                LENGTH=information1_3.length;
                while(LENGTH>=9) {
                    Note1_3 = information1_3[LENGTH-2]+" "+Note1_3;
                    LENGTH=LENGTH-1;
                }
            }else{
                if (information1_3[7].equals("NOTE")) {
                    Note1_3 = "";
                } else {
                    Note1_3 = information1_3[7];
                }
            }
        }
            JLabel name1_3 = new JLabel(Name1_3,SwingConstants.CENTER);
            name1_3.setPreferredSize(new Dimension(220, 45));
            name1_3.setOpaque(true);
            name1_3.setFont(new Font(Font.DIALOG, Font.PLAIN, 20));//设置文字字体
            name1_3.setForeground(Color.black);//设置文字的颜色
            name1_3.setBackground(new Color(232, 232, 232));
            gbc1.weightx = 0.2;    // 指定组件的分配区域
            gbc1.weighty = 0.09;
            gbc1.gridwidth = 1;
            gbc1.gridheight = 1;
            gbc1.gridx = 0;
            gbc1.gridy = 3;
            gbl1.setConstraints(name1_3, gbc1);
            middlePanel1.add(name1_3);
            JLabel state1_3 = new JLabel(State1_3, SwingConstants.CENTER);
            state1_3.setPreferredSize(new Dimension(220, 45));
            state1_3.setFont(new Font(Font.DIALOG, Font.PLAIN, 20));//设置文字字体
            state1_3.setForeground(Color.black);//设置文字的颜色
            state1_3.setOpaque(true);
            state1_3.setBackground(new Color(232, 232, 232));
            gbc1.weightx = 0.2;    // 指定组件的分配区域
            gbc1.weighty = 0.09;
            gbc1.gridwidth = 1;
            gbc1.gridheight = 1;
            gbc1.gridx = 1;
            gbc1.gridy = 3;
            gbl1.setConstraints(state1_3, gbc1);
            middlePanel1.add(state1_3);
            JLabel seats1_3 = new JLabel(Seats1_3, SwingConstants.CENTER);
            seats1_3.setPreferredSize(new Dimension(220, 45));
            seats1_3.setFont(new Font(Font.DIALOG, Font.PLAIN, 20));//设置文字字体
            seats1_3.setForeground(Color.black);//设置文字的颜色
            seats1_3.setOpaque(true);
            seats1_3.setBackground(new Color(232, 232, 232));
            gbc1.weightx = 0.2;    // 指定组件的分配区域
            gbc1.weighty = 0.09;
            gbc1.gridwidth = 1;
            gbc1.gridheight = 1;
            gbc1.gridx = 2;
            gbc1.gridy = 3;
            gbl1.setConstraints(seats1_3, gbc1);
            middlePanel1.add(seats1_3);
            JLabel foods1_3 = new JLabel(Foods1_3, SwingConstants.CENTER);
            foods1_3.setPreferredSize(new Dimension(220, 45));
            foods1_3.setFont(new Font(Font.DIALOG, Font.PLAIN, 20));//设置文字字体
            foods1_3.setForeground(Color.black);//设置文字的颜色
            foods1_3.setOpaque(true);
            foods1_3.setBackground(new Color(232, 232, 232));
            gbc1.weightx = 0.2;    // 指定组件的分配区域
            gbc1.weighty = 0.09;
            gbc1.gridwidth = 1;
            gbc1.gridheight = 1;
            gbc1.gridx = 3;
            gbc1.gridy = 3;
            gbl1.setConstraints(foods1_3, gbc1);
            middlePanel1.add(foods1_3);
            JLabel note1_3 = new JLabel(Note1_3, SwingConstants.CENTER);
            note1_3.setPreferredSize(new Dimension(220, 45));
            note1_3.setFont(new Font(Font.DIALOG, Font.PLAIN, 20));//设置文字字体
            note1_3.setForeground(Color.black);//设置文字的颜色
            note1_3.setOpaque(true);
            note1_3.setBackground(new Color(232, 232, 232));
            gbc1.weightx = 0.2;    // 指定组件的分配区域
            gbc1.weighty = 0.09;
            gbc1.gridwidth = 1;
            gbc1.gridheight = 1;
            gbc1.gridx = 4;
            gbc1.gridy = 3;
            gbl1.setConstraints(note1_3, gbc1);
            middlePanel1.add(note1_3);

        String Name1_4 = "";
        String State1_4 ="";
        String Seats1_4 ="";
        String Foods1_4 ="";
        String Note1_4 = "";
        if(passenger_num>=4) {
            String[] information1_4 = information.get(3).split("\\s+");
            Name1_4 = po.findName(information1_4[0]);
            State1_4 = information1_4[5];
            if(!information1_4[3].equals("LETTERUNCHOSEN")&&!information1_4[4].equals("NUMBERUNCHOSEN")) {
                Seats1_4 = information1_4[3] + information1_4[4];
            }
            if(!information1_4[2].equals("FOODUNCHOSEN")){
                Foods1_4 = information1_4[2];
            }
            if(information1_4.length>9) {
                LENGTH=information1_4.length;
                while(LENGTH>=9) {
                    Note1_4 = information1_4[LENGTH-2]+" "+Note1_4;
                    LENGTH=LENGTH-1;
                }
            }else{
                if (information1_4[7].equals("NOTE")) {
                    Note1_4 = "";
                } else {
                    Note1_4 = information1_4[7];
                }
            }
        }
            JLabel name1_4 = new JLabel(Name1_4,SwingConstants.CENTER);
            name1_4.setPreferredSize(new Dimension(220, 45));
            name1_4.setOpaque(true);
            name1_4.setFont(new Font(Font.DIALOG, Font.PLAIN, 20));//设置文字字体
            name1_4.setForeground(Color.black);//设置文字的颜色
            name1_4.setBackground(new Color(252, 250, 250));
            gbc1.weightx = 0.2;    // 指定组件的分配区域
            gbc1.weighty = 0.09;
            gbc1.gridwidth = 1;
            gbc1.gridheight = 1;
            gbc1.gridx = 0;
            gbc1.gridy = 4;
            gbl1.setConstraints(name1_4, gbc1);
            middlePanel1.add(name1_4);
            JLabel state1_4 = new JLabel(State1_4, SwingConstants.CENTER);
            state1_4.setPreferredSize(new Dimension(220, 45));
            state1_4.setFont(new Font(Font.DIALOG, Font.PLAIN, 20));//设置文字字体
            state1_4.setForeground(Color.black);//设置文字的颜色
            state1_4.setOpaque(true);
            state1_4.setBackground(new Color(252, 250, 250));
            gbc1.weightx = 0.2;    // 指定组件的分配区域
            gbc1.weighty = 0.09;
            gbc1.gridwidth = 1;
            gbc1.gridheight = 1;
            gbc1.gridx = 1;
            gbc1.gridy = 4;
            gbl1.setConstraints(state1_4, gbc1);
            middlePanel1.add(state1_4);
            JLabel seats1_4 = new JLabel(Seats1_4, SwingConstants.CENTER);
            seats1_4.setPreferredSize(new Dimension(220, 45));
            seats1_4.setFont(new Font(Font.DIALOG, Font.PLAIN, 20));//设置文字字体
            seats1_4.setForeground(Color.black);//设置文字的颜色
            seats1_4.setOpaque(true);
            seats1_4.setBackground(new Color(252, 250, 250));
            gbc1.weightx = 0.2;    // 指定组件的分配区域
            gbc1.weighty = 0.09;
            gbc1.gridwidth = 1;
            gbc1.gridheight = 1;
            gbc1.gridx = 2;
            gbc1.gridy = 4;
            gbl1.setConstraints(seats1_4, gbc1);
            middlePanel1.add(seats1_4);
            JLabel foods1_4 = new JLabel(Foods1_4, SwingConstants.CENTER);
            foods1_4.setPreferredSize(new Dimension(220, 45));
            foods1_4.setFont(new Font(Font.DIALOG, Font.PLAIN, 20));//设置文字字体
            foods1_4.setForeground(Color.black);//设置文字的颜色
            foods1_4.setOpaque(true);
            foods1_4.setBackground(new Color(252, 250, 250));
            gbc1.weightx = 0.2;    // 指定组件的分配区域
            gbc1.weighty = 0.09;
            gbc1.gridwidth = 1;
            gbc1.gridheight = 1;
            gbc1.gridx = 3;
            gbc1.gridy = 4;
            gbl1.setConstraints(foods1_4, gbc1);
            middlePanel1.add(foods1_4);
            JLabel note1_4 = new JLabel(Note1_4, SwingConstants.CENTER);
            note1_4.setPreferredSize(new Dimension(220, 45));
            note1_4.setFont(new Font(Font.DIALOG, Font.PLAIN, 20));//设置文字字体
            note1_4.setForeground(Color.black);//设置文字的颜色
            note1_4.setOpaque(true);
            note1_4.setBackground(new Color(252, 250, 250));
            gbc1.weightx = 0.2;    // 指定组件的分配区域
            gbc1.weighty = 0.09;
            gbc1.gridwidth = 1;
            gbc1.gridheight = 1;
            gbc1.gridx = 4;
            gbc1.gridy = 4;
            gbl1.setConstraints(note1_4, gbc1);
            middlePanel1.add(note1_4);

        String Name1_5 = "";
        String State1_5 ="";
        String Seats1_5 ="";
        String Foods1_5 ="";
        String Note1_5 = "";
        if(passenger_num>=5) {
            String[] information1_5 = information.get(4).split("\\s+");
            Name1_5 = po.findName(information1_5[0]);
            State1_5 = information1_5[5];
            if(!information1_5[3].equals("LETTERUNCHOSEN")&&!information1_5[4].equals("NUMBERUNCHOSEN")) {
                Seats1_5 = information1_5[3] + information1_5[4];
            }
            if(!information1_5[2].equals("FOODUNCHOSEN")){
                Foods1_5 = information1_5[2];
            }
            if(information1_5.length>9) {
                LENGTH=information1_5.length;
                while(LENGTH>=9) {
                    Note1_5 = information1_5[LENGTH-2]+" "+Note1_5;
                    LENGTH=LENGTH-1;
                }
            }else{
                if (information1_5[7].equals("NOTE")) {
                    Note1_5 = "";
                } else {
                    Note1_5 = information1_5[7];
                }
            }
        }
            JLabel name1_5 = new JLabel(Name1_5,SwingConstants.CENTER);
            name1_5.setPreferredSize(new Dimension(220, 45));
            name1_5.setOpaque(true);
            name1_5.setFont(new Font(Font.DIALOG, Font.PLAIN, 20));//设置文字字体
            name1_5.setForeground(Color.black);//设置文字的颜色
            name1_5.setBackground(new Color(232, 232, 232));
            gbc1.weightx = 0.2;    // 指定组件的分配区域
            gbc1.weighty = 0.09;
            gbc1.gridwidth = 1;
            gbc1.gridheight = 1;
            gbc1.gridx = 0;
            gbc1.gridy = 5;
            gbl1.setConstraints(name1_5, gbc1);
            middlePanel1.add(name1_5);
            JLabel state1_5 = new JLabel(State1_5, SwingConstants.CENTER);
            state1_5.setPreferredSize(new Dimension(220, 45));
            state1_5.setFont(new Font(Font.DIALOG, Font.PLAIN, 20));//设置文字字体
            state1_5.setForeground(Color.black);//设置文字的颜色
            state1_5.setOpaque(true);
            state1_5.setBackground(new Color(232, 232, 232));
            gbc1.weightx = 0.2;    // 指定组件的分配区域
            gbc1.weighty = 0.09;
            gbc1.gridwidth = 1;
            gbc1.gridheight = 1;
            gbc1.gridx = 1;
            gbc1.gridy = 5;
            gbl1.setConstraints(state1_5, gbc1);
            middlePanel1.add(state1_5);
            JLabel seats1_5 = new JLabel(Seats1_5, SwingConstants.CENTER);
            seats1_5.setPreferredSize(new Dimension(220, 45));
            seats1_5.setFont(new Font(Font.DIALOG, Font.PLAIN, 20));//设置文字字体
            seats1_5.setForeground(Color.black);//设置文字的颜色
            seats1_5.setOpaque(true);
            seats1_5.setBackground(new Color(232, 232, 232));
            gbc1.weightx = 0.2;    // 指定组件的分配区域
            gbc1.weighty = 0.09;
            gbc1.gridwidth = 1;
            gbc1.gridheight = 1;
            gbc1.gridx = 2;
            gbc1.gridy = 5;
            gbl1.setConstraints(seats1_5, gbc1);
            middlePanel1.add(seats1_5);
            JLabel foods1_5 = new JLabel(Foods1_5, SwingConstants.CENTER);
            foods1_5.setPreferredSize(new Dimension(220, 45));
            foods1_5.setFont(new Font(Font.DIALOG, Font.PLAIN, 20));//设置文字字体
            foods1_5.setForeground(Color.black);//设置文字的颜色
            foods1_5.setOpaque(true);
            foods1_5.setBackground(new Color(232, 232, 232));
            gbc1.weightx = 0.2;    // 指定组件的分配区域
            gbc1.weighty = 0.09;
            gbc1.gridwidth = 1;
            gbc1.gridheight = 1;
            gbc1.gridx = 3;
            gbc1.gridy = 5;
            gbl1.setConstraints(foods1_5, gbc1);
            middlePanel1.add(foods1_5);
            JLabel note1_5 = new JLabel(Note1_5, SwingConstants.CENTER);
            note1_5.setPreferredSize(new Dimension(220, 45));
            note1_5.setFont(new Font(Font.DIALOG, Font.PLAIN, 20));//设置文字字体
            note1_5.setForeground(Color.black);//设置文字的颜色
            note1_5.setOpaque(true);
            note1_5.setBackground(new Color(232, 232, 232));
            gbc1.weightx = 0.2;    // 指定组件的分配区域
            gbc1.weighty = 0.09;
            gbc1.gridwidth = 1;
            gbc1.gridheight = 1;
            gbc1.gridx = 4;
            gbc1.gridy = 5;
            gbl1.setConstraints(note1_5, gbc1);
            middlePanel1.add(note1_5);

        String Name1_6 = "";
        String State1_6 ="";
        String Seats1_6 ="";
        String Foods1_6 ="";
        String Note1_6 = "";
        if(passenger_num>=6) {
            String[] information1_6 = information.get(5).split("\\s+");
            Name1_6 = po.findName(information1_6[0]);
            State1_6 = information1_6[5];
            if(!information1_6[3].equals("LETTERUNCHOSEN")&&!information1_6[4].equals("NUMBERUNCHOSEN")) {
                Seats1_6 = information1_6[3] + information1_6[4];
            }
            if(!information1_6[2].equals("FOODUNCHOSEN")){
                Foods1_6 = information1_6[2];
            }
            if(information1_6.length>9) {
                LENGTH=information1_6.length;
                while(LENGTH>=9) {
                    Note1_6 = information1_6[LENGTH-2]+" "+Note1_6;
                    LENGTH=LENGTH-1;
                }
            }else{
                if (information1_6[7].equals("NOTE")) {
                    Note1_6 = "";
                } else {
                    Note1_6 = information1_6[7];
                }
            }
        }
            JLabel name1_6 = new JLabel(Name1_6,SwingConstants.CENTER);
            name1_6.setPreferredSize(new Dimension(220, 45));
            name1_6.setOpaque(true);
            name1_6.setFont(new Font(Font.DIALOG, Font.PLAIN, 20));//设置文字字体
            name1_6.setForeground(Color.black);//设置文字的颜色
            name1_6.setBackground(new Color(252, 250, 250));
            gbc1.weightx = 0.2;    // 指定组件的分配区域
            gbc1.weighty = 0.09;
            gbc1.gridwidth = 1;
            gbc1.gridheight = 1;
            gbc1.gridx = 0;
            gbc1.gridy = 6;
            gbl1.setConstraints(name1_6, gbc1);
            middlePanel1.add(name1_6);
            JLabel state1_6 = new JLabel(State1_6, SwingConstants.CENTER);
            state1_6.setPreferredSize(new Dimension(220, 45));
            state1_6.setFont(new Font(Font.DIALOG, Font.PLAIN, 20));//设置文字字体
            state1_6.setForeground(Color.black);//设置文字的颜色
            state1_6.setOpaque(true);
            state1_6.setBackground(new Color(252, 250, 250));
            gbc1.weightx = 0.2;    // 指定组件的分配区域
            gbc1.weighty = 0.09;
            gbc1.gridwidth = 1;
            gbc1.gridheight = 1;
            gbc1.gridx = 1;
            gbc1.gridy = 6;
            gbl1.setConstraints(state1_6, gbc1);
            middlePanel1.add(state1_6);
            JLabel seats1_6 = new JLabel(Seats1_6, SwingConstants.CENTER);
            seats1_6.setPreferredSize(new Dimension(220, 45));
            seats1_6.setFont(new Font(Font.DIALOG, Font.PLAIN, 20));//设置文字字体
            seats1_6.setForeground(Color.black);//设置文字的颜色
            seats1_6.setOpaque(true);
            seats1_6.setBackground(new Color(252, 250, 250));
            gbc1.weightx = 0.2;    // 指定组件的分配区域
            gbc1.weighty = 0.09;
            gbc1.gridwidth = 1;
            gbc1.gridheight = 1;
            gbc1.gridx = 2;
            gbc1.gridy = 6;
            gbl1.setConstraints(seats1_6, gbc1);
            middlePanel1.add(seats1_6);
            JLabel foods1_6 = new JLabel(Foods1_6, SwingConstants.CENTER);
            foods1_6.setPreferredSize(new Dimension(220, 45));
            foods1_6.setFont(new Font(Font.DIALOG, Font.PLAIN, 20));//设置文字字体
            foods1_6.setForeground(Color.black);//设置文字的颜色
            foods1_6.setOpaque(true);
            foods1_6.setBackground(new Color(252, 250, 250));
            gbc1.weightx = 0.2;    // 指定组件的分配区域
            gbc1.weighty = 0.09;
            gbc1.gridwidth = 1;
            gbc1.gridheight = 1;
            gbc1.gridx = 3;
            gbc1.gridy = 6;
            gbl1.setConstraints(foods1_6, gbc1);
            middlePanel1.add(foods1_6);
            JLabel note1_6 = new JLabel(Note1_6, SwingConstants.CENTER);
            note1_6.setPreferredSize(new Dimension(220, 45));
            note1_6.setFont(new Font(Font.DIALOG, Font.PLAIN, 20));//设置文字字体
            note1_6.setForeground(Color.black);//设置文字的颜色
            note1_6.setOpaque(true);
            note1_6.setBackground(new Color(252, 250, 250));
            gbc1.weightx = 0.2;    // 指定组件的分配区域
            gbc1.weighty = 0.09;
            gbc1.gridwidth = 1;
            gbc1.gridheight = 1;
            gbc1.gridx = 4;
            gbc1.gridy = 6;
            gbl1.setConstraints(note1_6, gbc1);
            middlePanel1.add(note1_6);


        String Name1_7 = "";
        String State1_7 ="";
        String Seats1_7 ="";
        String Foods1_7 ="";
        String Note1_7 = "";
        if(passenger_num>=7) {
            String[] information1_7 = information.get(6).split("\\s+");
            Name1_7 = po.findName(information1_7[0]);
            State1_7 = information1_7[5];
            if(!information1_7[3].equals("LETTERUNCHOSEN")&&!information1_7[4].equals("NUMBERUNCHOSEN")) {
                Seats1_7 = information1_7[3] + information1_7[4];
            }
            if(!information1_7[2].equals("FOODUNCHOSEN")){
                Foods1_7 = information1_7[2];
            }
            if(information1_7.length>9) {
                LENGTH=information1_7.length;
                while(LENGTH>=9) {
                    Note1_7 = information1_7[LENGTH-2]+" "+Note1_7;
                    LENGTH=LENGTH-1;
                }
            }else{
                if (information1_7[7].equals("NOTE")) {
                    Note1_7 = "";
                } else {
                    Note1_7 = information1_7[7];
                }
            }
        }
            JLabel name1_7 = new JLabel(Name1_7,SwingConstants.CENTER);
            name1_7.setPreferredSize(new Dimension(220, 45));
            name1_7.setOpaque(true);
            name1_7.setFont(new Font(Font.DIALOG, Font.PLAIN, 20));//设置文字字体
            name1_7.setForeground(Color.black);//设置文字的颜色
            name1_7.setBackground(new Color(232, 232, 232));
            gbc1.weightx = 0.2;    // 指定组件的分配区域
            gbc1.weighty = 0.09;
            gbc1.gridwidth = 1;
            gbc1.gridheight = 1;
            gbc1.gridx = 0;
            gbc1.gridy = 7;
            gbl1.setConstraints(name1_7, gbc1);
            middlePanel1.add(name1_7);
            JLabel state1_7 = new JLabel(State1_7, SwingConstants.CENTER);
            state1_7.setPreferredSize(new Dimension(220, 45));
            state1_7.setFont(new Font(Font.DIALOG, Font.PLAIN, 20));//设置文字字体
            state1_7.setForeground(Color.black);//设置文字的颜色
            state1_7.setOpaque(true);
            state1_7.setBackground(new Color(232, 232, 232));
            gbc1.weightx = 0.2;    // 指定组件的分配区域
            gbc1.weighty = 0.09;
            gbc1.gridwidth = 1;
            gbc1.gridheight = 1;
            gbc1.gridx = 1;
            gbc1.gridy = 7;
            gbl1.setConstraints(state1_7, gbc1);
            middlePanel1.add(state1_7);
            JLabel seats1_7 = new JLabel(Seats1_7, SwingConstants.CENTER);
            seats1_7.setPreferredSize(new Dimension(220, 45));
            seats1_7.setFont(new Font(Font.DIALOG, Font.PLAIN, 20));//设置文字字体
            seats1_7.setForeground(Color.black);//设置文字的颜色
            seats1_7.setOpaque(true);
            seats1_7.setBackground(new Color(232, 232, 232));
            gbc1.weightx = 0.2;    // 指定组件的分配区域
            gbc1.weighty = 0.09;
            gbc1.gridwidth = 1;
            gbc1.gridheight = 1;
            gbc1.gridx = 2;
            gbc1.gridy = 7;
            gbl1.setConstraints(seats1_7, gbc1);
            middlePanel1.add(seats1_7);
            JLabel foods1_7 = new JLabel(Foods1_7, SwingConstants.CENTER);
            foods1_7.setPreferredSize(new Dimension(220, 45));
            foods1_7.setFont(new Font(Font.DIALOG, Font.PLAIN, 20));//设置文字字体
            foods1_7.setForeground(Color.black);//设置文字的颜色
            foods1_7.setOpaque(true);
            foods1_7.setBackground(new Color(232, 232, 232));
            gbc1.weightx = 0.2;    // 指定组件的分配区域
            gbc1.weighty = 0.09;
            gbc1.gridwidth = 1;
            gbc1.gridheight = 1;
            gbc1.gridx = 3;
            gbc1.gridy = 7;
            gbl1.setConstraints(foods1_7, gbc1);
            middlePanel1.add(foods1_7);
            JLabel note1_7 = new JLabel(Note1_7, SwingConstants.CENTER);
            note1_7.setPreferredSize(new Dimension(220, 45));
            note1_7.setFont(new Font(Font.DIALOG, Font.PLAIN, 20));//设置文字字体
            note1_7.setForeground(Color.black);//设置文字的颜色
            note1_7.setOpaque(true);
            note1_7.setBackground(new Color(232, 232, 232));
            gbc1.weightx = 0.2;    // 指定组件的分配区域
            gbc1.weighty = 0.09;
            gbc1.gridwidth = 1;
            gbc1.gridheight = 1;
            gbc1.gridx = 4;
            gbc1.gridy = 7;
            gbl1.setConstraints(note1_7, gbc1);
            middlePanel1.add(note1_7);

        String Name1_8 = "";
        String State1_8 ="";
        String Seats1_8 ="";
        String Foods1_8 ="";
        String Note1_8 = "";
        if(passenger_num>=8) {
            String[] information1_8 = information.get(7).split("\\s+");
            Name1_8 = po.findName(information1_8[0]);
            State1_8 = information1_8[5];
            if(!information1_8[3].equals("LETTERUNCHOSEN")&&!information1_8[4].equals("NUMBERUNCHOSEN")) {
                Seats1_8 = information1_8[3] + information1_8[4];
            }
            if(!information1_8[2].equals("FOODUNCHOSEN")){
                Foods1_8 = information1_8[2];
            }
            if(information1_8.length>9) {
                LENGTH=information1_8.length;
                while(LENGTH>=9) {
                    Note1_8 = information1_8[LENGTH-2]+" "+Note1_8;
                    LENGTH=LENGTH-1;
                }
            }else{
                if (information1_8[7].equals("NOTE")) {
                    Note1_8 = "";
                } else {
                    Note1_8 = information1_8[7];
                }
            }
        }
            JLabel name1_8 = new JLabel(Name1_8,SwingConstants.CENTER);
            name1_8.setPreferredSize(new Dimension(220, 45));
            name1_8.setOpaque(true);
            name1_8.setFont(new Font(Font.DIALOG, Font.PLAIN, 20));//设置文字字体
            name1_8.setForeground(Color.black);//设置文字的颜色
            name1_8.setBackground(new Color(252, 250, 250));
            gbc1.weightx = 0.2;    // 指定组件的分配区域
            gbc1.weighty = 0.09;
            gbc1.gridwidth = 1;
            gbc1.gridheight = 1;
            gbc1.gridx = 0;
            gbc1.gridy = 8;
            gbl1.setConstraints(name1_8, gbc1);
            middlePanel1.add(name1_8);
            JLabel state1_8 = new JLabel(State1_8, SwingConstants.CENTER);
            state1_8.setPreferredSize(new Dimension(220, 45));
            state1_8.setFont(new Font(Font.DIALOG, Font.PLAIN, 20));//设置文字字体
            state1_8.setForeground(Color.black);//设置文字的颜色
            state1_8.setOpaque(true);
            state1_8.setBackground(new Color(252, 250, 250));
            gbc1.weightx = 0.2;    // 指定组件的分配区域
            gbc1.weighty = 0.09;
            gbc1.gridwidth = 1;
            gbc1.gridheight = 1;
            gbc1.gridx = 1;
            gbc1.gridy = 8;
            gbl1.setConstraints(state1_8, gbc1);
            middlePanel1.add(state1_8);
            JLabel seats1_8 = new JLabel(Seats1_8, SwingConstants.CENTER);
            seats1_8.setPreferredSize(new Dimension(220, 45));
            seats1_8.setFont(new Font(Font.DIALOG, Font.PLAIN, 20));//设置文字字体
            seats1_8.setForeground(Color.black);//设置文字的颜色
            seats1_8.setOpaque(true);
            seats1_8.setBackground(new Color(252, 250, 250));
            gbc1.weightx = 0.2;    // 指定组件的分配区域
            gbc1.weighty = 0.09;
            gbc1.gridwidth = 1;
            gbc1.gridheight = 1;
            gbc1.gridx = 2;
            gbc1.gridy = 8;
            gbl1.setConstraints(seats1_8, gbc1);
            middlePanel1.add(seats1_8);
            JLabel foods1_8 = new JLabel(Foods1_8, SwingConstants.CENTER);
            foods1_8.setPreferredSize(new Dimension(220, 45));
            foods1_8.setFont(new Font(Font.DIALOG, Font.PLAIN, 20));//设置文字字体
            foods1_8.setForeground(Color.black);//设置文字的颜色
            foods1_8.setOpaque(true);
            foods1_8.setBackground(new Color(252, 250, 250));
            gbc1.weightx = 0.2;    // 指定组件的分配区域
            gbc1.weighty = 0.09;
            gbc1.gridwidth = 1;
            gbc1.gridheight = 1;
            gbc1.gridx = 3;
            gbc1.gridy = 8;
            gbl1.setConstraints(foods1_8, gbc1);
            middlePanel1.add(foods1_8);
            JLabel note1_8 = new JLabel(Note1_8, SwingConstants.CENTER);
            note1_8.setPreferredSize(new Dimension(220, 45));
            note1_8.setFont(new Font(Font.DIALOG, Font.PLAIN, 20));//设置文字字体
            note1_8.setForeground(Color.black);//设置文字的颜色
            note1_8.setOpaque(true);
            note1_8.setBackground(new Color(252, 250, 250));
            gbc1.weightx = 0.2;    // 指定组件的分配区域
            gbc1.weighty = 0.09;
            gbc1.gridwidth = 1;
            gbc1.gridheight = 1;
            gbc1.gridx = 4;
            gbc1.gridy = 8;
            gbl1.setConstraints(note1_8, gbc1);
            middlePanel1.add(note1_8);

        String Name1_9 = "";
        String State1_9 ="";
        String Seats1_9 ="";
        String Foods1_9 ="";
        String Note1_9 = "";
        if(passenger_num>=9) {
            String[] information1_9 = information.get(8).split("\\s+");
            Name1_9 = po.findName(information1_9[0]);
            State1_9 = information1_9[5];
            if(!information1_9[3].equals("LETTERUNCHOSEN")&&!information1_9[4].equals("NUMBERUNCHOSEN")) {
                Seats1_9 = information1_9[3] + information1_9[4];
            }
            if(!information1_9[2].equals("FOODUNCHOSEN")){
                Foods1_9 = information1_9[2];
            }
            if(information1_9.length>9) {
                LENGTH=information1_9.length;
                while(LENGTH>=9) {
                    Note1_9 = information1_9[LENGTH-2]+" "+Note1_9;
                    LENGTH=LENGTH-1;
                }
            }else{
                if (information1_9[7].equals("NOTE")) {
                    Note1_9 = "";
                } else {
                    Note1_9 = information1_9[7];
                }
            }
        }
            JLabel name1_9 = new JLabel(Name1_9,SwingConstants.CENTER);
            name1_9.setPreferredSize(new Dimension(220, 45));
            name1_9.setOpaque(true);
            name1_9.setFont(new Font(Font.DIALOG, Font.PLAIN, 20));//设置文字字体
            name1_9.setForeground(Color.black);//设置文字的颜色
            name1_9.setBackground(new Color(232, 232, 232));
            gbc1.weightx = 0.2;    // 指定组件的分配区域
            gbc1.weighty = 0.09;
            gbc1.gridwidth = 1;
            gbc1.gridheight = 1;
            gbc1.gridx = 0;
            gbc1.gridy = 9;
            gbl1.setConstraints(name1_9, gbc1);
            middlePanel1.add(name1_9);
            JLabel state1_9 = new JLabel(State1_9, SwingConstants.CENTER);
            state1_9.setPreferredSize(new Dimension(220, 45));
            state1_9.setFont(new Font(Font.DIALOG, Font.PLAIN, 20));//设置文字字体
            state1_9.setForeground(Color.black);//设置文字的颜色
            state1_9.setOpaque(true);
            state1_9.setBackground(new Color(232, 232, 232));
            gbc1.weightx = 0.2;    // 指定组件的分配区域
            gbc1.weighty = 0.09;
            gbc1.gridwidth = 1;
            gbc1.gridheight = 1;
            gbc1.gridx = 1;
            gbc1.gridy = 9;
            gbl1.setConstraints(state1_9, gbc1);
            middlePanel1.add(state1_9);
            JLabel seats1_9 = new JLabel(Seats1_9, SwingConstants.CENTER);
            seats1_9.setPreferredSize(new Dimension(220, 45));
            seats1_9.setFont(new Font(Font.DIALOG, Font.PLAIN, 20));//设置文字字体
            seats1_9.setForeground(Color.black);//设置文字的颜色
            seats1_9.setOpaque(true);
            seats1_9.setBackground(new Color(232, 232, 232));
            gbc1.weightx = 0.2;    // 指定组件的分配区域
            gbc1.weighty = 0.09;
            gbc1.gridwidth = 1;
            gbc1.gridheight = 1;
            gbc1.gridx = 2;
            gbc1.gridy = 9;
            gbl1.setConstraints(seats1_9, gbc1);
            middlePanel1.add(seats1_9);
            JLabel foods1_9 = new JLabel(Foods1_9, SwingConstants.CENTER);
            foods1_9.setPreferredSize(new Dimension(220, 45));
            foods1_9.setFont(new Font(Font.DIALOG, Font.PLAIN, 20));//设置文字字体
            foods1_9.setForeground(Color.black);//设置文字的颜色
            foods1_9.setOpaque(true);
            foods1_9.setBackground(new Color(232, 232, 232));
            gbc1.weightx = 0.2;    // 指定组件的分配区域
            gbc1.weighty = 0.09;
            gbc1.gridwidth = 1;
            gbc1.gridheight = 1;
            gbc1.gridx = 3;
            gbc1.gridy = 9;
            gbl1.setConstraints(foods1_9, gbc1);
            middlePanel1.add(foods1_9);
            JLabel note1_9 = new JLabel(Note1_9, SwingConstants.CENTER);
            note1_9.setPreferredSize(new Dimension(220, 45));
            note1_9.setFont(new Font(Font.DIALOG, Font.PLAIN, 20));//设置文字字体
            note1_9.setForeground(Color.black);//设置文字的颜色
            note1_9.setOpaque(true);
            note1_9.setBackground(new Color(232, 232, 232));
            gbc1.weightx = 0.2;    // 指定组件的分配区域
            gbc1.weighty = 0.09;
            gbc1.gridwidth = 1;
            gbc1.gridheight = 1;
            gbc1.gridx = 4;
            gbc1.gridy = 9;
            gbl1.setConstraints(note1_9, gbc1);
            middlePanel1.add(note1_9);

        String Name1_10 = "";
        String State1_10 ="";
        String Seats1_10 ="";
        String Foods1_10 ="";
        String Note1_10 = "";
        if(passenger_num>=10) {
            String[] information1_10 = information.get(9).split("\\s+");
            Name1_10 = po.findName(information1_10[0]);
            State1_10 = information1_10[5];
            if(!information1_10[3].equals("LETTERUNCHOSEN")&&!information1_10[4].equals("NUMBERUNCHOSEN")) {
                Seats1_10 = information1_10[3] + information1_10[4];
            }
            if(!information1_10[2].equals("FOODUNCHOSEN")){
                Foods1_10 = information1_10[2];
            }
            if(information1_10.length>9) {
                LENGTH=information1_10.length;
                while(LENGTH>=9) {
                    Note1_10 = information1_10[LENGTH-2]+" "+Note1_10;
                    LENGTH=LENGTH-1;
                }
            }else{
                if (information1_10[7].equals("NOTE")) {
                    Note1_10 = "";
                } else {
                    Note1_10 = information1_10[7];
                }
            }
        }
            JLabel name1_10 = new JLabel(Name1_10,SwingConstants.CENTER);
            name1_10.setPreferredSize(new Dimension(220, 45));
            name1_10.setOpaque(true);
            name1_10.setFont(new Font(Font.DIALOG, Font.PLAIN, 20));//设置文字字体
            name1_10.setForeground(Color.black);//设置文字的颜色
            name1_10.setBackground(new Color(252, 250, 250));
            gbc1.weightx = 0.2;    // 指定组件的分配区域
            gbc1.weighty = 0.09;
            gbc1.gridwidth = 1;
            gbc1.gridheight = 1;
            gbc1.gridx = 0;
            gbc1.gridy = 10;
            gbl1.setConstraints(name1_10, gbc1);
            middlePanel1.add(name1_10);
            JLabel state1_10 = new JLabel(State1_10, SwingConstants.CENTER);
            state1_10.setPreferredSize(new Dimension(220, 45));
            state1_10.setFont(new Font(Font.DIALOG, Font.PLAIN, 20));//设置文字字体
            state1_10.setForeground(Color.black);//设置文字的颜色
            state1_10.setOpaque(true);
            state1_10.setBackground(new Color(252, 250, 250));
            gbc1.weightx = 0.2;    // 指定组件的分配区域
            gbc1.weighty = 0.09;
            gbc1.gridwidth = 1;
            gbc1.gridheight = 1;
            gbc1.gridx = 1;
            gbc1.gridy = 10;
            gbl1.setConstraints(state1_10, gbc1);
            middlePanel1.add(state1_10);
            JLabel seats1_10 = new JLabel(Seats1_10, SwingConstants.CENTER);
            seats1_10.setPreferredSize(new Dimension(220, 45));
            seats1_10.setFont(new Font(Font.DIALOG, Font.PLAIN, 20));//设置文字字体
            seats1_10.setForeground(Color.black);//设置文字的颜色
            seats1_10.setOpaque(true);
            seats1_10.setBackground(new Color(252, 250, 250));
            gbc1.weightx = 0.2;    // 指定组件的分配区域
            gbc1.weighty = 0.09;
            gbc1.gridwidth = 1;
            gbc1.gridheight = 1;
            gbc1.gridx = 2;
            gbc1.gridy = 10;
            gbl1.setConstraints(seats1_10, gbc1);
            middlePanel1.add(seats1_10);
            JLabel foods1_10 = new JLabel(Foods1_10, SwingConstants.CENTER);
            foods1_10.setPreferredSize(new Dimension(220, 45));
            foods1_10.setFont(new Font(Font.DIALOG, Font.PLAIN, 20));//设置文字字体
            foods1_10.setForeground(Color.black);//设置文字的颜色
            foods1_10.setOpaque(true);
            foods1_10.setBackground(new Color(252, 250, 250));
            gbc1.weightx = 0.2;    // 指定组件的分配区域
            gbc1.weighty = 0.09;
            gbc1.gridwidth = 1;
            gbc1.gridheight = 1;
            gbc1.gridx = 3;
            gbc1.gridy = 10;
            gbl1.setConstraints(foods1_10, gbc1);
            middlePanel1.add(foods1_10);
            JLabel note1_10 = new JLabel(Note1_10, SwingConstants.CENTER);
            note1_10.setPreferredSize(new Dimension(220, 45));
            note1_10.setFont(new Font(Font.DIALOG, Font.PLAIN, 20));//设置文字字体
            note1_10.setForeground(Color.black);//设置文字的颜色
            note1_10.setOpaque(true);
            note1_10.setBackground(new Color(252, 250, 250));
            gbc1.weightx = 0.2;    // 指定组件的分配区域
            gbc1.weighty = 0.09;
            gbc1.gridwidth = 1;
            gbc1.gridheight = 1;
            gbc1.gridx = 4;
            gbc1.gridy = 10;
            gbl1.setConstraints(note1_10, gbc1);
            middlePanel1.add(note1_10);

            p1.add(middlePanel1);

            panel.add(p1,"1");

        JPanel p2 = new JPanel();
        p2.setBackground(new Color(72,46,115));
        p2.setPreferredSize(new Dimension(1200,680));
        p2.setLayout(null);


        JLabel Head2 = new JLabel(Flight_num);
        Head2.setBounds(450,10,300,50);
        //外观设计
        Head2.setFont(new Font(Font.DIALOG,Font.BOLD,29));//设置文字字体
        Head2.setForeground(Color.white);//设置文字的颜色
        Head2.setOpaque(true);
        Head2.setBackground(new Color(96, 62,151));
        Head2.setPreferredSize(new Dimension(300,50));
        Head2.setHorizontalAlignment(JLabel.CENTER);

        p2.add(Head2);

        JButton button_left = new JButton("LAST");
        button_left.setFont(new Font(Font.DIALOG,Font.BOLD,29));//设置文字字体
        button_left.setForeground(Color.white);//设置文字的颜色
        button_left.setBackground(new Color(96, 62,151));
        button_left.setSize(200,50);
        button_left.setVisible(false);
        JLabel label_left = new JLabel();
        label_left.setBounds(200,10,200,50);
        //外观设计
        label_left.setOpaque(true);
        label_left.setBackground(new Color(96, 62,151));
        label_left.setHorizontalAlignment(JLabel.CENTER);
        label_left.add(button_left);

        p2.add(label_left);


        JPanel middlePanel2 = new JPanel();
        middlePanel2.setBounds(50,80,1100,500);


        GridBagLayout gbl2 = new GridBagLayout();
        GridBagConstraints gbc2 = new GridBagConstraints();
        middlePanel2.setLayout(gbl2);
        gbc2.fill=GridBagConstraints.BOTH;


        JLabel information21 = new JLabel("Name",SwingConstants.CENTER);
        information21.setPreferredSize(new Dimension(220, 50));
        information21.setOpaque(true);
        information21.setFont(new Font(Font.DIALOG, Font.PLAIN, 20));//设置文字字体
        information21.setForeground(Color.black);//设置文字的颜色
        information21.setBackground(new Color(252, 250, 250));
        gbc2.weightx = 0.2;    // 指定组件的分配区域
        gbc2.weighty = 0.1;
        gbc2.gridwidth = 1;
        gbc2.gridheight = 1;
        gbc2.gridx = 0;
        gbc2.gridy = 0;
        gbl2.setConstraints(information21, gbc2);
        middlePanel2.add(information21);
        JLabel information22 = new JLabel("State", SwingConstants.CENTER);
        information22.setPreferredSize(new Dimension(220, 50));
        information22.setFont(new Font(Font.DIALOG, Font.PLAIN, 20));//设置文字字体
        information22.setForeground(Color.black);//设置文字的颜色
        information22.setOpaque(true);
        information22.setBackground(new Color(252, 250, 250));
        gbc2.weightx = 0.2;    // 指定组件的分配区域
        gbc2.weighty = 0.1;
        gbc2.gridwidth = 1;
        gbc2.gridheight = 1;
        gbc2.gridx = 1;
        gbc2.gridy = 0;
        gbl2.setConstraints(information22, gbc2);
        middlePanel2.add(information22);
        JLabel information23 = new JLabel("Seats", SwingConstants.CENTER);
        information23.setPreferredSize(new Dimension(220, 50));
        information23.setFont(new Font(Font.DIALOG, Font.PLAIN, 20));//设置文字字体
        information23.setForeground(Color.black);//设置文字的颜色
        information23.setOpaque(true);
        information23.setBackground(new Color(252, 250, 250));
        gbc2.weightx = 0.2;    // 指定组件的分配区域
        gbc2.weighty = 0.1;
        gbc2.gridwidth = 1;
        gbc2.gridheight = 1;
        gbc2.gridx = 2;
        gbc2.gridy = 0;
        gbl2.setConstraints(information23, gbc2);
        middlePanel2.add(information23);
        JLabel information24 = new JLabel("Foods", SwingConstants.CENTER);
        information24.setPreferredSize(new Dimension(220, 50));
        information24.setFont(new Font(Font.DIALOG, Font.PLAIN, 20));//设置文字字体
        information24.setForeground(Color.black);//设置文字的颜色
        information24.setOpaque(true);
        information24.setBackground(new Color(252, 250, 250));
        gbc2.weightx = 0.2;    // 指定组件的分配区域
        gbc2.weighty = 0.1;
        gbc2.gridwidth = 1;
        gbc2.gridheight = 1;
        gbc2.gridx = 3;
        gbc2.gridy = 0;
        gbl2.setConstraints(information24, gbc2);
        middlePanel2.add(information24);
        JLabel information25 = new JLabel("Note", SwingConstants.CENTER);
        information25.setPreferredSize(new Dimension(220, 50));
        information25.setFont(new Font(Font.DIALOG, Font.PLAIN, 20));//设置文字字体
        information25.setForeground(Color.black);//设置文字的颜色
        information25.setOpaque(true);
        information25.setBackground(new Color(252, 250, 250));
        gbc2.weightx = 0.2;    // 指定组件的分配区域
        gbc2.weighty = 0.1;
        gbc2.gridwidth = 1;
        gbc2.gridheight = 1;
        gbc2.gridx = 4;
        gbc2.gridy = 0;
        gbl2.setConstraints(information25, gbc2);
        middlePanel2.add(information25);



        String Name2_1 = "";
        String State2_1 ="";
        String Seats2_1 ="";
        String Foods2_1 ="";
        String Note2_1 = "";
        if(passenger_num>=11) {
            String[] information2_1 = information.get(10).split("\\s+");
            Name2_1 = po.findName(information2_1[0]);
            State2_1 = information2_1[5];
            if(!information2_1[3].equals("LETTERUNCHOSEN")&&!information2_1[4].equals("NUMBERUNCHOSEN")) {
                Seats2_1 = information2_1[3] + information2_1[4];
            }
            if(!information2_1[2].equals("FOODUNCHOSEN")){
                Foods2_1 = information2_1[2];
            }
            if(information2_1.length>9) {
                LENGTH=information2_1.length;
                while(LENGTH>=9) {
                    Note2_1 = information2_1[LENGTH-2]+" "+Note2_1;
                    LENGTH=LENGTH-1;
                }
            }else{
                if (information2_1[7].equals("NOTE")) {
                    Note2_1 = "";
                } else {
                    Note2_1 = information2_1[7];
                }
            }
        }
        JLabel name2_1 = new JLabel(Name2_1,SwingConstants.CENTER);
        name2_1.setPreferredSize(new Dimension(220, 45));
        name2_1.setOpaque(true);
        name2_1.setFont(new Font(Font.DIALOG, Font.PLAIN, 20));//设置文字字体
        name2_1.setForeground(Color.black);//设置文字的颜色
        name2_1.setBackground(new Color(232, 232, 232));
        gbc2.weightx = 0.2;    // 指定组件的分配区域
        gbc2.weighty = 0.09;
        gbc2.gridwidth = 1;
        gbc2.gridheight = 1;
        gbc2.gridx = 0;
        gbc2.gridy = 1;
        gbl2.setConstraints(name2_1, gbc2);
        middlePanel2.add(name2_1);
        JLabel state2_1 = new JLabel(State2_1, SwingConstants.CENTER);
        state2_1.setPreferredSize(new Dimension(220, 45));
        state2_1.setFont(new Font(Font.DIALOG, Font.PLAIN, 20));//设置文字字体
        state2_1.setForeground(Color.black);//设置文字的颜色
        state2_1.setOpaque(true);
        state2_1.setBackground(new Color(232, 232, 232));
        gbc2.weightx = 0.2;    // 指定组件的分配区域
        gbc2.weighty = 0.09;
        gbc2.gridwidth = 1;
        gbc2.gridheight = 1;
        gbc2.gridx = 1;
        gbc2.gridy = 1;
        gbl2.setConstraints(state2_1, gbc2);
        middlePanel2.add(state2_1);
        JLabel seats2_1 = new JLabel(Seats2_1, SwingConstants.CENTER);
        seats2_1.setPreferredSize(new Dimension(220, 45));
        seats2_1.setFont(new Font(Font.DIALOG, Font.PLAIN, 20));//设置文字字体
        seats2_1.setForeground(Color.black);//设置文字的颜色
        seats2_1.setOpaque(true);
        seats2_1.setBackground(new Color(232, 232, 232));
        gbc2.weightx = 0.2;    // 指定组件的分配区域
        gbc2.weighty = 0.09;
        gbc2.gridwidth = 1;
        gbc2.gridheight = 1;
        gbc2.gridx = 2;
        gbc2.gridy = 1;
        gbl2.setConstraints(seats2_1, gbc2);
        middlePanel2.add(seats2_1);
        JLabel foods2_1 = new JLabel(Foods2_1, SwingConstants.CENTER);
        foods2_1.setPreferredSize(new Dimension(220, 45));
        foods2_1.setFont(new Font(Font.DIALOG, Font.PLAIN, 20));//设置文字字体
        foods2_1.setForeground(Color.black);//设置文字的颜色
        foods2_1.setOpaque(true);
        foods2_1.setBackground(new Color(232, 232, 232));
        gbc2.weightx = 0.2;    // 指定组件的分配区域
        gbc2.weighty = 0.09;
        gbc2.gridwidth = 1;
        gbc2.gridheight = 1;
        gbc2.gridx = 3;
        gbc2.gridy = 1;
        gbl2.setConstraints(foods2_1, gbc2);
        middlePanel2.add(foods2_1);
        JLabel note2_1 = new JLabel(Note2_1, SwingConstants.CENTER);
        note2_1.setPreferredSize(new Dimension(220, 45));
        note2_1.setFont(new Font(Font.DIALOG, Font.PLAIN, 20));//设置文字字体
        note2_1.setForeground(Color.black);//设置文字的颜色
        note2_1.setOpaque(true);
        note2_1.setBackground(new Color(232, 232, 232));
        gbc2.weightx = 0.2;    // 指定组件的分配区域
        gbc2.weighty = 0.09;
        gbc2.gridwidth = 1;
        gbc2.gridheight = 1;
        gbc2.gridx = 4;
        gbc2.gridy = 1;
        gbl2.setConstraints(note2_1, gbc2);
        middlePanel2.add(note2_1);

        String Name2_2 = "";
        String State2_2 ="";
        String Seats2_2 ="";
        String Foods2_2 ="";
        String Note2_2 = "";
        if(passenger_num>=12) {
            String[] information2_2 = information.get(11).split("\\s+");
            Name2_2 = po.findName(information2_2[0]);
            State2_2 = information2_2[5];
            if(!information2_2[3].equals("LETTERUNCHOSEN")&&!information2_2[4].equals("NUMBERUNCHOSEN")) {
                Seats2_2 = information2_2[3] + information2_2[4];
            }
            if(!information2_2[2].equals("FOODUNCHOSEN")){
                Foods2_2 = information2_2[2];
            }
            if(information2_2.length>9) {
                LENGTH=information2_2.length;
                while(LENGTH>=9) {
                    Note2_2 = information2_2[LENGTH-2]+" "+Note2_2;
                    LENGTH=LENGTH-1;
                }
            }else{
                if (information2_2[7].equals("NOTE")) {
                    Note2_2 = "";
                } else {
                    Note2_2 = information2_2[7];
                }
            }
        }
        JLabel name2_2 = new JLabel(Name2_2,SwingConstants.CENTER);
        name2_2.setPreferredSize(new Dimension(220, 45));
        name2_2.setOpaque(true);
        name2_2.setFont(new Font(Font.DIALOG, Font.PLAIN, 20));//设置文字字体
        name2_2.setForeground(Color.black);//设置文字的颜色
        name2_2.setBackground(new Color(252, 250, 250));
        gbc2.weightx = 0.2;    // 指定组件的分配区域
        gbc2.weighty = 0.09;
        gbc2.gridwidth = 1;
        gbc2.gridheight = 1;
        gbc2.gridx = 0;
        gbc2.gridy = 2;
        gbl2.setConstraints(name2_2, gbc2);
        middlePanel2.add(name2_2);
        JLabel state2_2 = new JLabel(State2_2, SwingConstants.CENTER);
        state2_2.setPreferredSize(new Dimension(220, 45));
        state2_2.setFont(new Font(Font.DIALOG, Font.PLAIN, 20));//设置文字字体
        state2_2.setForeground(Color.black);//设置文字的颜色
        state2_2.setOpaque(true);
        state2_2.setBackground(new Color(252, 250, 250));
        gbc2.weightx = 0.2;    // 指定组件的分配区域
        gbc2.weighty = 0.09;
        gbc2.gridwidth = 1;
        gbc2.gridheight = 1;
        gbc2.gridx = 1;
        gbc2.gridy = 2;
        gbl2.setConstraints(state2_2, gbc2);
        middlePanel2.add(state2_2);
        JLabel seats2_2 = new JLabel(Seats2_2, SwingConstants.CENTER);
        seats2_2.setPreferredSize(new Dimension(220, 45));
        seats2_2.setFont(new Font(Font.DIALOG, Font.PLAIN, 20));//设置文字字体
        seats2_2.setForeground(Color.black);//设置文字的颜色
        seats2_2.setOpaque(true);
        seats2_2.setBackground(new Color(252, 250, 250));
        gbc2.weightx = 0.2;    // 指定组件的分配区域
        gbc2.weighty = 0.09;
        gbc2.gridwidth = 1;
        gbc2.gridheight = 1;
        gbc2.gridx = 2;
        gbc2.gridy = 2;
        gbl2.setConstraints(seats2_2, gbc2);
        middlePanel2.add(seats2_2);
        JLabel foods2_2 = new JLabel(Foods2_2, SwingConstants.CENTER);
        foods2_2.setPreferredSize(new Dimension(220, 45));
        foods2_2.setFont(new Font(Font.DIALOG, Font.PLAIN, 20));//设置文字字体
        foods2_2.setForeground(Color.black);//设置文字的颜色
        foods2_2.setOpaque(true);
        foods2_2.setBackground(new Color(252, 250, 250));
        gbc2.weightx = 0.2;    // 指定组件的分配区域
        gbc2.weighty = 0.09;
        gbc2.gridwidth = 1;
        gbc2.gridheight = 1;
        gbc2.gridx = 3;
        gbc2.gridy = 2;
        gbl2.setConstraints(foods2_2, gbc2);
        middlePanel2.add(foods2_2);
        JLabel note2_2 = new JLabel(Note2_2, SwingConstants.CENTER);
        note2_2.setPreferredSize(new Dimension(220, 45));
        note2_2.setFont(new Font(Font.DIALOG, Font.PLAIN, 20));//设置文字字体
        note2_2.setForeground(Color.black);//设置文字的颜色
        note2_2.setOpaque(true);
        note2_2.setBackground(new Color(252, 250, 250));
        gbc2.weightx = 0.2;    // 指定组件的分配区域
        gbc2.weighty = 0.09;
        gbc2.gridwidth = 1;
        gbc2.gridheight = 1;
        gbc2.gridx = 4;
        gbc2.gridy = 2;
        gbl2.setConstraints(note2_2, gbc2);
        middlePanel2.add(note2_2);

        String Name2_3 = "";
        String State2_3 ="";
        String Seats2_3 ="";
        String Foods2_3 ="";
        String Note2_3 = "";
        if(passenger_num>=13) {
            String[] information2_3 = information.get(12).split("\\s+");
            Name2_3 = po.findName(information2_3[0]);
            State2_3 = information2_3[5];
            if(!information2_3[3].equals("LETTERUNCHOSEN")&&!information2_3[4].equals("NUMBERUNCHOSEN")) {
                Seats2_3 = information2_3[3] + information2_3[4];
            }
            if(!information2_3[2].equals("FOODUNCHOSEN")){
                Foods2_3 = information2_3[2];
            }
            if(information2_3.length>9) {
                LENGTH=information2_3.length;
                while(LENGTH>=9) {
                    Note2_3 = information2_3[LENGTH-2]+" "+Note2_3;
                    LENGTH=LENGTH-1;
                }
            }else{
                if (information2_3[7].equals("NOTE")) {
                    Note2_3 = "";
                } else {
                    Note2_3 = information2_3[7];
                }
            }
        }
        JLabel name2_3 = new JLabel(Name2_3,SwingConstants.CENTER);
        name2_3.setPreferredSize(new Dimension(220, 45));
        name2_3.setOpaque(true);
        name2_3.setFont(new Font(Font.DIALOG, Font.PLAIN, 20));//设置文字字体
        name2_3.setForeground(Color.black);//设置文字的颜色
        name2_3.setBackground(new Color(232, 232, 232));
        gbc2.weightx = 0.2;    // 指定组件的分配区域
        gbc2.weighty = 0.09;
        gbc2.gridwidth = 1;
        gbc2.gridheight = 1;
        gbc2.gridx = 0;
        gbc2.gridy = 3;
        gbl2.setConstraints(name2_3, gbc2);
        middlePanel2.add(name2_3);
        JLabel state2_3 = new JLabel(State2_3, SwingConstants.CENTER);
        state2_3.setPreferredSize(new Dimension(220, 45));
        state2_3.setFont(new Font(Font.DIALOG, Font.PLAIN, 20));//设置文字字体
        state2_3.setForeground(Color.black);//设置文字的颜色
        state2_3.setOpaque(true);
        state2_3.setBackground(new Color(232, 232, 232));
        gbc2.weightx = 0.2;    // 指定组件的分配区域
        gbc2.weighty = 0.09;
        gbc2.gridwidth = 1;
        gbc2.gridheight = 1;
        gbc2.gridx = 1;
        gbc2.gridy = 3;
        gbl2.setConstraints(state2_3, gbc2);
        middlePanel2.add(state2_3);
        JLabel seats2_3 = new JLabel(Seats2_3, SwingConstants.CENTER);
        seats2_3.setPreferredSize(new Dimension(220, 45));
        seats2_3.setFont(new Font(Font.DIALOG, Font.PLAIN, 20));//设置文字字体
        seats2_3.setForeground(Color.black);//设置文字的颜色
        seats2_3.setOpaque(true);
        seats2_3.setBackground(new Color(232, 232, 232));
        gbc2.weightx = 0.2;    // 指定组件的分配区域
        gbc2.weighty = 0.09;
        gbc2.gridwidth = 1;
        gbc2.gridheight = 1;
        gbc2.gridx = 2;
        gbc2.gridy = 3;
        gbl2.setConstraints(seats2_3, gbc2);
        middlePanel2.add(seats2_3);
        JLabel foods2_3 = new JLabel(Foods2_3, SwingConstants.CENTER);
        foods2_3.setPreferredSize(new Dimension(220, 45));
        foods2_3.setFont(new Font(Font.DIALOG, Font.PLAIN, 20));//设置文字字体
        foods2_3.setForeground(Color.black);//设置文字的颜色
        foods2_3.setOpaque(true);
        foods2_3.setBackground(new Color(232, 232, 232));
        gbc2.weightx = 0.2;    // 指定组件的分配区域
        gbc2.weighty = 0.09;
        gbc2.gridwidth = 1;
        gbc2.gridheight = 1;
        gbc2.gridx = 3;
        gbc2.gridy = 3;
        gbl2.setConstraints(foods2_3, gbc2);
        middlePanel2.add(foods2_3);
        JLabel note2_3 = new JLabel(Note2_3, SwingConstants.CENTER);
        note2_3.setPreferredSize(new Dimension(220, 45));
        note2_3.setFont(new Font(Font.DIALOG, Font.PLAIN, 20));//设置文字字体
        note2_3.setForeground(Color.black);//设置文字的颜色
        note2_3.setOpaque(true);
        note2_3.setBackground(new Color(232, 232, 232));
        gbc2.weightx = 0.2;    // 指定组件的分配区域
        gbc2.weighty = 0.09;
        gbc2.gridwidth = 1;
        gbc2.gridheight = 1;
        gbc2.gridx = 4;
        gbc2.gridy = 3;
        gbl2.setConstraints(note2_3, gbc2);
        middlePanel2.add(note2_3);

        String Name2_4 = "";
        String State2_4 ="";
        String Seats2_4 ="";
        String Foods2_4 ="";
        String Note2_4 = "";
        if(passenger_num>=14) {
            String[] information2_4 = information.get(13).split("\\s+");
            Name2_4 = po.findName(information2_4[0]);
            State2_4 = information2_4[5];
            if(!information2_4[3].equals("LETTERUNCHOSEN")&&!information2_4[4].equals("NUMBERUNCHOSEN")) {
                Seats2_4 = information2_4[3] + information2_4[4];
            }
            if(!information2_4[2].equals("FOODUNCHOSEN")){
                Foods2_4 = information2_4[2];
            }
            if(information2_4.length>9) {
                LENGTH=information2_4.length;
                while(LENGTH>=9) {
                    Note2_4 = information2_4[LENGTH-2]+" "+Note2_4;
                    LENGTH=LENGTH-1;
                }
            }else{
                if (information2_4[7].equals("NOTE")) {
                    Note2_4 = "";
                } else {
                    Note2_4 = information2_4[7];
                }
            }
        }
        JLabel name2_4 = new JLabel(Name2_4,SwingConstants.CENTER);
        name2_4.setPreferredSize(new Dimension(220, 45));
        name2_4.setOpaque(true);
        name2_4.setFont(new Font(Font.DIALOG, Font.PLAIN, 20));//设置文字字体
        name2_4.setForeground(Color.black);//设置文字的颜色
        name2_4.setBackground(new Color(252, 250, 250));
        gbc2.weightx = 0.2;    // 指定组件的分配区域
        gbc2.weighty = 0.09;
        gbc2.gridwidth = 1;
        gbc2.gridheight = 1;
        gbc2.gridx = 0;
        gbc2.gridy = 4;
        gbl2.setConstraints(name2_4, gbc2);
        middlePanel2.add(name2_4);
        JLabel state2_4 = new JLabel(State2_4, SwingConstants.CENTER);
        state2_4.setPreferredSize(new Dimension(220, 45));
        state2_4.setFont(new Font(Font.DIALOG, Font.PLAIN, 20));//设置文字字体
        state2_4.setForeground(Color.black);//设置文字的颜色
        state2_4.setOpaque(true);
        state2_4.setBackground(new Color(252, 250, 250));
        gbc2.weightx = 0.2;    // 指定组件的分配区域
        gbc2.weighty = 0.09;
        gbc2.gridwidth = 1;
        gbc2.gridheight = 1;
        gbc2.gridx = 1;
        gbc2.gridy = 4;
        gbl2.setConstraints(state2_4, gbc2);
        middlePanel2.add(state2_4);
        JLabel seats2_4 = new JLabel(Seats2_4, SwingConstants.CENTER);
        seats2_4.setPreferredSize(new Dimension(220, 45));
        seats2_4.setFont(new Font(Font.DIALOG, Font.PLAIN, 20));//设置文字字体
        seats2_4.setForeground(Color.black);//设置文字的颜色
        seats2_4.setOpaque(true);
        seats2_4.setBackground(new Color(252, 250, 250));
        gbc2.weightx = 0.2;    // 指定组件的分配区域
        gbc2.weighty = 0.09;
        gbc2.gridwidth = 1;
        gbc2.gridheight = 1;
        gbc2.gridx = 2;
        gbc2.gridy = 4;
        gbl2.setConstraints(seats2_4, gbc2);
        middlePanel2.add(seats2_4);
        JLabel foods2_4 = new JLabel(Foods2_4, SwingConstants.CENTER);
        foods2_4.setPreferredSize(new Dimension(220, 45));
        foods2_4.setFont(new Font(Font.DIALOG, Font.PLAIN, 20));//设置文字字体
        foods2_4.setForeground(Color.black);//设置文字的颜色
        foods2_4.setOpaque(true);
        foods2_4.setBackground(new Color(252, 250, 250));
        gbc2.weightx = 0.2;    // 指定组件的分配区域
        gbc2.weighty = 0.09;
        gbc2.gridwidth = 1;
        gbc2.gridheight = 1;
        gbc2.gridx = 3;
        gbc2.gridy = 4;
        gbl2.setConstraints(foods2_4, gbc2);
        middlePanel2.add(foods2_4);
        JLabel note2_4 = new JLabel(Note2_4, SwingConstants.CENTER);
        note2_4.setPreferredSize(new Dimension(220, 45));
        note2_4.setFont(new Font(Font.DIALOG, Font.PLAIN, 20));//设置文字字体
        note2_4.setForeground(Color.black);//设置文字的颜色
        note2_4.setOpaque(true);
        note2_4.setBackground(new Color(252, 250, 250));
        gbc2.weightx = 0.2;    // 指定组件的分配区域
        gbc2.weighty = 0.09;
        gbc2.gridwidth = 1;
        gbc2.gridheight = 1;
        gbc2.gridx = 4;
        gbc2.gridy = 4;
        gbl2.setConstraints(note2_4, gbc2);
        middlePanel2.add(note2_4);

        String Name2_5 = "";
        String State2_5 ="";
        String Seats2_5 ="";
        String Foods2_5 ="";
        String Note2_5 = "";
        if(passenger_num>=15) {
            String[] information2_5 = information.get(14).split("\\s+");
            Name2_5 = po.findName(information2_5[0]);
            State2_5 = information2_5[5];
            if(!information2_5[3].equals("LETTERUNCHOSEN")&&!information2_5[4].equals("NUMBERUNCHOSEN")) {
                Seats2_5 = information2_5[3] + information2_5[4];
            }
            if(!information2_5[2].equals("FOODUNCHOSEN")){
                Foods2_5 = information2_5[2];
            }
            if(information2_5.length>9) {
                LENGTH=information2_5.length;
                while(LENGTH>=9) {
                    Note2_5 = information2_5[LENGTH-2]+" "+Note2_5;
                    LENGTH=LENGTH-1;
                }
            }else{
                if (information2_5[7].equals("NOTE")) {
                    Note2_5 = "";
                } else {
                    Note2_5 = information2_5[7];
                }
            }
        }
        JLabel name2_5 = new JLabel(Name2_5,SwingConstants.CENTER);
        name2_5.setPreferredSize(new Dimension(220, 45));
        name2_5.setOpaque(true);
        name2_5.setFont(new Font(Font.DIALOG, Font.PLAIN, 20));//设置文字字体
        name2_5.setForeground(Color.black);//设置文字的颜色
        name2_5.setBackground(new Color(232, 232, 232));
        gbc2.weightx = 0.2;    // 指定组件的分配区域
        gbc2.weighty = 0.09;
        gbc2.gridwidth = 1;
        gbc2.gridheight = 1;
        gbc2.gridx = 0;
        gbc2.gridy = 5;
        gbl2.setConstraints(name2_5, gbc2);
        middlePanel2.add(name2_5);
        JLabel state2_5 = new JLabel(State2_5, SwingConstants.CENTER);
        state2_5.setPreferredSize(new Dimension(220, 45));
        state2_5.setFont(new Font(Font.DIALOG, Font.PLAIN, 20));//设置文字字体
        state2_5.setForeground(Color.black);//设置文字的颜色
        state2_5.setOpaque(true);
        state2_5.setBackground(new Color(232, 232, 232));
        gbc2.weightx = 0.2;    // 指定组件的分配区域
        gbc2.weighty = 0.09;
        gbc2.gridwidth = 1;
        gbc2.gridheight = 1;
        gbc2.gridx = 1;
        gbc2.gridy = 5;
        gbl2.setConstraints(state2_5, gbc2);
        middlePanel2.add(state2_5);
        JLabel seats2_5 = new JLabel(Seats2_5, SwingConstants.CENTER);
        seats2_5.setPreferredSize(new Dimension(220, 45));
        seats2_5.setFont(new Font(Font.DIALOG, Font.PLAIN, 20));//设置文字字体
        seats2_5.setForeground(Color.black);//设置文字的颜色
        seats2_5.setOpaque(true);
        seats2_5.setBackground(new Color(232, 232, 232));
        gbc2.weightx = 0.2;    // 指定组件的分配区域
        gbc2.weighty = 0.09;
        gbc2.gridwidth = 1;
        gbc2.gridheight = 1;
        gbc2.gridx = 2;
        gbc2.gridy = 5;
        gbl2.setConstraints(seats2_5, gbc2);
        middlePanel2.add(seats2_5);
        JLabel foods2_5 = new JLabel(Foods2_5, SwingConstants.CENTER);
        foods2_5.setPreferredSize(new Dimension(220, 45));
        foods2_5.setFont(new Font(Font.DIALOG, Font.PLAIN, 20));//设置文字字体
        foods2_5.setForeground(Color.black);//设置文字的颜色
        foods2_5.setOpaque(true);
        foods2_5.setBackground(new Color(232, 232, 232));
        gbc2.weightx = 0.2;    // 指定组件的分配区域
        gbc2.weighty = 0.09;
        gbc2.gridwidth = 1;
        gbc2.gridheight = 1;
        gbc2.gridx = 3;
        gbc2.gridy = 5;
        gbl2.setConstraints(foods2_5, gbc2);
        middlePanel2.add(foods2_5);
        JLabel note2_5 = new JLabel(Note2_5, SwingConstants.CENTER);
        note2_5.setPreferredSize(new Dimension(220, 45));
        note2_5.setFont(new Font(Font.DIALOG, Font.PLAIN, 20));//设置文字字体
        note2_5.setForeground(Color.black);//设置文字的颜色
        note2_5.setOpaque(true);
        note2_5.setBackground(new Color(232, 232, 232));
        gbc2.weightx = 0.2;    // 指定组件的分配区域
        gbc2.weighty = 0.09;
        gbc2.gridwidth = 1;
        gbc2.gridheight = 1;
        gbc2.gridx = 4;
        gbc2.gridy = 5;
        gbl2.setConstraints(note2_5, gbc2);
        middlePanel2.add(note2_5);

        String Name2_6 = "";
        String State2_6 ="";
        String Seats2_6 ="";
        String Foods2_6 ="";
        String Note2_6 = "";
        if(passenger_num>=16) {
            String[] information2_6 = information.get(15).split("\\s+");
            Name2_6 = po.findName(information2_6[0]);
            State2_6 = information2_6[5];
            if(!information2_6[3].equals("LETTERUNCHOSEN")&&!information2_6[4].equals("NUMBERUNCHOSEN")) {
                Seats2_6 = information2_6[3] + information2_6[4];
            }
            if(!information2_6[2].equals("FOODUNCHOSEN")){
                Foods2_6 = information2_6[2];
            }
            if(information2_6.length>9) {
                LENGTH=information2_6.length;
                while(LENGTH>=9) {
                    Note2_6 = information2_6[LENGTH-2]+" "+Note2_6;
                    LENGTH=LENGTH-1;
                }
            }else{
                if (information2_6[7].equals("NOTE")) {
                    Note2_6 = "";
                } else {
                    Note2_6 = information2_6[7];
                }
            }
        }
        JLabel name2_6 = new JLabel(Name2_6,SwingConstants.CENTER);
        name2_6.setPreferredSize(new Dimension(220, 45));
        name2_6.setOpaque(true);
        name2_6.setFont(new Font(Font.DIALOG, Font.PLAIN, 20));//设置文字字体
        name2_6.setForeground(Color.black);//设置文字的颜色
        name2_6.setBackground(new Color(252, 250, 250));
        gbc2.weightx = 0.2;    // 指定组件的分配区域
        gbc2.weighty = 0.09;
        gbc2.gridwidth = 1;
        gbc2.gridheight = 1;
        gbc2.gridx = 0;
        gbc2.gridy = 6;
        gbl2.setConstraints(name2_6, gbc2);
        middlePanel2.add(name2_6);
        JLabel state2_6 = new JLabel(State2_6, SwingConstants.CENTER);
        state2_6.setPreferredSize(new Dimension(220, 45));
        state2_6.setFont(new Font(Font.DIALOG, Font.PLAIN, 20));//设置文字字体
        state2_6.setForeground(Color.black);//设置文字的颜色
        state2_6.setOpaque(true);
        state2_6.setBackground(new Color(252, 250, 250));
        gbc2.weightx = 0.2;    // 指定组件的分配区域
        gbc2.weighty = 0.09;
        gbc2.gridwidth = 1;
        gbc2.gridheight = 1;
        gbc2.gridx = 1;
        gbc2.gridy = 6;
        gbl2.setConstraints(state2_6, gbc2);
        middlePanel2.add(state2_6);
        JLabel seats2_6 = new JLabel(Seats2_6, SwingConstants.CENTER);
        seats2_6.setPreferredSize(new Dimension(220, 45));
        seats2_6.setFont(new Font(Font.DIALOG, Font.PLAIN, 20));//设置文字字体
        seats2_6.setForeground(Color.black);//设置文字的颜色
        seats2_6.setOpaque(true);
        seats2_6.setBackground(new Color(252, 250, 250));
        gbc2.weightx = 0.2;    // 指定组件的分配区域
        gbc2.weighty = 0.09;
        gbc2.gridwidth = 1;
        gbc2.gridheight = 1;
        gbc2.gridx = 2;
        gbc2.gridy = 6;
        gbl2.setConstraints(seats2_6, gbc2);
        middlePanel2.add(seats2_6);
        JLabel foods2_6 = new JLabel(Foods2_6, SwingConstants.CENTER);
        foods2_6.setPreferredSize(new Dimension(220, 45));
        foods2_6.setFont(new Font(Font.DIALOG, Font.PLAIN, 20));//设置文字字体
        foods2_6.setForeground(Color.black);//设置文字的颜色
        foods2_6.setOpaque(true);
        foods2_6.setBackground(new Color(252, 250, 250));
        gbc2.weightx = 0.2;    // 指定组件的分配区域
        gbc2.weighty = 0.09;
        gbc2.gridwidth = 1;
        gbc2.gridheight = 1;
        gbc2.gridx = 3;
        gbc2.gridy = 6;
        gbl2.setConstraints(foods2_6, gbc2);
        middlePanel2.add(foods2_6);
        JLabel note2_6 = new JLabel(Note2_6, SwingConstants.CENTER);
        note2_6.setPreferredSize(new Dimension(220, 45));
        note2_6.setFont(new Font(Font.DIALOG, Font.PLAIN, 20));//设置文字字体
        note2_6.setForeground(Color.black);//设置文字的颜色
        note2_6.setOpaque(true);
        note2_6.setBackground(new Color(252, 250, 250));
        gbc2.weightx = 0.2;    // 指定组件的分配区域
        gbc2.weighty = 0.09;
        gbc2.gridwidth = 1;
        gbc2.gridheight = 1;
        gbc2.gridx = 4;
        gbc2.gridy = 6;
        gbl2.setConstraints(note2_6, gbc2);
        middlePanel2.add(note2_6);


        String Name2_7 = "";
        String State2_7 ="";
        String Seats2_7 ="";
        String Foods2_7 ="";
        String Note2_7 = "";
        if(passenger_num>=17) {
            String[] information2_7 = information.get(16).split("\\s+");
            Name2_7 = po.findName(information2_7[0]);
            State2_7 = information2_7[5];
            if(!information2_7[3].equals("LETTERUNCHOSEN")&&!information2_7[4].equals("NUMBERUNCHOSEN")) {
                Seats2_7 = information2_7[3] + information2_7[4];
            }
            if(!information2_7[2].equals("FOODUNCHOSEN")){
                Foods2_7 = information2_7[2];
            }
            if(information2_7.length>9) {
                LENGTH=information2_7.length;
                while(LENGTH>=9) {
                    Note2_7 = information2_7[LENGTH-2]+" "+Note2_7;
                    LENGTH=LENGTH-1;
                }
            }else{
                if (information2_7[7].equals("NOTE")) {
                    Note2_7 = "";
                } else {
                    Note2_7 = information2_7[7];
                }
            }
        }
        JLabel name2_7 = new JLabel(Name2_7,SwingConstants.CENTER);
        name2_7.setPreferredSize(new Dimension(220, 45));
        name2_7.setOpaque(true);
        name2_7.setFont(new Font(Font.DIALOG, Font.PLAIN, 20));//设置文字字体
        name2_7.setForeground(Color.black);//设置文字的颜色
        name2_7.setBackground(new Color(232, 232, 232));
        gbc2.weightx = 0.2;    // 指定组件的分配区域
        gbc2.weighty = 0.09;
        gbc2.gridwidth = 1;
        gbc2.gridheight = 1;
        gbc2.gridx = 0;
        gbc2.gridy = 7;
        gbl2.setConstraints(name2_7, gbc2);
        middlePanel2.add(name2_7);
        JLabel state2_7 = new JLabel(State2_7, SwingConstants.CENTER);
        state2_7.setPreferredSize(new Dimension(220, 45));
        state2_7.setFont(new Font(Font.DIALOG, Font.PLAIN, 20));//设置文字字体
        state2_7.setForeground(Color.black);//设置文字的颜色
        state2_7.setOpaque(true);
        state2_7.setBackground(new Color(232, 232, 232));
        gbc2.weightx = 0.2;    // 指定组件的分配区域
        gbc2.weighty = 0.09;
        gbc2.gridwidth = 1;
        gbc2.gridheight = 1;
        gbc2.gridx = 1;
        gbc2.gridy = 7;
        gbl2.setConstraints(state2_7, gbc2);
        middlePanel2.add(state2_7);
        JLabel seats2_7 = new JLabel(Seats2_7, SwingConstants.CENTER);
        seats2_7.setPreferredSize(new Dimension(220, 45));
        seats2_7.setFont(new Font(Font.DIALOG, Font.PLAIN, 20));//设置文字字体
        seats2_7.setForeground(Color.black);//设置文字的颜色
        seats2_7.setOpaque(true);
        seats2_7.setBackground(new Color(232, 232, 232));
        gbc2.weightx = 0.2;    // 指定组件的分配区域
        gbc2.weighty = 0.09;
        gbc2.gridwidth = 1;
        gbc2.gridheight = 1;
        gbc2.gridx = 2;
        gbc2.gridy = 7;
        gbl2.setConstraints(seats2_7, gbc2);
        middlePanel2.add(seats2_7);
        JLabel foods2_7 = new JLabel(Foods2_7, SwingConstants.CENTER);
        foods2_7.setPreferredSize(new Dimension(220, 45));
        foods2_7.setFont(new Font(Font.DIALOG, Font.PLAIN, 20));//设置文字字体
        foods2_7.setForeground(Color.black);//设置文字的颜色
        foods2_7.setOpaque(true);
        foods2_7.setBackground(new Color(232, 232, 232));
        gbc2.weightx = 0.2;    // 指定组件的分配区域
        gbc2.weighty = 0.09;
        gbc2.gridwidth = 1;
        gbc2.gridheight = 1;
        gbc2.gridx = 3;
        gbc2.gridy = 7;
        gbl2.setConstraints(foods2_7, gbc2);
        middlePanel2.add(foods2_7);
        JLabel note2_7 = new JLabel(Note2_7, SwingConstants.CENTER);
        note2_7.setPreferredSize(new Dimension(220, 45));
        note2_7.setFont(new Font(Font.DIALOG, Font.PLAIN, 20));//设置文字字体
        note2_7.setForeground(Color.black);//设置文字的颜色
        note2_7.setOpaque(true);
        note2_7.setBackground(new Color(232, 232, 232));
        gbc2.weightx = 0.2;    // 指定组件的分配区域
        gbc2.weighty = 0.09;
        gbc2.gridwidth = 1;
        gbc2.gridheight = 1;
        gbc2.gridx = 4;
        gbc2.gridy = 7;
        gbl2.setConstraints(note2_7, gbc2);
        middlePanel2.add(note2_7);

        String Name2_8 = "";
        String State2_8 ="";
        String Seats2_8 ="";
        String Foods2_8 ="";
        String Note2_8 = "";
        if(passenger_num>=18) {
            String[] information2_8 = information.get(17).split("\\s+");
            Name2_8 = po.findName(information2_8[0]);
            State2_8 = information2_8[5];
            if(!information2_8[3].equals("LETTERUNCHOSEN")&&!information2_8[4].equals("NUMBERUNCHOSEN")) {
                Seats2_8 = information2_8[3] + information2_8[4];
            }
            if(!information2_8[2].equals("FOODUNCHOSEN")){
                Foods2_8 = information2_8[2];
            }
            if(information2_8.length>9) {
                LENGTH=information2_8.length;
                while(LENGTH>=9) {
                    Note2_8 = information2_8[LENGTH-2]+" "+Note2_8;
                    LENGTH=LENGTH-1;
                }
            }else{
                if (information2_8[7].equals("NOTE")) {
                    Note2_8 = "";
                } else {
                    Note2_8 = information2_8[7];
                }
            }
        }
        JLabel name2_8 = new JLabel(Name2_8,SwingConstants.CENTER);
        name2_8.setPreferredSize(new Dimension(220, 45));
        name2_8.setOpaque(true);
        name2_8.setFont(new Font(Font.DIALOG, Font.PLAIN, 20));//设置文字字体
        name2_8.setForeground(Color.black);//设置文字的颜色
        name2_8.setBackground(new Color(252, 250, 250));
        gbc2.weightx = 0.2;    // 指定组件的分配区域
        gbc2.weighty = 0.09;
        gbc2.gridwidth = 1;
        gbc2.gridheight = 1;
        gbc2.gridx = 0;
        gbc2.gridy = 8;
        gbl2.setConstraints(name2_8, gbc2);
        middlePanel2.add(name2_8);
        JLabel state2_8 = new JLabel(State2_8, SwingConstants.CENTER);
        state2_8.setPreferredSize(new Dimension(220, 45));
        state2_8.setFont(new Font(Font.DIALOG, Font.PLAIN, 20));//设置文字字体
        state2_8.setForeground(Color.black);//设置文字的颜色
        state2_8.setOpaque(true);
        state2_8.setBackground(new Color(252, 250, 250));
        gbc2.weightx = 0.2;    // 指定组件的分配区域
        gbc2.weighty = 0.09;
        gbc2.gridwidth = 1;
        gbc2.gridheight = 1;
        gbc2.gridx = 1;
        gbc2.gridy = 8;
        gbl2.setConstraints(state2_8, gbc2);
        middlePanel2.add(state2_8);
        JLabel seats2_8 = new JLabel(Seats2_8, SwingConstants.CENTER);
        seats2_8.setPreferredSize(new Dimension(220, 45));
        seats2_8.setFont(new Font(Font.DIALOG, Font.PLAIN, 20));//设置文字字体
        seats2_8.setForeground(Color.black);//设置文字的颜色
        seats2_8.setOpaque(true);
        seats2_8.setBackground(new Color(252, 250, 250));
        gbc2.weightx = 0.2;    // 指定组件的分配区域
        gbc2.weighty = 0.09;
        gbc2.gridwidth = 1;
        gbc2.gridheight = 1;
        gbc2.gridx = 2;
        gbc2.gridy = 8;
        gbl2.setConstraints(seats2_8, gbc2);
        middlePanel2.add(seats2_8);
        JLabel foods2_8 = new JLabel(Foods2_8, SwingConstants.CENTER);
        foods2_8.setPreferredSize(new Dimension(220, 45));
        foods2_8.setFont(new Font(Font.DIALOG, Font.PLAIN, 20));//设置文字字体
        foods2_8.setForeground(Color.black);//设置文字的颜色
        foods2_8.setOpaque(true);
        foods2_8.setBackground(new Color(252, 250, 250));
        gbc2.weightx = 0.2;    // 指定组件的分配区域
        gbc2.weighty = 0.09;
        gbc2.gridwidth = 1;
        gbc2.gridheight = 1;
        gbc2.gridx = 3;
        gbc2.gridy = 8;
        gbl2.setConstraints(foods2_8, gbc2);
        middlePanel2.add(foods2_8);
        JLabel note2_8 = new JLabel(Note2_8, SwingConstants.CENTER);
        note2_8.setPreferredSize(new Dimension(220, 45));
        note2_8.setFont(new Font(Font.DIALOG, Font.PLAIN, 20));//设置文字字体
        note2_8.setForeground(Color.black);//设置文字的颜色
        note2_8.setOpaque(true);
        note2_8.setBackground(new Color(252, 250, 250));
        gbc2.weightx = 0.2;    // 指定组件的分配区域
        gbc2.weighty = 0.09;
        gbc2.gridwidth = 1;
        gbc2.gridheight = 1;
        gbc2.gridx = 4;
        gbc2.gridy = 8;
        gbl2.setConstraints(note2_8, gbc2);
        middlePanel2.add(note2_8);

        String Name2_9 = "";
        String State2_9 ="";
        String Seats2_9 ="";
        String Foods2_9 ="";
        String Note2_9 = "";
        if(passenger_num>=19) {
            String[] information2_9 = information.get(18).split("\\s+");
            Name2_9 = po.findName(information2_9[0]);
            State2_9 = information2_9[5];
            if(!information2_9[3].equals("LETTERUNCHOSEN")&&!information2_9[4].equals("NUMBERUNCHOSEN")) {
                Seats2_9 = information2_9[3] + information2_9[4];
            }
            if(!information2_9[2].equals("FOODUNCHOSEN")){
                Foods2_9 = information2_9[2];
            }
            if(information2_9.length>9) {
                LENGTH=information2_9.length;
                while(LENGTH>=9) {
                    Note2_9 = information2_9[LENGTH-2]+" "+Note2_9;
                    LENGTH=LENGTH-1;
                }
            }else{
                if (information2_9[7].equals("NOTE")) {
                    Note2_9 = "";
                } else {
                    Note2_9 = information2_9[7];
                }
            }
        }
        JLabel name2_9 = new JLabel(Name2_9,SwingConstants.CENTER);
        name2_9.setPreferredSize(new Dimension(220, 45));
        name2_9.setOpaque(true);
        name2_9.setFont(new Font(Font.DIALOG, Font.PLAIN, 20));//设置文字字体
        name2_9.setForeground(Color.black);//设置文字的颜色
        name2_9.setBackground(new Color(232, 232, 232));
        gbc2.weightx = 0.2;    // 指定组件的分配区域
        gbc2.weighty = 0.09;
        gbc2.gridwidth = 1;
        gbc2.gridheight = 1;
        gbc2.gridx = 0;
        gbc2.gridy = 9;
        gbl2.setConstraints(name2_9, gbc2);
        middlePanel2.add(name2_9);
        JLabel state2_9 = new JLabel(State2_9, SwingConstants.CENTER);
        state2_9.setPreferredSize(new Dimension(220, 45));
        state2_9.setFont(new Font(Font.DIALOG, Font.PLAIN, 20));//设置文字字体
        state2_9.setForeground(Color.black);//设置文字的颜色
        state2_9.setOpaque(true);
        state2_9.setBackground(new Color(232, 232, 232));
        gbc2.weightx = 0.2;    // 指定组件的分配区域
        gbc2.weighty = 0.09;
        gbc2.gridwidth = 1;
        gbc2.gridheight = 1;
        gbc2.gridx = 1;
        gbc2.gridy = 9;
        gbl2.setConstraints(state2_9, gbc2);
        middlePanel2.add(state2_9);
        JLabel seats2_9 = new JLabel(Seats2_9, SwingConstants.CENTER);
        seats2_9.setPreferredSize(new Dimension(220, 45));
        seats2_9.setFont(new Font(Font.DIALOG, Font.PLAIN, 20));//设置文字字体
        seats2_9.setForeground(Color.black);//设置文字的颜色
        seats2_9.setOpaque(true);
        seats2_9.setBackground(new Color(232, 232, 232));
        gbc2.weightx = 0.2;    // 指定组件的分配区域
        gbc2.weighty = 0.09;
        gbc2.gridwidth = 1;
        gbc2.gridheight = 1;
        gbc2.gridx = 2;
        gbc2.gridy = 9;
        gbl2.setConstraints(seats2_9, gbc2);
        middlePanel2.add(seats2_9);
        JLabel foods2_9 = new JLabel(Foods2_9, SwingConstants.CENTER);
        foods2_9.setPreferredSize(new Dimension(220, 45));
        foods2_9.setFont(new Font(Font.DIALOG, Font.PLAIN, 20));//设置文字字体
        foods2_9.setForeground(Color.black);//设置文字的颜色
        foods2_9.setOpaque(true);
        foods2_9.setBackground(new Color(232, 232, 232));
        gbc2.weightx = 0.2;    // 指定组件的分配区域
        gbc2.weighty = 0.09;
        gbc2.gridwidth = 1;
        gbc2.gridheight = 1;
        gbc2.gridx = 3;
        gbc2.gridy = 9;
        gbl2.setConstraints(foods2_9, gbc2);
        middlePanel2.add(foods2_9);
        JLabel note2_9 = new JLabel(Note2_9, SwingConstants.CENTER);
        note2_9.setPreferredSize(new Dimension(220, 45));
        note2_9.setFont(new Font(Font.DIALOG, Font.PLAIN, 20));//设置文字字体
        note2_9.setForeground(Color.black);//设置文字的颜色
        note2_9.setOpaque(true);
        note2_9.setBackground(new Color(232, 232, 232));
        gbc2.weightx = 0.2;    // 指定组件的分配区域
        gbc2.weighty = 0.09;
        gbc2.gridwidth = 1;
        gbc2.gridheight = 1;
        gbc2.gridx = 4;
        gbc2.gridy = 9;
        gbl2.setConstraints(note2_9, gbc2);
        middlePanel2.add(note2_9);

        String Name2_10 = "";
        String State2_10 ="";
        String Seats2_10 ="";
        String Foods2_10 ="";
        String Note2_10 = "";
        if(passenger_num>=20) {
            String[] information2_10 = information.get(19).split("\\s+");
            Name2_10 = po.findName(information2_10[0]);
            State2_10 = information2_10[5];
            if(!information2_10[3].equals("LETTERUNCHOSEN")&&!information2_10[4].equals("NUMBERUNCHOSEN")) {
                Seats2_10 = information2_10[3] + information2_10[4];
            }
            if(!information2_10[2].equals("FOODUNCHOSEN")){
                Foods2_10 = information2_10[2];
            }
            if(information2_10.length>9) {
                LENGTH=information2_10.length;
                while(LENGTH>=9) {
                    Note2_10 = information2_10[LENGTH-2]+" "+Note2_10;
                    LENGTH=LENGTH-1;
                }
            }else{
                if (information2_10[7].equals("NOTE")) {
                    Note2_10 = "";
                } else {
                    Note2_10 = information2_10[7];
                }
            }
        }
        JLabel name2_10 = new JLabel(Name2_10,SwingConstants.CENTER);
        name2_10.setPreferredSize(new Dimension(220, 45));
        name2_10.setOpaque(true);
        name2_10.setFont(new Font(Font.DIALOG, Font.PLAIN, 20));//设置文字字体
        name2_10.setForeground(Color.black);//设置文字的颜色
        name2_10.setBackground(new Color(252, 250, 250));
        gbc2.weightx = 0.2;    // 指定组件的分配区域
        gbc2.weighty = 0.09;
        gbc2.gridwidth = 1;
        gbc2.gridheight = 1;
        gbc2.gridx = 0;
        gbc2.gridy = 10;
        gbl2.setConstraints(name2_10, gbc2);
        middlePanel2.add(name2_10);
        JLabel state2_10 = new JLabel(State2_10, SwingConstants.CENTER);
        state2_10.setPreferredSize(new Dimension(220, 45));
        state2_10.setFont(new Font(Font.DIALOG, Font.PLAIN, 20));//设置文字字体
        state2_10.setForeground(Color.black);//设置文字的颜色
        state2_10.setOpaque(true);
        state2_10.setBackground(new Color(252, 250, 250));
        gbc2.weightx = 0.2;    // 指定组件的分配区域
        gbc2.weighty = 0.09;
        gbc2.gridwidth = 1;
        gbc2.gridheight = 1;
        gbc2.gridx = 1;
        gbc2.gridy = 10;
        gbl2.setConstraints(state2_10, gbc2);
        middlePanel2.add(state2_10);
        JLabel seats2_10 = new JLabel(Seats2_10, SwingConstants.CENTER);
        seats2_10.setPreferredSize(new Dimension(220, 45));
        seats2_10.setFont(new Font(Font.DIALOG, Font.PLAIN, 20));//设置文字字体
        seats2_10.setForeground(Color.black);//设置文字的颜色
        seats2_10.setOpaque(true);
        seats2_10.setBackground(new Color(252, 250, 250));
        gbc2.weightx = 0.2;    // 指定组件的分配区域
        gbc2.weighty = 0.09;
        gbc2.gridwidth = 1;
        gbc2.gridheight = 1;
        gbc2.gridx = 2;
        gbc2.gridy = 10;
        gbl2.setConstraints(seats2_10, gbc2);
        middlePanel2.add(seats2_10);
        JLabel foods2_10 = new JLabel(Foods2_10, SwingConstants.CENTER);
        foods2_10.setPreferredSize(new Dimension(220, 45));
        foods2_10.setFont(new Font(Font.DIALOG, Font.PLAIN, 20));//设置文字字体
        foods2_10.setForeground(Color.black);//设置文字的颜色
        foods2_10.setOpaque(true);
        foods2_10.setBackground(new Color(252, 250, 250));
        gbc2.weightx = 0.2;    // 指定组件的分配区域
        gbc2.weighty = 0.09;
        gbc2.gridwidth = 1;
        gbc2.gridheight = 1;
        gbc2.gridx = 3;
        gbc2.gridy = 10;
        gbl2.setConstraints(foods2_10, gbc2);
        middlePanel2.add(foods2_10);
        JLabel note2_10 = new JLabel(Note2_10, SwingConstants.CENTER);
        note2_10.setPreferredSize(new Dimension(220, 45));
        note2_10.setFont(new Font(Font.DIALOG, Font.PLAIN, 20));//设置文字字体
        note2_10.setForeground(Color.black);//设置文字的颜色
        note2_10.setOpaque(true);
        note2_10.setBackground(new Color(252, 250, 250));
        gbc2.weightx = 0.2;    // 指定组件的分配区域
        gbc2.weighty = 0.09;
        gbc2.gridwidth = 1;
        gbc2.gridheight = 1;
        gbc2.gridx = 4;
        gbc2.gridy = 10;
        gbl2.setConstraints(note2_10, gbc2);
        middlePanel2.add(note2_10);

        p2.add(middlePanel2);

        panel.add(p2,"2");

        c.show(panel,"1");

        button_right.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(pageNum==1) {
                    c.show(panel,"2");
                    pageNum++;
                    button_right.setVisible(false);
                    button_left.setVisible(true);
                }

            }


        });

        button_left.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                if(pageNum==2){
                    c.show(panel,"1");
                    pageNum--;
                    button_left.setVisible(false);
                    button_right.setVisible(true);
                }

            }


        });


    }
}
