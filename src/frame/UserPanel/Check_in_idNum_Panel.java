package frame.UserPanel;

import frame.Main_Frame;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;
/**
 * Title             : Check_in_idNum_Panel.java
 * Description : This class is used to build an interface for logging in with an id number.
 */
public class Check_in_idNum_Panel  extends JPanel {

    //  
    //
    //IDNum_panel本来就是输入的身份证号，放在这个静态变量里了。
    public static String IDnum = "";
    public static String Surname = "";
    //
    //

    public Check_in_idNum_Panel(){

        //设置背景颜色
        setBackground(new Color(72,46,115));
        //设置初始面板
        JPanel panel = new JPanel();
        panel.setBackground(new Color(72,46,115));//设置背景颜色
        panel.setPreferredSize(new Dimension(1200,680));//设置尺寸
        add(panel);
        panel.setLayout(null);//关闭布局设置，采用绝对布局

        JTextField IDNumInputArea = new JTextField("", 30);
        //外观设计
        IDNumInputArea.setText("");//初始化文本框为空
        IDNumInputArea.setBounds(284,105,632,50);//设置尺寸和绝对坐标
        IDNumInputArea.setFont(new Font (Font.DIALOG,Font.BOLD, 40));//设置字体
        IDNumInputArea.setHorizontalAlignment(JTextField.CENTER);//文本居中
        IDNumInputArea.setForeground(Color.WHITE);//设置文本颜色
        IDNumInputArea.setBackground(new Color(72,46,115));//设置背景
        IDNumInputArea.setEditable(false);
        IDNumInputArea.setBorder(BorderFactory.createLineBorder(Color.WHITE, 2));//设置边框
        panel.add(IDNumInputArea);//将文本框添加到面板里

        JLabel IDNumHint = new JLabel("");
        if (!Main_Frame.flag){
            IDNumHint.setText("ENTER YOUR ID NUMBER AND SELECT CONFIRM");
        }else {
            IDNumHint.setText("NOW ENTER YOUR SURNAME AND SELECT CONFIRM");
        }

        //外观设计
        IDNumHint.setBounds(184,45,816,50);
        IDNumHint.setFont(new Font(Font.DIALOG,Font.BOLD,29));//设置文字字体
        IDNumHint.setForeground(Color.white);//设置文字的颜色
        IDNumHint.setOpaque(true);
        IDNumHint.setBackground(new Color(72,46,115));
        IDNumHint.setPreferredSize(new Dimension(800,80));
        IDNumHint.setHorizontalAlignment(JLabel.CENTER);
        panel.add(IDNumHint);

        //
        //
        //用四个Arraylisy分别存放四行的按钮，并通过for循环一次递增坐标和属性设置
        //
        //
        ArrayList<JButton> first_row = new ArrayList<>();
        for(int i = 0; i < 10; i++){

            first_row.add(new JButton());
            panel.add(first_row.get(i));
            first_row.get(i).setBackground(new Color(96,62,151));
            first_row.get(i).setForeground(Color.white);
            first_row.get(i).setFont(new Font (Font.DIALOG, Font.PLAIN, 45));
            first_row.get(i).setBorder(BorderFactory.createLineBorder(Color.WHITE, 2));

            switch(i){
                case 0: first_row.get(i).setText("1");
                    first_row.get(i).setBounds(105 + 90 * i, 202, 75, 75);
                    first_row.get(i).addActionListener((e)->{
                        String temp = IDNumInputArea.getText();
                        temp = temp + '1';
                        IDnum = temp;
                        IDNumInputArea.setText(temp);
                    });
                    break;
                case 1: first_row.get(i).setText("2");
                    first_row.get(i).setBounds(105 + 90 * i, 202, 75, 75);
                    first_row.get(i).addActionListener((e) -> {
                        String temp = IDNumInputArea.getText();
                        temp = temp + '2';
                        IDnum = temp;
                        IDNumInputArea.setText(temp);
                    });
                    break;
                case 2: first_row.get(i).setText("3");
                    first_row.get(i).setBounds(105 + 90 * i, 202, 75, 75);
                    first_row.get(i).addActionListener((e) -> {
                        String temp = IDNumInputArea.getText();
                        temp = temp + '3';
                        IDnum = temp;
                        IDNumInputArea.setText(temp);
                    });
                    break;
                case 3: first_row.get(i).setText("4");
                    first_row.get(i).setBounds(105 + 90 * i, 202, 75, 75);
                    first_row.get(i).addActionListener((e) -> {
                        String temp = IDNumInputArea.getText();
                        temp = temp + '4';
                        IDnum = temp;
                        IDNumInputArea.setText(temp);
                    });
                    break;
                case 4: first_row.get(i).setText("5");
                    first_row.get(i).setBounds(105 + 90 * i, 202, 75, 75);
                    first_row.get(i).addActionListener((e) -> {
                        String temp = IDNumInputArea.getText();
                        temp = temp + '5';
                        IDnum = temp;
                        IDNumInputArea.setText(temp);
                    });
                    break;
                case 5: first_row.get(i).setText("6");
                    first_row.get(i).setBounds(105 + 90 * i, 202, 75, 75);
                    first_row.get(i).addActionListener((e) -> {
                        String temp = IDNumInputArea.getText();
                        temp = temp + '6';
                        IDnum = temp;
                        IDNumInputArea.setText(temp);
                    });
                    break;
                case 6: first_row.get(i).setText("7");
                    first_row.get(i).setBounds(105 + 90 * i, 202, 75, 75);
                    first_row.get(i).addActionListener((e) -> {
                        String temp = IDNumInputArea.getText();
                        temp = temp + '7';
                        IDnum = temp;
                        IDNumInputArea.setText(temp);
                    });
                    break;
                case 7: first_row.get(i).setText("8");
                    first_row.get(i).setBounds(105 + 90 * i, 202, 75, 75);
                    first_row.get(i).addActionListener((e) -> {
                        String temp = IDNumInputArea.getText();
                        temp = temp + '8';
                        IDnum = temp;
                        IDNumInputArea.setText(temp);
                    });
                    break;
                case 8: first_row.get(i).setText("9");
                    first_row.get(i).setBounds(105 + 90 * i, 202, 75, 75);
                    first_row.get(i).addActionListener((e) -> {
                        String temp = IDNumInputArea.getText();
                        temp = temp + '9';
                        IDnum = temp;
                        IDNumInputArea.setText(temp);
                    });
                    break;
                case 9: first_row.get(i).setText("0");
                    first_row.get(i).setBounds(105 + 90 * i, 202, 75, 75);
                    first_row.get(i).addActionListener((e) -> {
                        String temp = IDNumInputArea.getText();
                        temp = temp + '0';
                        IDnum = temp;
                        IDNumInputArea.setText(temp);
                    });
                    break;
            }
        }

        ArrayList<JButton> second_row = new ArrayList<>();
        for(int i = 0; i < 10; i++){
            //JButton temp = new JButton();
            second_row.add(new JButton());
            panel.add(second_row.get(i));
            second_row.get(i).setBackground(new Color(96,62,151));
            second_row.get(i).setForeground(Color.white);
            second_row.get(i).setFont(new Font (Font.DIALOG, Font.PLAIN, 45));
            second_row.get(i).setBorder(BorderFactory.createLineBorder(Color.WHITE, 2));
            switch(i){
                case 0: second_row.get(i).setText("Q");
                    second_row.get(i).setBounds(105 + 90 * i, 287, 75, 75);
                    second_row.get(i).addActionListener((e) -> {
                        String temp = IDNumInputArea.getText();
                        temp = temp + 'Q';
                        IDnum = temp;
                        IDNumInputArea.setText(temp);
                    });
                    break;
                case 1: second_row.get(i).setText("W");
                    second_row.get(i).setBounds(105 + 90 * i, 287, 75, 75);
                    second_row.get(i).addActionListener((e) -> {
                        String temp = IDNumInputArea.getText();
                        temp = temp + 'W';
                        IDnum = temp;
                        IDNumInputArea.setText(temp);
                    });
                    break;
                case 2: second_row.get(i).setText("E");
                    second_row.get(i).setBounds(105 + 90 * i, 287, 75, 75);
                    second_row.get(i).addActionListener((e) -> {
                        String temp = IDNumInputArea.getText();
                        temp = temp + 'E';
                        IDnum = temp;
                        IDNumInputArea.setText(temp);
                    });
                    break;
                case 3: second_row.get(i).setText("R");
                    second_row.get(i).setBounds(105 + 90 * i, 287, 75, 75);
                    second_row.get(i).addActionListener((e) -> {
                        String temp = IDNumInputArea.getText();
                        temp = temp + 'R';
                        IDnum = temp;
                        IDNumInputArea.setText(temp);
                    });
                    break;
                case 4: second_row.get(i).setText("T");
                    second_row.get(i).setBounds(105 + 90 * i, 287, 75, 75);
                    second_row.get(i).addActionListener((e) -> {
                        String temp = IDNumInputArea.getText();
                        temp = temp + 'T';
                        IDnum = temp;
                        IDNumInputArea.setText(temp);
                    });
                    break;
                case 5: second_row.get(i).setText("Y");
                    second_row.get(i).setBounds(105 + 90 * i, 287, 75, 75);
                    second_row.get(i).addActionListener((e) -> {
                        String temp = IDNumInputArea.getText();
                        temp = temp + 'Y';
                        IDnum = temp;
                        IDNumInputArea.setText(temp);
                    });
                    break;
                case 6: second_row.get(i).setText("U");
                    second_row.get(i).setBounds(105 + 90 * i, 287, 75, 75);
                    second_row.get(i).addActionListener((e) -> {
                        String temp = IDNumInputArea.getText();
                        temp = temp + 'U';
                        IDnum = temp;
                        IDNumInputArea.setText(temp);
                    });
                    break;
                case 7: second_row.get(i).setText("I");
                    second_row.get(i).setBounds(105 + 90 * i, 287, 75, 75);
                    second_row.get(i).addActionListener((e) -> {
                        String temp = IDNumInputArea.getText();
                        temp = temp + 'I';
                        IDnum = temp;
                        IDNumInputArea.setText(temp);
                    });
                    break;
                case 8: second_row.get(i).setText("O");
                    second_row.get(i).setBounds(105 + 90 * i, 287, 75, 75);
                    second_row.get(i).addActionListener((e) -> {
                        String temp = IDNumInputArea.getText();
                        temp = temp + 'O';
                        IDnum = temp;
                        IDNumInputArea.setText(temp);
                    });
                    break;
                case 9: second_row.get(i).setText("P");
                    second_row.get(i).setBounds(105 + 90 * i, 287, 75, 75);
                    second_row.get(i).addActionListener((e) -> {
                        String temp = IDNumInputArea.getText();
                        temp = temp + 'P';
                        IDnum = temp;
                        IDNumInputArea.setText(temp);
                    });
                    break;
            }
        }

        ArrayList<JButton> third_row = new ArrayList<>();
        for(int i = 0; i < 9; i++){

            third_row.add(new JButton());
            panel.add(third_row.get(i));
            third_row.get(i).setBackground(new Color(96,62,151));
            third_row.get(i).setForeground(Color.white);
            third_row.get(i).setFont(new Font (Font.DIALOG, Font.PLAIN, 45));
            third_row.get(i).setBorder(BorderFactory.createLineBorder(Color.WHITE, 2));

            switch(i){
                case 0: third_row.get(i).setText("A");
                    third_row.get(i).setBounds(160 + 90 * i, 372, 75, 75);
                    third_row.get(i).addActionListener((e) -> {
                        String temp = IDNumInputArea.getText();
                        temp = temp + 'A';
                        IDnum = temp;
                        IDNumInputArea.setText(temp);
                    });
                    break;
                case 1: third_row.get(i).setText("S");
                    third_row.get(i).setBounds(160 + 90 * i, 372, 75, 75);
                    third_row.get(i).addActionListener((e) -> {
                        String temp = IDNumInputArea.getText();
                        temp = temp + 'S';
                        IDnum = temp;
                        IDNumInputArea.setText(temp);
                    });
                    break;
                case 2: third_row.get(i).setText("D");
                    third_row.get(i).setBounds(160 + 90 * i, 372, 75, 75);
                    third_row.get(i).addActionListener((e) -> {
                        String temp = IDNumInputArea.getText();
                        temp = temp + 'D';
                        IDnum = temp;
                        IDNumInputArea.setText(temp);
                    });
                    break;
                case 3: third_row.get(i).setText("F");
                    third_row.get(i).setBounds(160 + 90 * i, 372, 75, 75);
                    third_row.get(i).addActionListener((e) -> {
                        String temp = IDNumInputArea.getText();
                        temp = temp + 'F';
                        IDnum = temp;
                        IDNumInputArea.setText(temp);
                    });
                    break;
                case 4: third_row.get(i).setText("G");
                    third_row.get(i).setBounds(160 + 90 * i, 372, 75, 75);
                    third_row.get(i).addActionListener((e) -> {
                        String temp = IDNumInputArea.getText();
                        temp = temp + 'G';
                        IDnum = temp;
                        IDNumInputArea.setText(temp);
                    });
                    break;
                case 5: third_row.get(i).setText("H");
                    third_row.get(i).setBounds(160 + 90 * i, 372, 75, 75);
                    third_row.get(i).addActionListener((e) -> {
                        String temp = IDNumInputArea.getText();
                        temp = temp + 'H';
                        IDnum = temp;
                        IDNumInputArea.setText(temp);
                    });
                    break;
                case 6: third_row.get(i).setText("J");
                    third_row.get(i).setBounds(160 + 90 * i, 372, 75, 75);
                    third_row.get(i).addActionListener((e) -> {
                        String temp = IDNumInputArea.getText();
                        temp = temp + 'J';
                        IDnum = temp;
                        IDNumInputArea.setText(temp);
                    });
                    break;
                case 7: third_row.get(i).setText("K");
                    third_row.get(i).setBounds(160 + 90 * i, 372, 75, 75);
                    third_row.get(i).addActionListener((e) -> {
                        String temp = IDNumInputArea.getText();
                        temp = temp + 'K';
                        IDnum = temp;
                        IDNumInputArea.setText(temp);
                    });
                    break;
                case 8: third_row.get(i).setText("L");
                    third_row.get(i).setBounds(160 + 90 * i, 372, 75, 75);
                    third_row.get(i).addActionListener((e) -> {
                        String temp = IDNumInputArea.getText();
                        temp = temp + 'L';
                        IDnum = temp;
                        IDNumInputArea.setText(temp);
                    });
                    break;
            }
        }

        ArrayList<JButton> fourth_row = new ArrayList<>();
        for(int i = 0; i < 9; i++){

            fourth_row.add(new JButton());
            panel.add(fourth_row.get(i));
            fourth_row.get(i).setBackground(new Color(96,62,151));
            fourth_row.get(i).setForeground(Color.white);
            fourth_row.get(i).setFont(new Font (Font.DIALOG, Font.PLAIN, 45));
            fourth_row.get(i).setBorder(BorderFactory.createLineBorder(Color.WHITE, 2));

            switch(i){
                case 0: fourth_row.get(i).setText("Z");
                    fourth_row.get(i).setBounds(250 + 90 * i, 457, 75, 75);
                    fourth_row.get(i).addActionListener((e) -> {
                        String temp = IDNumInputArea.getText();
                        temp = temp + 'Z';
                        IDnum = temp;
                        IDNumInputArea.setText(temp);
                    });
                    break;
                case 1: fourth_row.get(i).setText("X");
                    fourth_row.get(i).setBounds(250 + 90 * i, 457, 75, 75);
                    fourth_row.get(i).addActionListener((e) -> {
                        String temp = IDNumInputArea.getText();
                        temp = temp + 'X';
                        IDnum = temp;
                        IDNumInputArea.setText(temp);
                    });
                    break;
                case 2: fourth_row.get(i).setText("C");
                    fourth_row.get(i).setBounds(250 + 90 * i, 457, 75, 75);
                    fourth_row.get(i).addActionListener((e) -> {
                        String temp = IDNumInputArea.getText();
                        temp = temp + 'C';
                        IDnum = temp;
                        IDNumInputArea.setText(temp);
                    });
                    break;
                case 3: fourth_row.get(i).setText("V");
                    fourth_row.get(i).setBounds(250 + 90 * i, 457, 75, 75);
                    fourth_row.get(i).addActionListener((e) -> {
                        String temp = IDNumInputArea.getText();
                        temp = temp + 'V';
                        IDnum = temp;
                        IDNumInputArea.setText(temp);
                    });
                    break;
                case 4: fourth_row.get(i).setText("B");
                    fourth_row.get(i).setBounds(250 + 90 * i, 457, 75, 75);
                    fourth_row.get(i).addActionListener((e) -> {
                        String temp = IDNumInputArea.getText();
                        temp = temp + 'B';
                        IDnum = temp;
                        IDNumInputArea.setText(temp);
                    });
                    break;
                case 5: fourth_row.get(i).setText("N");
                    fourth_row.get(i).setBounds(250 + 90 * i, 457, 75, 75);
                    fourth_row.get(i).addActionListener((e) -> {
                        String temp = IDNumInputArea.getText();
                        temp = temp + 'N';
                        IDnum = temp;
                        IDNumInputArea.setText(temp);
                    });
                    break;
                case 6: fourth_row.get(i).setText("M");
                    fourth_row.get(i).setBounds(250 + 90 * i, 457, 75, 75);
                    fourth_row.get(i).addActionListener((e) -> {
                        String temp = IDNumInputArea.getText();
                        temp = temp + 'M';
                        IDnum = temp;
                        IDNumInputArea.setText(temp);
                    });
            }
        }

        //添加删除键，原理同上
        JButton delete = new JButton("Delete");
        panel.add(delete);
        delete.setBackground(new Color(96,62,151));
        delete.setForeground(Color.white);
        delete.setFont(new Font (Font.DIALOG, Font.PLAIN, 30));
        delete.setBorder(BorderFactory.createLineBorder(Color.WHITE, 2));
        delete.setBounds(1015,202,126,75);
        delete.addActionListener((e) -> {
            //若存在文本则执行删除操作，无则不执行
            if( IDNumInputArea.getText().length() > 0 ){
                String temp = IDNumInputArea.getText();
                temp = temp.substring(0, temp.length() - 1);
                IDnum = temp;
                IDNumInputArea.setText(temp);
            }
            else;
        });



    }
}