package frame.UserPanel;

//import com.sun.tools.javac.Main;
import constant.Constant;
import frame.Main_Frame;
import model.eachFlight;
import model.passenger_order;
import utils.BackGroundImagePanle;

import javax.swing.*;
import java.awt.*;

/**
 * <p>This page is mainly for users to print boarding passes.</p>
 *
 * @author Qingwei Gao
 * @version 3.1
 */
public class Print_Panel extends JPanel {
    public Print_Panel(){
        //Set background color
        setBackground(new Color(72,46,115));
        //Set initial panel
        JPanel panel = new JPanel();

        panel.setBackground(new Color(72,46,115));
        panel.setPreferredSize(new Dimension(1200,680));
        add(panel);
        panel.setLayout(null);//Set empty layout

        //Add sub panel
        JPanel panel1 = new JPanel();
        JPanel panel2 = new JPanel();
        JPanel panel3 = new JPanel();
        JPanel panel4 = new JPanel();

        //Set panel size, color and layout
        panel1.setBounds(100,150,600,360);
        panel1.setBackground(new Color(96,62,151));
        panel1.setLayout(null);//Set empty layout
        panel.add(panel1);

        //Set panel size, color and layout
        panel2.setBounds(710,150,350,360);
        panel2.setBackground(new Color(96,62,151));
        panel2.setLayout(null);//Set empty layout
        panel.add(panel2);

        //Set panel size, color and layout
        panel3.setBounds(0,50,600,280);
        panel3.setBackground(new Color(255,255,255));
        panel3.setLayout(null);//Set empty layout
        panel1.add(panel3);

        //Set panel size, color and layout
        panel4.setBounds(0,50,350,280);
        panel4.setBackground(new Color(255,255,255));
        panel4.setLayout(null);//Set empty layout
        panel2.add(panel4);



        JLabel label1 = new JLabel("PLEASE CLICK CONTINUE TO PRINT YOUR BOARDING PASS ");
        JLabel label2 = new JLabel("ACCORDING TO THE FIGURE BELOW ");
        JLabel label3 = new JLabel("BOARDING PASS");
        JLabel label4 = new JLabel("BOARDING PASS ");
        JLabel label5 = new JLabel("HAVE A NICE TRIP");
        JLabel label11 = new JLabel("NAME OF PASSENGER");
        JLabel label12 = new JLabel("FROM");
        JLabel label13 = new JLabel("TO");
        JLabel label14 = new JLabel("SEAT");
        JLabel label15 = new JLabel("GATE");
        JLabel label16 = new JLabel("BOARDING TIME");
        JLabel label17 = new JLabel(passenger_order.findName(Main_Frame.IDNum));
        JLabel label18 = new JLabel(eachFlight.DEPARTURE(Main_Frame.flightNum));
        JLabel label19 = new JLabel(eachFlight.DESTINATION(Main_Frame.flightNum));
        JLabel label110 = new JLabel(Seats_Panel.number+" "+Seats_Panel.letter);
        JLabel label111 = new JLabel(eachFlight.GATE(Main_Frame.flightNum));
        JLabel label112 = new JLabel(eachFlight.boardingTime(Main_Frame.flightNum));


        JLabel label21 = new JLabel("SEAT");
        JLabel label22 = new JLabel("GATE");
        JLabel label23 = new JLabel("BOARDING TIME");
        JLabel label24 = new JLabel(Seats_Panel.number+" "+Seats_Panel.letter);
        JLabel label25 = new JLabel(eachFlight.GATE(Main_Frame.flightNum));
        JLabel label26 = new JLabel(eachFlight.boardingTime(Main_Frame.flightNum));

        JLabel icon1 = new JLabel();
        ImageIcon image1 = new ImageIcon("resource/barcode1-.png");//Instantiate imageicon object
        image1.setImage(image1.getImage().getScaledInstance(120, 260,Image.SCALE_DEFAULT ));
        icon1.setIcon(image1);
        icon1.setBounds(0,-350,1000,1000);
        panel3.add(icon1);

        JLabel icon2 = new JLabel();
        ImageIcon image2 = new ImageIcon(Constant.RESOURCE_PATH + "barcode2.png");//Instantiate imageicon object
        image2.setImage(image2.getImage().getScaledInstance(180, 40,Image.SCALE_DEFAULT ));
        icon2.setIcon(image2);
        icon2.setIcon(image2);
        icon2.setBounds(80,-250,1000,1000);
        panel4.add(icon2);

        label1.setFont(new Font(Font.DIALOG,Font.PLAIN,33));//Set text font
        label1.setForeground(Color.white);//Set text color
        label2.setFont(new Font(Font.DIALOG,Font.PLAIN,33));//Set text font
        label2.setForeground(Color.white);//Set text color
        label3.setFont(new Font(Font.DIALOG,Font.BOLD,30));//Set text font
        label3.setForeground(Color.white);//Set text color
        label4.setFont(new Font(Font.DIALOG,Font.BOLD,25));//Set text font
        label4.setForeground(Color.white);//Set text color
        label5.setFont(new Font(Font.DIALOG,Font.BOLD,16));//Set text font
        label5.setForeground(Color.white);//Set text color

        label11.setFont(new Font(Font.DIALOG,Font.BOLD,17));//Set text font
        label11.setForeground(Color.black);//Set text color
        label12.setFont(new Font(Font.DIALOG,Font.BOLD,17));//Set text font
        label12.setForeground(Color.black);//Set text color
        label13.setFont(new Font(Font.DIALOG,Font.BOLD,17));//Set text font
        label13.setForeground(Color.black);//Set text color
        label14.setFont(new Font(Font.DIALOG,Font.BOLD,17));//Set text font
        label14.setForeground(Color.black);//Set text color
        label15.setFont(new Font(Font.DIALOG,Font.BOLD,17));//Set text font
        label15.setForeground(Color.black);//Set text color
        label16.setFont(new Font(Font.DIALOG,Font.BOLD,17));//Set text font
        label16.setForeground(Color.black);//Set text color


        label17.setFont(new Font(Font.DIALOG,Font.BOLD,30));//Set text font
        label17.setForeground(Color.black);//Set text color
        label18.setFont(new Font(Font.DIALOG,Font.BOLD,30));//Set text font
        label18.setForeground(Color.black);//Set text color
        label19.setFont(new Font(Font.DIALOG,Font.BOLD,30));//Set text font
        label19.setForeground(Color.black);//Set text color
        label110.setFont(new Font(Font.DIALOG,Font.BOLD,30));//Set text font
        label110.setForeground(Color.black);//Set text color
        label111.setFont(new Font(Font.DIALOG,Font.BOLD,30));//Set text font
        label111.setForeground(Color.black);//Set text color
        label112.setFont(new Font(Font.DIALOG,Font.BOLD,30));//Set text font
        label112.setForeground(Color.black);//Set text color


        label21.setFont(new Font(Font.DIALOG,Font.BOLD,17));//Set text font
        label21.setForeground(Color.black);//Set text color
        label22.setFont(new Font(Font.DIALOG,Font.BOLD,17));//Set text font
        label22.setForeground(Color.black);//Set text color
        label23.setFont(new Font(Font.DIALOG,Font.BOLD,17));//Set text font
        label23.setForeground(Color.black);//Set text color
        label24.setFont(new Font(Font.DIALOG,Font.BOLD,36));//Set text font
        label24.setForeground(Color.black);//Set text color
        label25.setFont(new Font(Font.DIALOG,Font.BOLD,36));//Set text font
        label25.setForeground(Color.black);//Set text color
        label26.setFont(new Font(Font.DIALOG,Font.BOLD,36));//Set text font
        label26.setForeground(Color.black);//Set text color


        //Set label size position and add labels
        label1.setBounds(80,0,1000,100);
        panel.add(label1);
        label2.setBounds(80,50,1000,100);
        panel.add(label2);
        label3.setBounds(30,-20,1000,100);
        panel1.add(label3);
        label4.setBounds(70,-20,1000,100);
        panel2.add(label4);
        label5.setBounds(100,295,1000,100);
        panel2.add(label5);

        label11.setBounds(130,-20,1000,100);
        panel3.add(label11);
        label12.setBounds(130,60,1000,100);
        panel3.add(label12);
        label13.setBounds(130,140,1000,100);
        panel3.add(label13);
        label14.setBounds(400,-20,1000,100);
        panel3.add(label14);
        label15.setBounds(400,60,1000,100);
        panel3.add(label15);
        label16.setBounds(400,140,1000,100);
        panel3.add(label16);

        label17.setBounds(140,20,1000,100);
        panel3.add(label17);
        label18.setBounds(140,100,1000,100);
        panel3.add(label18);
        label19.setBounds(140,180,1000,100);
        panel3.add(label19);
        label110.setBounds(410,20,1000,100);
        panel3.add(label110);
        label111.setBounds(410,100,1000,100);
        panel3.add(label111);
        label112.setBounds(410,180,1000,100);
        panel3.add(label112);


        label21.setBounds(20,-30,1000,100);
        panel4.add(label21);
        label22.setBounds(20,40,1000,100);
        panel4.add(label22);
        label23.setBounds(20,110,1000,100);
        panel4.add(label23);
        label24.setBounds(130,0,1000,100);
        panel4.add(label24);
        label25.setBounds(130,70,1000,100);
        panel4.add(label25);
        label26.setBounds(130,150,1000,100);
        panel4.add(label26);

    }

}
