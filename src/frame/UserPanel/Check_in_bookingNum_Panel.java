package frame.UserPanel;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;


/**
 * Title             : Check_in_bookingNum_Panel.java
 * Description : This class is used to build an interface for logging in with an order number.
 */
public class Check_in_bookingNum_Panel  extends JPanel {

    //bookingNum_panel点continue会把框里的订单号转成身份证号放在方法下的这个静态变量里
    public static String IDnum="";

    public Check_in_bookingNum_Panel(){
        //设置背景颜色
        IDnum="";
        setBackground(new Color(72,46,115));
        //设置初始面板
        JPanel panel = new JPanel();
        panel.setBackground(new Color(72,46,115));
        panel.setPreferredSize(new Dimension(1200,680));
        add(panel);
        //取消布局设计
        panel.setLayout(null);


        JTextField BookingNumInputArea = new JTextField("", 30);
        //外观设计
        BookingNumInputArea.setBounds(284,105,632,50);
        BookingNumInputArea.setFont(new Font (Font.DIALOG,Font.BOLD, 40));
        BookingNumInputArea.setHorizontalAlignment(JTextField.CENTER);
        BookingNumInputArea.setForeground(Color.WHITE);
        BookingNumInputArea.setBackground(new Color(72,46,115));
        BookingNumInputArea.setEditable(false);
        BookingNumInputArea.setBorder(BorderFactory.createLineBorder(Color.WHITE, 2));


        JLabel BookingNumHint = new JLabel("ENTER YOUR BOOKING NUMBER AND SELECT CONFIRM");
        BookingNumHint.setBounds(184,45,816,50);
        //外观设计
        BookingNumHint.setFont(new Font(Font.DIALOG,Font.BOLD,29));//设置文字字体
        BookingNumHint.setForeground(Color.white);//设置文字的颜色
        BookingNumHint.setOpaque(true);//设置透明效果
        BookingNumHint.setBackground(new Color(72,46,115));//设置背景颜色
        BookingNumHint.setPreferredSize(new Dimension(800,80));//设置长宽
        BookingNumHint.setHorizontalAlignment(JLabel.CENTER);//设置对齐方式

        //创建0-9和delete按钮
        JButton button1 = new JButton("1");
        JButton button2 = new JButton("2");
        JButton button3 = new JButton("3");
        JButton button4 = new JButton("4");
        JButton button5 = new JButton("5");
        JButton button6 = new JButton("6");
        JButton button7 = new JButton("7");
        JButton button8 = new JButton("8");
        JButton button9 = new JButton("9");
        JButton button0 = new JButton("0");
        JButton delete = new JButton("Delete");

        //按钮外观设计
        button1.setBounds(485,215,75,75);
        button1.setBackground(new Color(96,62,151));//设置背景颜色
        button1.setForeground(Color.white);//设置文字颜色
        button1.setFont(new Font (Font.DIALOG, Font.PLAIN, 45));//设置字体大小
        button1.setBorder(BorderFactory.createLineBorder(Color.WHITE, 2));//设置边框
        //以下同理

        button2.setBounds(575,215,75,75);
        button2.setBackground(new Color(96,62,151));
        button2.setForeground(Color.white);
        button2.setFont(new Font (Font.DIALOG, Font.PLAIN, 45));
        button2.setBorder(BorderFactory.createLineBorder(Color.WHITE, 2));

        button3.setBounds(665,215,75,75);
        button3.setBackground(new Color(96,62,151));
        button3.setForeground(Color.white);
        button3.setFont(new Font (Font.DIALOG, Font.PLAIN, 45));
        button3.setBorder(BorderFactory.createLineBorder(Color.WHITE, 2));

        button4.setBounds(485,305,75,75);
        button4.setBackground(new Color(96,62,151));
        button4.setForeground(Color.white);
        button4.setFont(new Font (Font.DIALOG, Font.PLAIN, 45));
        button4.setBorder(BorderFactory.createLineBorder(Color.WHITE, 2));

        button6.setBounds(665,305,75,75);
        button6.setBackground(new Color(96,62,151));
        button6.setForeground(Color.white);
        button6.setFont(new Font (Font.DIALOG, Font.PLAIN, 45));
        button6.setBorder(BorderFactory.createLineBorder(Color.WHITE, 2));

        button5.setBounds(575,305,75,75);
        button5.setBackground(new Color(96,62,151));
        button5.setForeground(Color.white);
        button5.setFont(new Font (Font.DIALOG, Font.PLAIN, 45));
        button5.setBorder(BorderFactory.createLineBorder(Color.WHITE, 2));

        button7.setBounds(487,395,75,75);
        button7.setBackground(new Color(96,62,151));
        button7.setForeground(Color.white);
        button7.setFont(new Font (Font.DIALOG, Font.PLAIN, 45));
        button7.setBorder(BorderFactory.createLineBorder(Color.WHITE, 2));

        button8.setBounds(575,395,75,75);
        button8.setBackground(new Color(96,62,151));
        button8.setForeground(Color.white);
        button8.setFont(new Font (Font.DIALOG, Font.PLAIN, 45));
        button8.setBorder(BorderFactory.createLineBorder(Color.WHITE, 2));

        button9.setBounds(665,395,75,75);
        button9.setBackground(new Color(96,62,151));
        button9.setForeground(Color.white);
        button9.setFont(new Font (Font.DIALOG, Font.PLAIN, 45));
        button9.setBorder(BorderFactory.createLineBorder(Color.WHITE, 2));

        button0.setBounds(575,485,75,75);
        button0.setBackground(new Color(96,62,151));
        button0.setForeground(Color.white);
        button0.setFont(new Font (Font.DIALOG, Font.PLAIN, 45));
        button0.setBorder(BorderFactory.createLineBorder(Color.WHITE, 2));

        delete.setBounds(790,215,126,75);
        delete.setBackground(new Color(96,62,151));
        delete.setForeground(Color.white);
        delete.setFont(new Font (Font.DIALOG, Font.PLAIN, 30));
        delete.setBorder(BorderFactory.createLineBorder(Color.WHITE, 2));

        //将按钮添加到panel上
        panel.add(button1);
        panel.add(button3);
        panel.add(button2);
        panel.add(button4);
        panel.add(button5);
        panel.add(button6);
        panel.add(button7);
        panel.add(button8);
        panel.add(button9);
        panel.add(button0);
        panel.add(delete);
        panel.add(BookingNumInputArea);
        panel.add(BookingNumHint);

        //为按钮添加监听事件
        button0.addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {

            }
        });

        button0.addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                IDnum=IDnum+"0";//实时更新IDnum
                BookingNumInputArea.setText(IDnum);//更新输入框里的内容，以下同理

            }
        });

        button1.addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                IDnum=IDnum+"1";
                BookingNumInputArea.setText(IDnum);
            }
        });

        button2.addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                IDnum=IDnum+"2";
                BookingNumInputArea.setText(IDnum);
            }
        });

        button3.addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                IDnum=IDnum+"3";
                BookingNumInputArea.setText(IDnum);
            }
        });

        button4.addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                IDnum=IDnum+"4";
                BookingNumInputArea.setText(IDnum);
            }
        });

        button5.addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                IDnum=IDnum+"5";
                BookingNumInputArea.setText(IDnum);
            }
        });

        button6.addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                IDnum=IDnum+"6";
                BookingNumInputArea.setText(IDnum);
            }
        });

        button7.addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                IDnum=IDnum+"7";
                BookingNumInputArea.setText(IDnum);
            }
        });

        button8.addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                IDnum=IDnum+"8";
                BookingNumInputArea.setText(IDnum);
            }
        });

        button9.addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                IDnum=IDnum+"9";
                BookingNumInputArea.setText(IDnum);
            }
        });

        delete.addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(IDnum.length() > 0){
                    //若文本框内字符串长度大于一，则执行删除命令
                    IDnum=IDnum.substring(0,IDnum.length()-1);
                    BookingNumInputArea.setText(IDnum);
                } 
                else;//若没有内容，则不执行删除
            }
        });






    }






        
}



