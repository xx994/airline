package frame.UserPanel;

import frame.Main_Frame;
import model.eachFlight;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Title             : Foods_Panel.java
 * Description :This class is methods to display the available food and its price, and record the data of the selection.
 */

public class Foods_Panel extends JPanel {

    public static String food = "";

    public Foods_Panel(){
        //set background
        food = "";
        setBackground(new Color(72, 46, 115));
        //set initial panel
        JPanel panel = new JPanel();
        panel.setBackground(new Color(72, 46, 115));
        panel.setPreferredSize(new Dimension(1200, 680));
        add(panel);
        panel.setLayout(null);

        JPanel panelTOP = new JPanel();
        panelTOP.setLayout(null);
        panelTOP.setBounds(0,0,1200,85);
        panelTOP.setBackground(new Color(72,46,115));

        JLabel labelFlight = new JLabel();
        JLabel labelCompany = new JLabel();
        JLabel labelBoarding = new JLabel();
        JLabel labelTime = new JLabel();
        JLabel labelGate = new JLabel();
        JLabel labelGateNum = new JLabel();

        labelFlight.setFont(new Font(Font.DIALOG,Font.BOLD,25));//set font
        labelFlight.setForeground(Color.white);//set text color
        labelFlight.setText(Main_Frame.flightNum);
        labelFlight.setBounds(60,17,100,40);

        labelCompany.setFont(new Font(Font.DIALOG,Font.PLAIN,25));//set font
        labelCompany.setForeground(Color.white);//set text color
        labelCompany.setText(eachFlight.COMPANY(Main_Frame.flightNum));
        labelCompany.setBounds(170,17,300,40);

        labelBoarding.setFont(new Font(Font.DIALOG,Font.BOLD,25));//set font
        labelBoarding.setForeground(Color.white);//set text color
        labelBoarding.setText("BOARDING TIME");
        labelBoarding.setBounds(480,17,300,40);

        labelTime.setFont(new Font(Font.DIALOG,Font.PLAIN,25));//set font
        labelTime.setForeground(Color.white);//set text color
        labelTime.setText(eachFlight.boardingTime(Main_Frame.flightNum));
        labelTime.setBounds(710,17,100,40);

        labelGate.setFont(new Font(Font.DIALOG,Font.BOLD,25));//set font
        labelGate.setForeground(Color.white);//set text color
        labelGate.setText("GATE");
        labelGate.setBounds(1000,17,100,40);

        labelGateNum.setFont(new Font(Font.DIALOG,Font.PLAIN,25));//set font
        labelGateNum.setForeground(Color.white);//set text color
        labelGateNum.setText(eachFlight.GATE(Main_Frame.flightNum));
        labelGateNum.setBounds(1085,17,50,40);

        panel.add(panelTOP);

        panelTOP.add(labelBoarding);
        panelTOP.add(labelCompany);
        panelTOP.add(labelFlight);
        panelTOP.add(labelTime);
        panelTOP.add(labelGate);
        panelTOP.add(labelGateNum);//Set the layout of the top panel



        BorderLayout b11 = new BorderLayout();
        BorderLayout b12 = new BorderLayout();
        BorderLayout b13 = new BorderLayout();
        BorderLayout b21 = new BorderLayout();
        BorderLayout b22 = new BorderLayout();
        BorderLayout b23 = new BorderLayout();//set the layout of food panels

        JPanel panel_choose_standard = new JPanel(b11);
        panel_choose_standard.setBackground(new Color(72,46,115));
        panel_choose_standard.setBounds(150,85,200,250);
        panel.add(panel_choose_standard);

        JPanel panel_choose_vegetarian = new JPanel(b12);
        panel_choose_vegetarian.setBackground(new Color(72,46,115));
        panel_choose_vegetarian.setBounds(500,85,200,250);
        panel.add(panel_choose_vegetarian);

        JPanel panel_choose_halal = new JPanel(b13);
        panel_choose_halal.setBackground(new Color(72,46,115));
        panel_choose_halal.setBounds(850,85,200,250);
        panel.add(panel_choose_halal);


        JPanel panel_choose_seafood = new JPanel(b21);
        panel_choose_seafood.setBackground(new Color(72,46,115));
        panel_choose_seafood.setBounds(150,350,200,250);
        panel.add(panel_choose_seafood);

        JPanel panel_choose_steak = new JPanel(b22);
        panel_choose_steak.setBackground(new Color(72,46,115));
        panel_choose_steak.setBounds(500,350,200,250);
        panel.add(panel_choose_steak);

        JPanel panel_choose_sushi = new JPanel(b23);
        panel_choose_sushi.setBackground(new Color(72,46,115));
        panel_choose_sushi.setBounds(850,350,200,250);
panel.add(panel_choose_sushi);
/*
set the size and style of food-choose panels.
*/



        JLabel seafood_text = new JLabel("Seafood",SwingConstants.CENTER);
        seafood_text.setPreferredSize(new Dimension(200,40));
        seafood_text.setFont(new Font(Font.DIALOG,Font.BOLD,20));//set font
        seafood_text.setForeground(Color.white);//set text color
        panel_choose_seafood.add(seafood_text,BorderLayout.NORTH);

        JLabel seafood_img = new JLabel();
        seafood_img.setPreferredSize(new Dimension(200,160));
        ImageIcon image_seafood = new ImageIcon("resource/seafood.jpg");
        image_seafood.setImage(image_seafood.getImage().getScaledInstance(200,160,Image.SCALE_DEFAULT));
        seafood_img.setIcon(image_seafood);
        seafood_img.setSize(200,160);
        panel_choose_seafood.add(seafood_img,BorderLayout.CENTER);//import images

        JLabel seafood_choose = new JLabel();
        seafood_choose.setPreferredSize(new Dimension(200,50));

        JButton button_seafood = new JButton("£39.90");
        button_seafood.setSize(200,50);
        button_seafood.setBackground(new Color(250, 250, 250));
        seafood_choose.add(button_seafood);
        button_seafood.setVisible(true);
        panel_choose_seafood.add(seafood_choose,BorderLayout.SOUTH);


        JLabel steak_text = new JLabel("steak",SwingConstants.CENTER);
        steak_text.setPreferredSize(new Dimension(200,40));
        steak_text.setFont(new Font(Font.DIALOG,Font.BOLD,20));//set font
        steak_text.setForeground(Color.white);//set text color
        panel_choose_steak.add(steak_text,BorderLayout.NORTH);

        JLabel steak_img = new JLabel();
        steak_img.setPreferredSize(new Dimension(200,160));
        ImageIcon image_steak= new ImageIcon("resource/steak.jpg");
        image_steak.setImage(image_steak.getImage().getScaledInstance(200,160,Image.SCALE_DEFAULT));//import images
        steak_img.setIcon(image_steak);
        steak_img.setSize(200,160);
        panel_choose_steak.add(steak_img,BorderLayout.CENTER);

        JLabel steak_choose = new JLabel();
        steak_choose.setPreferredSize(new Dimension(200,50));

        JButton button_steak = new JButton("£29.90");
        button_steak.setBackground(new Color(250, 250, 250));
        button_steak.setSize(200,50);
        button_steak.setVisible(true);
        steak_choose.add(button_steak);
        panel_choose_steak.add(steak_choose,BorderLayout.SOUTH);


        JLabel sushi_text = new JLabel("sushi",SwingConstants.CENTER);
        sushi_text.setPreferredSize(new Dimension(200,40));
        sushi_text.setFont(new Font(Font.DIALOG,Font.BOLD,20));//set font
        sushi_text.setForeground(Color.white);//set text color
        panel_choose_sushi.add(sushi_text,BorderLayout.NORTH);

        JLabel sushi_img = new JLabel();
        sushi_img.setPreferredSize(new Dimension(200,160));
        ImageIcon image_sushi= new ImageIcon("resource/sushi.jpg");
        image_sushi.setImage(image_sushi.getImage().getScaledInstance(200,160,Image.SCALE_DEFAULT));//import images
        sushi_img.setIcon(image_sushi);
        sushi_img.setSize(180,200);
        panel_choose_sushi.add(sushi_img,BorderLayout.CENTER);

        JLabel sushi_choose = new JLabel();
        sushi_choose.setPreferredSize(new Dimension(200,50));

        JButton button_sushi = new JButton("£35.90");
        button_sushi.setBackground(new Color(250, 250, 250));
        button_sushi.setSize(200,50);
        button_sushi.setVisible(true);
        sushi_choose.add(button_sushi);
        panel_choose_sushi.add(sushi_choose,BorderLayout.SOUTH);



        JLabel standard_text = new JLabel("Standard",SwingConstants.CENTER);
        standard_text.setPreferredSize(new Dimension(200,40));
        standard_text.setFont(new Font(Font.DIALOG,Font.BOLD,20));//set font
        standard_text.setForeground(Color.white);//set text color
        panel_choose_standard.add(standard_text,BorderLayout.NORTH);

        JLabel standard_img = new JLabel();
        standard_img.setPreferredSize(new Dimension(200,160));
        ImageIcon image_standard = new ImageIcon("resource/standard_food.jpeg");
        image_standard.setImage(image_standard.getImage().getScaledInstance(200,160,Image.SCALE_DEFAULT));//import images
        standard_img.setIcon(image_standard);
        standard_img.setSize(200,160);
        panel_choose_standard.add(standard_img,BorderLayout.CENTER);

        JLabel standard_choose = new JLabel();
        standard_choose.setPreferredSize(new Dimension(200,50));//set selection button

        JButton button_standard = new JButton("FREE");
        button_standard.setSize(200,50);
        button_standard.setBackground(new Color(250, 250, 250));
        standard_choose.add(button_standard);
        button_standard.setVisible(true);
        panel_choose_standard.add(standard_choose,BorderLayout.SOUTH);


        JLabel vegetarian_text = new JLabel("Vegetarian",SwingConstants.CENTER);
        vegetarian_text.setPreferredSize(new Dimension(200,40));
        vegetarian_text.setFont(new Font(Font.DIALOG,Font.BOLD,20));//set font
        vegetarian_text.setForeground(Color.white);//set text color
        panel_choose_vegetarian.add(vegetarian_text,BorderLayout.NORTH);

        JLabel vegetarian_img = new JLabel();
        vegetarian_img.setPreferredSize(new Dimension(200,160));
        ImageIcon image_vegetarian= new ImageIcon("resource/vegetarian_food.jpg");
      image_vegetarian.setImage(image_vegetarian.getImage().getScaledInstance(200,160,Image.SCALE_DEFAULT));//import images
        vegetarian_img.setIcon(image_vegetarian);
        vegetarian_img.setSize(200,160);
        panel_choose_vegetarian.add(vegetarian_img,BorderLayout.CENTER);

        JLabel vegetarian_choose = new JLabel();
        vegetarian_choose.setPreferredSize(new Dimension(200,50));

        JButton button_vegetarian = new JButton("FREE");
        button_vegetarian.setBackground(new Color(250, 250, 250));
        button_vegetarian.setSize(200,50);
        button_vegetarian.setVisible(true);
        vegetarian_choose.add(button_vegetarian);
        panel_choose_vegetarian.add(vegetarian_choose,BorderLayout.SOUTH);


        JLabel halal_text = new JLabel("Halal",SwingConstants.CENTER);
        halal_text.setPreferredSize(new Dimension(200,40));
        halal_text.setFont(new Font(Font.DIALOG,Font.BOLD,20));//set font
        halal_text.setForeground(Color.white);//set text color
        panel_choose_halal.add(halal_text,BorderLayout.NORTH);

        JLabel halal_img = new JLabel();
        halal_img.setPreferredSize(new Dimension(200,160));
        ImageIcon image_halal= new ImageIcon("resource/halal_food.jpg");
        image_halal.setImage(image_halal.getImage().getScaledInstance(200,160,Image.SCALE_DEFAULT));//import images
        halal_img.setIcon(image_halal);
        halal_img.setSize(180,200);
        panel_choose_halal.add(halal_img,BorderLayout.CENTER);//import images

        JLabel halal_choose = new JLabel();
        halal_choose.setPreferredSize(new Dimension(200,50));//set selection button
        
    JButton button_halal = new JButton("FREE");
        button_halal.setBackground(new Color(250, 250, 250));
        button_halal.setSize(200,50);
        button_halal.setVisible(true);
        halal_choose.add(button_halal);
        panel_choose_halal.add(halal_choose,BorderLayout.SOUTH);

/*
Write logical statements for the function of selecting food. The customer clicks the button to select the food category. The system records the data in the background and the corresponding button is changed in color.
*/
    
        button_standard.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                button_standard.setBackground(new Color(114, 236, 114));
                button_vegetarian.setBackground(new Color(250, 250, 250));
                button_halal.setBackground(new Color(250, 250, 250));
                button_seafood.setBackground(new Color(250, 250, 250));
                button_sushi.setBackground(new Color(250, 250, 250));
                button_steak.setBackground(new Color(250, 250, 250));
/*
When the customer clicks the button, the button changes color, and the other buttons change back to the original color. In this way, if the customer chooses one and changes his mind, the system can display it intuitively.
*/
                food="standard";
            }


        });
        button_vegetarian.addActionListener(new ActionListener() {
            @Override 
            public void actionPerformed(ActionEvent e) {

                button_vegetarian.setBackground(new Color(114, 236, 114));
                button_standard.setBackground(new Color(250, 250, 250));
                button_halal.setBackground(new Color(250, 250, 250));
                button_seafood.setBackground(new Color(250, 250, 250));
                button_sushi.setBackground(new Color(250, 250, 250));
                button_steak.setBackground(new Color(250, 250, 250));

                food="vegetarian";
            }


        });

        button_halal.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                button_halal.setBackground(new Color(114, 236, 114));
                button_standard.setBackground(new Color(250, 250, 250));
                button_vegetarian.setBackground(new Color(250, 250, 250));
                button_seafood.setBackground(new Color(250, 250, 250));
                button_sushi.setBackground(new Color(250, 250, 250));
                button_steak.setBackground(new Color(250, 250, 250));


                food="halal";
            }


        });
        button_seafood.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                button_seafood.setBackground(new Color(114, 236, 114));
                button_standard.setBackground(new Color(250, 250, 250));
                button_halal.setBackground(new Color(250, 250, 250));
                button_vegetarian.setBackground(new Color(250, 250, 250));
                button_sushi.setBackground(new Color(250, 250, 250));
                button_steak.setBackground(new Color(250, 250, 250));

                food="seafood";
            }


        });
        button_steak.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                button_steak.setBackground(new Color(114, 236, 114));
                button_standard.setBackground(new Color(250, 250, 250));
                button_halal.setBackground(new Color(250, 250, 250));
                button_seafood.setBackground(new Color(250, 250, 250));
                button_sushi.setBackground(new Color(250, 250, 250));
                button_vegetarian.setBackground(new Color(250, 250, 250));

                food="steak";
            }


        });
        button_sushi.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                button_sushi.setBackground(new Color(114, 236, 114));
                button_standard.setBackground(new Color(250, 250, 250));
                button_halal.setBackground(new Color(250, 250, 250));
                button_seafood.setBackground(new Color(250, 250, 250));
                button_vegetarian.setBackground(new Color(250, 250, 250));
                button_steak.setBackground(new Color(250, 250, 250));

                food="sushi";
            }


        });

    }
}