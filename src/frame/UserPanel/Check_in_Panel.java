package frame.UserPanel;

import frame.Init;
import frame.Main_Frame;
import utils.GBC;
import utils.MyLayout;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
/**
 * Title             : Check_in_Panel.java
 * Description : This class is a directory for building three login modes.
 */
public class Check_in_Panel extends JPanel {

    public Check_in_Panel(JPanel panelMiddle, JButton jButtonRight){
        //设置背景颜色
        setBackground(new Color(72,46,115));
        //设置初始面板
        JPanel panel = new JPanel();
        panel.setBackground(new Color(72,46,115));
        panel.setPreferredSize(new Dimension(1200,680));
        add(panel);

        panel.setLayout(new MyLayout(panel));


        JPanel Panel1 = new JPanel();
        Panel1.setBackground(new Color(72,46,115));
        panel.add(Panel1, new GBC(0,0,3,1).
                setFill(GBC.BOTH).setIpad(1200, 58).setWeight(10, 0));

        JPanel Panel2 = new JPanel();
        Panel2.setBackground(new Color(72,46,115));
        panel.add(Panel2,new GBC(0,1,1,5).
                setFill(GBC.BOTH).setIpad(260, 544).setWeight(10, 10));

        JPanel Panel3 = new JPanel();
        panel.add(Panel3,new GBC(1,1).
                setFill(GBC.BOTH).setIpad(200, 100).setWeight(10, 10));

        JPanel Panel4 = new JPanel();
        Panel4.setBackground(new Color(72,46,115));
        panel.add(Panel4,new GBC(1,2).
                setFill(GBC.BOTH).setIpad(200, 68).setWeight(10, 10));

        JPanel Panel5 = new JPanel();
        panel.add(Panel5,new GBC(1,3).
                setFill(GBC.BOTH).setIpad(200, 100).setWeight(10, 10));

        JPanel Panel6 = new JPanel();
        Panel6.setBackground(new Color(72,46,115));
        panel.add(Panel6,new GBC(1,4).
                setFill(GBC.BOTH).setIpad(200, 68).setWeight(10, 10));

        JPanel Panel7 = new JPanel();
        panel.add(Panel7,new GBC(1,5).
                setFill(GBC.BOTH).setIpad(200, 100).setWeight(10, 10));

        JPanel Panel8 = new JPanel();
        Panel8.setBackground(new Color(72,46,115));
        panel.add(Panel8,new GBC(2,1 ,1,5).
                setFill(GBC.BOTH).setIpad(260,544).setWeight(10, 0));

        JPanel Panel9 = new JPanel();
        Panel9.setBackground(new Color(72,46,115));
        panel.add(Panel9,new GBC(0,6,3,1).
                setFill(GBC.BOTH).setIpad(1200,78).setWeight(10, 0));


        JButton buttonTOP = new JButton("INPUT THE BOOKING NUMBER");
        buttonTOP.setBackground(new Color(96,62,151));
        buttonTOP.setFont(new Font (Font.DIALOG,Font.BOLD, 42));
        buttonTOP.setForeground(Color.white);

        JButton buttonMID = new JButton("ENTER THE ID NUMBER");
        buttonMID.setBackground(new Color(96,62,151));
        buttonMID.setFont(new Font (Font.DIALOG,Font.BOLD, 42));
        buttonMID.setForeground(Color.white);

        JButton buttonBOT = new JButton("SCAN THE ID DOCUMENT");
        buttonBOT.setBackground(new Color(96,62,151));
        buttonBOT.setFont(new Font (Font.DIALOG,Font.BOLD, 42));
        buttonBOT.setForeground(Color.white);



        Panel3.setLayout(new GridLayout(1, 1));
        Panel3.add(buttonTOP);
        Panel5.setLayout(new GridLayout(1, 1));
        Panel5.add(buttonMID);
        Panel7.setLayout(new GridLayout(1, 1));
        Panel7.add(buttonBOT);

        buttonTOP.addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Init.cardLayout.show(panelMiddle, "2");
                Main_Frame.pageNum++;
                jButtonRight.setText("CONTINUE");
                jButtonRight.setBackground(new Color(132, 177, 132));
                jButtonRight.setVisible(true);
            }
        });

        buttonMID.addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Init.cardLayout.show(panelMiddle, "3");
                jButtonRight.setText("CONTINUE");
                jButtonRight.setBackground(new Color(132, 177, 132));
                jButtonRight.setVisible(true);
                Main_Frame.pageNum=Main_Frame.pageNum+2;

            }
        });

        buttonBOT.addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Init.cardLayout.show(panelMiddle, "4");
                Main_Frame.pageNum=Main_Frame.pageNum+3;
                jButtonRight.setText("CONTINUE");
                jButtonRight.setBackground(new Color(132, 177, 132));
                jButtonRight.setVisible(true);
            }
        });





        /*JButton jButton = new JButton("121");
        panel.add(jButton);

        jButton.addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Init.cardLayout.show(panelMiddle, "8");
            }
        });*/




    }

}
