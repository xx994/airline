package frame.UserPanel;


import frame.Main_Frame;
import model.*;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;
/**
 * <p>This page is mainly for the passengers to choose which flight to check in and see the detail information of the flights. </p>
 *
 * @author Jiayi Yu
 * @version 3.1
 */

public class Flights_Panel extends JPanel {

    public static String idNum=""; // ID number initialization

    passenger_order po = new passenger_order();   // Initialization of passenger's order
    flight flight_object1 = new flight();  // Initialization of the "flight" object
    public static String flight_num = ""; //  Initialization of the flight number
    public static String flight_order=""; //  Initialization of the flight order
    public static  int page = 1 ; // set the number of page in this panel
    public  int num ; // Initialization of an integer number which stands for the number of flights which the current passenger is able to check in

    String flight1_order ;
    String flight2_order ;
    String flight3_order ;
    String flight4_order ;
    // Initialization of the orders of these four flights

    String flight1 ;
    String flight2 ;
    String flight3 ;
    String flight4 ;
    // Initialization of these four flights

    public Flights_Panel() {


        idNum= Main_Frame.temp;


        setBackground(new Color(72,46,115)); //Set background color

        JPanel panel = new JPanel(); //Set the initial panel
        panel.setBackground(new Color(72,46,115)); // Set the background color of the initial panel
        panel.setPreferredSize(new Dimension(1200,680)); // Set the size of the initial panel
        add(panel);  // Add the initial panel to this page
        panel.setLayout(null);

        JPanel panel0 = new JPanel();   // Create a title panel at the top of the initial panel
        BorderLayout f1 = new BorderLayout();
        panel0.setLayout(f1);               // set the layout of the title panel
        panel0.setBounds(0,0,1200,100);// Set the size of the title panel
        panel0.setBackground(new Color(72, 46, 115));// Set the color of the title panel

        JLabel title = new JLabel("SELECT THE FLIGHTS YOU WILL BE CHECKING IN FOR",SwingConstants.CENTER);
        // Create a new text label as title
        title.setFont(new Font(Font.DIALOG,Font.BOLD,30));// Set text font
        title.setForeground(Color.white);// Set text color
        title.setOpaque(true);
        title.setBackground(new Color(72,46,115)); // Set the color of this label
        title.setPreferredSize(new Dimension(800,80)); // Set the size of this label
        panel0.add(title,BorderLayout.CENTER); // Add this label to the title panel



        JLabel  leftButton_label = new JLabel(); // Create a left page turning label
        leftButton_label.setPreferredSize(new Dimension(100,80)); // Set the size of this label
        BorderLayout f2 = new BorderLayout();
        leftButton_label.setLayout(f2); // Set the layout of this label
        panel0.add(leftButton_label,BorderLayout.WEST); // Add this label on the left of the title panel

        JLabel  rightButton_label = new JLabel(); // Create a right page turning label
        rightButton_label.setPreferredSize(new Dimension(100,80));// Set the size of this label
        BorderLayout f3 = new BorderLayout();
        rightButton_label.setLayout(f3); // Set the layout of this label
        panel0.add(rightButton_label,BorderLayout.EAST); // Add this label on the right of the title panel

        JButton jButtonLast = new JButton("LAST"); // Create a button with text on the left page turning label
        jButtonLast.setFont(new Font(Font.DIALOG,Font.BOLD,23));// Set text font
        jButtonLast.setForeground(Color.white);                      // Set text color
        jButtonLast.setBackground(new Color(100,100,100));  // Set the size of this button

        JButton jButtonNext = new JButton("NEXT"); // Create a button with text on the right page turning label
        jButtonNext.setFont(new Font(Font.DIALOG,Font.BOLD,23));// Set text font
        jButtonNext.setForeground(Color.white);                     // Set text color
        jButtonNext.setBackground(new Color(100,100,100));// Set the size of this button


        rightButton_label.add(jButtonNext,BorderLayout.CENTER);
        leftButton_label.add(jButtonLast,BorderLayout.CENTER);

        //String id_num = "360124200102040017"; which is just for software testing


        num =po.orderNum(idNum);  // get the number of flights which the current passenger is able to check in.

        if(num>2) {
            jButtonNext.setVisible(true);
        }
        if(num<=2){
            jButtonNext.setVisible(false);
        }
        jButtonLast.setVisible(false);
        // "Next" button and "Last" button will be visible only if the number of flights which the current passenger is able to check in is bigger than 2


        flight1_order = po.findOrder(idNum,1);
        flight1 = flight_object1.findFlight(flight1_order); // Get the detail information of the first flight of current passenger

        JPanel panel1 = new JPanel(); // Create a new middle-size panel for the first flight to show all important information
        panel1.setBounds(150,120,900,220); // Set the size of this panel
        panel1.setBackground(new Color(72,46,20)); // Set the background color of this panel

        GridBagLayout gbl1 = new GridBagLayout();
        GridBagConstraints gbc1 = new GridBagConstraints();
        panel1.setLayout(gbl1);
        gbc1.fill=GridBagConstraints.BOTH;
        BorderLayout f11 = new BorderLayout();
        // Set layout of this panel

        JPanel panel1_11 = new JPanel(f11); // Create a panel to contain the text "FLIGHT 1"
        JLabel label1_11 = new JLabel("FLIGHT 1",SwingConstants.CENTER); // Create the text label
        label1_11.setPreferredSize(new Dimension(135,66)); // Set size
        label1_11.setFont(new Font(Font.DIALOG,Font.PLAIN,28));// Set text font
        label1_11.setForeground(Color.white);// Set text color
        label1_11.setOpaque(true);
        label1_11.setBackground(new Color(220 ,66, 75)); //Set the background color of this panel
        panel1_11.add(label1_11,BorderLayout.CENTER); // Set the position of the text label

        // Specify the assignment area for the component
        gbc1.weightx=0.15;
        gbc1.weighty=0.3;
        gbc1.gridwidth=1;
        gbc1.gridheight=1;
        gbc1.gridx=0;
        gbc1.gridy=0;
        gbl1.setConstraints(panel1_11,gbc1);

        panel1.add(panel1_11); // Add this tiny-size panel to the middle-size panel

        eachFlight ef1 = new eachFlight(); // Initialization of "eachFlight" (for more details please see the class"eachFlight")

        BorderLayout f12 = new BorderLayout(); // Set the layout

        JPanel panel1_12 = new JPanel(f12); // Create another panel to contain the title which instructs the detailed information of the first flight

        String flight1_company= ef1.COMPANY(flight1); // Get the company of this flight
        String flight1_boardingTime= ef1.boardingTime(flight1); // Get the boarding time of this flight
        String flight1_gate= ef1.GATE(flight1); // Get the boarding gate number of this flight

        JLabel label1_12 = new JLabel(flight1+" "+flight1_company+"         "+"BOARDING TIME  "+flight1_boardingTime+"         "+"GATE "+flight1_gate,SwingConstants.CENTER);
        // Create a new text label to show the text
        label1_12.setPreferredSize(new Dimension(765,66)); // Set size
        label1_12.setFont(new Font(Font.DIALOG,Font.BOLD,20));// Set text font
        label1_12.setForeground(Color.white);// Set text color
        label1_12.setOpaque(true);
        label1_12.setBackground(new Color(56, 53, 53));  //Set the background color of this panel
        panel1_12.add(label1_12,BorderLayout.CENTER); // Set the position of the text label

        // Specify the assignment area for the component
        gbc1.weightx=0.85;
        gbc1.weighty=0.3;
        gbc1.gridwidth=2;
        gbc1.gridheight=1;
        gbc1.gridx=1;
        gbc1.gridy=0;
        gbl1.setConstraints(panel1_12,gbc1);

        panel1.add(panel1_12); // Add this tiny-size panel to the middle-size panel

        BorderLayout f21 =new BorderLayout(); // Set the layout
        JPanel panel1_21 = new JPanel(f21);// Create another panel
        JLabel label1_21 = new JLabel(); // Create a new label
        label1_21.setPreferredSize(new Dimension(135,154)); // Set size
        label1_21.setOpaque(true);
        label1_21.setBackground(new Color(213, 213, 12)); // Set background color
        panel1_21.add(label1_21,BorderLayout.CENTER);// Set the position of this label

        // Specify the assignment area for the component
        gbc1.weightx=0.15;
        gbc1.weighty=0.7;
        gbc1.gridwidth=1;
        gbc1.gridheight=1;
        gbc1.gridx=0;
        gbc1.gridy=1;
        gbl1.setConstraints(panel1_21,gbc1);
        panel1.add(panel1_21); // Add this tiny-size panel to the middle-size panel

        BorderLayout f22 = new BorderLayout();
        JPanel panel1_22 = new JPanel(f22);
        JLabel label1_22 = new JLabel();
        label1_22.setPreferredSize(new Dimension(315,154));
        label1_22.setOpaque(true);
        label1_22.setBackground(new Color(6, 39, 206));
        panel1_22.add(label1_22,BorderLayout.CENTER);

        // Specify the assignment area for the component
        gbc1.weightx=0.35;
        gbc1.weighty=0.7;
        gbc1.gridwidth=1;
        gbc1.gridheight=1;
        gbc1.gridx=1;
        gbc1.gridy=1;
        gbl1.setConstraints(panel1_22,gbc1);
        panel1.add(panel1_22); // Add this tiny-size panel to the middle-size panel


        JPanel panel1_23 = new JPanel();
        panel1_23.setPreferredSize(new Dimension(450,154));

        // Specify the assignment area for the component
        gbc1.weightx=0.5;
        gbc1.weighty=0.7;
        gbc1.gridwidth=1;
        gbc1.gridheight=1;
        gbc1.gridx=2;
        gbc1.gridy=1;
        gbl1.setConstraints(panel1_23,gbc1);
        panel1.add(panel1_23); // Add this tiny-size panel to the middle-size panel

        GridBagLayout gbl1_small = new GridBagLayout(); // Set layout
        GridBagConstraints gbc1_small = new GridBagConstraints();
        panel1_23.setLayout(gbl1_small);
        gbc1_small.fill=GridBagConstraints.BOTH;

        String flight1_departure = ef1.DEPARTURE(flight1); // Get the departure place of the first flight
        JLabel label1_2311 = new JLabel(flight1_departure,SwingConstants.CENTER);// Create the label
        label1_2311.setPreferredSize(new Dimension(180,66));// Set size
        label1_2311.setFont(new Font(Font.DIALOG,Font.BOLD,18));// Set text font
        label1_2311.setForeground(Color.black);// Set text color
        label1_2311.setOpaque(true);
        label1_2311.setBackground(new Color(245, 240, 240));// Set background color

        // Specify the assignment area for the component
        gbc1_small.weightx=0.4;
        gbc1_small.weighty=0.4;
        gbc1_small.gridwidth=1;
        gbc1_small.gridheight=1;
        gbc1_small.gridx=0;
        gbc1_small.gridy=0;
        gbl1_small.setConstraints(label1_2311,gbc1_small);
        panel1_23.add(label1_2311);// Add this tiny-size panel to the middle-size panel

        String flight1_departs = ef1.DEPARTS(flight1); // Get the time of departing
        JLabel label1_2312 = new JLabel("DEPARTS:"+flight1_departs,SwingConstants.CENTER);// Create a label show detailed information
        label1_2312.setPreferredSize(new Dimension(270,66)); // Set size
        label1_2312.setFont(new Font(Font.DIALOG,Font.BOLD,18));// Set text font
        label1_2312.setForeground(Color.black);// Set text color
        label1_2312.setOpaque(true);
        label1_2312.setBackground(new Color(233 ,233, 233)); //Set background color

        // Specify the assignment area for the component
        gbc1_small.weightx=0.6;
        gbc1_small.weighty=0.4;
        gbc1_small.gridwidth=1;
        gbc1_small.gridheight=1;
        gbc1_small.gridx=1;
        gbc1_small.gridy=0;
        gbl1_small.setConstraints(label1_2312,gbc1_small);
        panel1_23.add(label1_2312);// Add this tiny-size panel to the middle-size panel

        String flight1_destination = ef1.DESTINATION(flight1); // Get the destination of this flight
        JLabel label1_2321 = new JLabel(flight1_destination,SwingConstants.CENTER);// Create a new label to show the destination of this flight
        label1_2321.setPreferredSize(new Dimension(180,66));// Set text size
        label1_2321.setFont(new Font(Font.DIALOG,Font.BOLD,18));// Set text font
        label1_2321.setForeground(Color.black);// Set text color
        label1_2321.setOpaque(true);
        label1_2321.setBackground(new Color(245, 240, 240)); // Set background color

        // Specify the assignment area for the component
        gbc1_small.weightx=0.4;
        gbc1_small.weighty=0.4;
        gbc1_small.gridwidth=1;
        gbc1_small.gridheight=1;
        gbc1_small.gridx=0;
        gbc1_small.gridy=1;
        gbl1_small.setConstraints(label1_2321,gbc1_small);
        panel1_23.add(label1_2321);// Add this tiny-size panel to the middle-size panel

        String flight1_arrives = ef1.ARRIVES(flight1); // Get the arrival of this flight
        JLabel label1_2322 = new JLabel("ARRIVES:"+flight1_arrives,SwingConstants.CENTER);// Create a new label to show the arrival of this flight
        label1_2322.setPreferredSize(new Dimension(270,66));// Set text size
        label1_2322.setFont(new Font(Font.DIALOG,Font.BOLD,18));// Set text font
        label1_2322.setForeground(Color.black);// Set text color
        label1_2322.setOpaque(true);
        label1_2322.setBackground(new Color(213 ,213, 213));// Set background color

        // Specify the assignment area for the component
        gbc1_small.weightx=0.6;
        gbc1_small.weighty=0.4;
        gbc1_small.gridwidth=1;
        gbc1_small.gridheight=1;
        gbc1_small.gridx=1;
        gbc1_small.gridy=1;
        gbl1_small.setConstraints(label1_2322,gbc1_small);
        panel1_23.add(label1_2322);// Add this tiny-size panel to the middle-size panel

        String flight1_delay = ef1.DELAY(flight1); // Get the delay time of the first flight

        // On time: flight1_delay = 1.
        // Delay: flight1_delay = 0.
        if (flight1_delay.equals("1")) {
            JLabel label1_233 = new JLabel("ON TIME", SwingConstants.CENTER); //Create a text label
            label1_233.setPreferredSize(new Dimension(270, 66));// Set size
            label1_233.setFont(new Font(Font.DIALOG, Font.BOLD, 21));// Set font
            label1_233.setForeground(Color.white);// Set text color
            label1_233.setOpaque(true);
            label1_233.setBackground(new Color(104, 210, 128));// Set background color

            // Specify the assignment area for the component
            gbc1_small.weightx=1;
            gbc1_small.weighty=0.2;
            gbc1_small.gridwidth=2;
            gbc1_small.gridheight=1;
            gbc1_small.gridx=0;
            gbc1_small.gridy=2;
            gbl1_small.setConstraints(label1_233,gbc1_small);
            panel1_23.add(label1_233);// Add this tiny-size panel to the middle-size panel
        }
        if (flight1_delay.equals("0")) {
            JLabel label1_233 = new JLabel("DELAY", SwingConstants.CENTER);
            label1_233.setPreferredSize(new Dimension(270, 66));// Set size
            label1_233.setFont(new Font(Font.DIALOG, Font.BOLD, 21));// Set text font
            label1_233.setForeground(Color.white);// Set text color
            label1_233.setOpaque(true);
            label1_233.setBackground(new Color(222, 227, 108)); // Set background color

            // Specify the assignment area for the component
            gbc1_small.weightx=1;
            gbc1_small.weighty=0.2;
            gbc1_small.gridwidth=2;
            gbc1_small.gridheight=1;
            gbc1_small.gridx=0;
            gbc1_small.gridy=2;
            gbl1_small.setConstraints(label1_233,gbc1_small);
            panel1_23.add(label1_233);// Add this tiny-size panel to the middle-size panel
        }


        JButton button1 = new JButton(); // Create a button for passenger to pick which flight to check in
        button1.setBackground(new Color(96 ,84, 146)); // Set color
        panel1_21.add(button1); // Add it to the panel

        String image_name1= ef1.IMG(flight1);// Get the city image of the first flight
        ImageIcon image1 = new ImageIcon("resource/"+image_name1);// Create an ImageIcon
        image1.setImage(image1.getImage().getScaledInstance(315,154,Image.SCALE_DEFAULT));// Set the image size
        label1_22.setIcon(image1); // put the image on the label
        label1_22.setSize(315,154); // Set the label's size

        JPanel panel2 = new JPanel(); // Create a new panel for the second flight
        panel2.setBounds(150, 360, 900, 220);  // Set size
        panel2.setBackground(new Color(20, 20, 115));   // Set the background color
        JButton button2 = new JButton();                        // Create a new button
        button2.setBackground(new Color(96 ,84, 146));// Set the background color of this button

        /* To judge if there is a second flight.
        if there has, create the tiny-size panels and labels just as the first one
        */
        if(num>=2) {

            /* This part is just as the part of the first flight.
            To make codes clean and clear, less important and repetitive annotations won't be shown here.
            Any questions please reference to the annotations in the part of flight.
             */
            flight flight_object2 = new flight();
            flight2_order = po.findOrder(idNum,2);
            flight2 = flight_object2.findFlight(flight2_order);

            GridBagLayout gbl2 = new GridBagLayout();
            GridBagConstraints gbc2 = new GridBagConstraints();
            panel2.setLayout(gbl2);
            gbc2.fill = GridBagConstraints.BOTH;


            BorderLayout f2_11 = new BorderLayout();
            JPanel panel2_11 = new JPanel(f2_11);
            JLabel label2_11 = new JLabel("FLIGHT 2", SwingConstants.CENTER);
            label2_11.setPreferredSize(new Dimension(135, 66));
            label2_11.setFont(new Font(Font.DIALOG, Font.PLAIN, 28)); //Set the text font
            label2_11.setForeground(Color.white);// Set the text color
            label2_11.setOpaque(true);
            label2_11.setBackground(new Color(220 ,66, 75));
            panel2_11.add(label2_11, BorderLayout.CENTER);

            // Specify the assignment area for the component
            gbc2.weightx = 0.15;
            gbc2.weighty = 0.3;
            gbc2.gridwidth = 1;
            gbc2.gridheight = 1;
            gbc2.gridx = 0;
            gbc2.gridy = 0;
            gbl2.setConstraints(panel2_11, gbc2);
            panel2.add(panel2_11);


            BorderLayout f2_12 = new BorderLayout();
            JPanel panel2_12 = new JPanel(f2_12);
            eachFlight ef2 = new eachFlight();
            String flight2_company= ef2.COMPANY(flight2);
            String flight2_boardingTime= ef2.boardingTime(flight2);
            String flight2_gate= ef2.GATE(flight2);
            JLabel label2_12 = new JLabel(flight2+" "+flight1_company+"         "+"BOARDING TIME  "+flight2_boardingTime+"         "+"GATE "+flight2_gate,SwingConstants.CENTER);
            label2_12.setPreferredSize(new Dimension(765, 66));
            label2_12.setFont(new Font(Font.DIALOG, Font.BOLD, 20));// Set the text font
            label2_12.setForeground(Color.white);// Set the text color
            label2_12.setOpaque(true);
            label2_12.setBackground(new Color(56, 53, 53));
            panel2_12.add(label2_12, BorderLayout.CENTER);

            // Specify the assignment area for the component
            gbc2.weightx = 0.85;
            gbc2.weighty = 0.3;
            gbc2.gridwidth = 2;
            gbc2.gridheight = 1;
            gbc2.gridx = 1;
            gbc2.gridy = 0;
            gbl2.setConstraints(panel2_12, gbc2);
            panel2.add(panel2_12);

            BorderLayout f2_21 = new BorderLayout();
            JPanel panel2_21 = new JPanel(f2_21);
            JLabel label2_21 = new JLabel();
            label2_21.setPreferredSize(new Dimension(135, 154));
            label2_21.setOpaque(true);
            label2_21.setBackground(new Color(213, 213, 12));
            panel2_21.add(label2_21, BorderLayout.CENTER);

            // Specify the assignment area for the component
            gbc2.weightx = 0.15;
            gbc2.weighty = 0.7;
            gbc2.gridwidth = 1;
            gbc2.gridheight = 1;
            gbc2.gridx = 0;
            gbc2.gridy = 1;
            gbl2.setConstraints(panel2_21, gbc2);
            panel2.add(panel2_21);

            BorderLayout f2_22 = new BorderLayout();
            JPanel panel2_22 = new JPanel(f2_22);
            JLabel label2_22 = new JLabel();
            label2_22.setPreferredSize(new Dimension(315, 154));
            label2_22.setOpaque(true);
            label2_22.setBackground(new Color(6, 39, 206));
            panel2_22.add(label2_22, BorderLayout.CENTER);

            // Specify the assignment area for the component
            gbc2.weightx = 0.35;
            gbc2.weighty = 0.7;
            gbc2.gridwidth = 1;
            gbc2.gridheight = 1;
            gbc2.gridx = 1;
            gbc2.gridy = 1;
            gbl2.setConstraints(panel2_22, gbc2);
            panel2.add(panel2_22);


            JPanel panel2_23 = new JPanel();
            panel2_23.setPreferredSize(new Dimension(450, 154));

            // Specify the assignment area for the component
            gbc2.weightx = 0.5;
            gbc2.weighty = 0.7;
            gbc2.gridwidth = 1;
            gbc2.gridheight = 1;
            gbc2.gridx = 2;
            gbc2.gridy = 1;
            gbl2.setConstraints(panel2_23, gbc2);
            panel2.add(panel2_23);

            GridBagLayout gbl2_small = new GridBagLayout();
            GridBagConstraints gbc2_small = new GridBagConstraints();
            panel2_23.setLayout(gbl2_small);
            gbc2_small.fill = GridBagConstraints.BOTH;

            String flight2_departure= ef2.DEPARTURE(flight2);
            JLabel label2_2311 = new JLabel(flight2_departure, SwingConstants.CENTER);
            label2_2311.setPreferredSize(new Dimension(180, 66));
            label2_2311.setFont(new Font(Font.DIALOG, Font.BOLD, 18));
            label2_2311.setForeground(Color.black);
            label2_2311.setOpaque(true);
            label2_2311.setBackground(new Color(245, 240, 240));

            // Specify the assignment area for the component
            gbc2_small.weightx = 0.4;
            gbc2_small.weighty = 0.4;
            gbc2_small.gridwidth = 1;
            gbc2_small.gridheight = 1;
            gbc2_small.gridx = 0;
            gbc2_small.gridy = 0;
            gbl2_small.setConstraints(label2_2311, gbc2_small);
            panel2_23.add(label2_2311);

            String flight2_departs= ef2.DEPARTS(flight2);
            JLabel label2_2312 = new JLabel("DEPARTS:"+flight2_departs, SwingConstants.CENTER);
            label2_2312.setPreferredSize(new Dimension(270, 66));
            label2_2312.setFont(new Font(Font.DIALOG, Font.BOLD, 18));
            label2_2312.setForeground(Color.black);
            label2_2312.setOpaque(true);
            label2_2312.setBackground(new Color(233 ,233, 233));

            // Specify the assignment area for the component
            gbc2_small.weightx = 0.6;
            gbc2_small.weighty = 0.4;
            gbc2_small.gridwidth = 1;
            gbc2_small.gridheight = 1;
            gbc2_small.gridx = 1;
            gbc2_small.gridy = 0;
            gbl2_small.setConstraints(label2_2312, gbc2_small);
            panel2_23.add(label2_2312);

            String flight2_destination= ef2.DESTINATION(flight2);
            JLabel label2_2321 = new JLabel(flight2_destination, SwingConstants.CENTER);
            label2_2321.setPreferredSize(new Dimension(180, 66));
            label2_2321.setFont(new Font(Font.DIALOG, Font.BOLD, 18));//设置文字字体
            label2_2321.setForeground(Color.black);//设置文字的颜色
            label2_2321.setOpaque(true);
            label2_2321.setBackground(new Color(245, 240, 240));
            gbc2_small.weightx = 0.4;    // 指定组件的分配区域
            gbc2_small.weighty = 0.4;
            gbc2_small.gridwidth = 1;
            gbc2_small.gridheight = 1;
            gbc2_small.gridx = 0;
            gbc2_small.gridy = 1;
            gbl2_small.setConstraints(label2_2321, gbc2_small);
            panel2_23.add(label2_2321);

            String flight2_arrives= ef2.ARRIVES(flight2);
            JLabel label2_2322 = new JLabel("ARRIVES:"+flight2_arrives, SwingConstants.CENTER);
            label2_2322.setPreferredSize(new Dimension(270, 66));
            label2_2322.setFont(new Font(Font.DIALOG, Font.BOLD, 18));
            label2_2322.setForeground(Color.black);
            label2_2322.setOpaque(true);
            label2_2322.setBackground(new Color(213 ,213, 213));

            // Specify the assignment area for the component
            gbc2_small.weightx = 0.6;
            gbc2_small.weighty = 0.4;
            gbc2_small.gridwidth = 1;
            gbc2_small.gridheight = 1;
            gbc2_small.gridx = 1;
            gbc2_small.gridy = 1;
            gbl2_small.setConstraints(label2_2322, gbc2_small);
            panel2_23.add(label2_2322);

            String flight2_delay= ef2.DELAY(flight2);
            if(flight2_delay.equals("1")) {
                JLabel label2_233 = new JLabel("ON TIME", SwingConstants.CENTER);
                label2_233.setPreferredSize(new Dimension(270, 66));
                label2_233.setFont(new Font(Font.DIALOG, Font.BOLD, 21));
                label2_233.setForeground(Color.white);
                label2_233.setOpaque(true);
                label2_233.setBackground(new Color(104, 210, 128));

                // Specify the assignment area for the component
                gbc2_small.weightx = 1;
                gbc2_small.weighty = 0.2;
                gbc2_small.gridwidth = 2;
                gbc2_small.gridheight = 1;
                gbc2_small.gridx = 0;
                gbc2_small.gridy = 2;
                gbl2_small.setConstraints(label2_233, gbc2_small);
                panel2_23.add(label2_233);
            }
            if(flight2_delay.equals("0")){
                JLabel label2_233 = new JLabel("DELAYED", SwingConstants.CENTER);
                label2_233.setPreferredSize(new Dimension(270, 66));
                label2_233.setFont(new Font(Font.DIALOG, Font.BOLD, 21));
                label2_233.setForeground(Color.white);
                label2_233.setOpaque(true);
                label2_233.setBackground(new Color(222, 227, 108));

                // Specify the assignment area for the component
                gbc2_small.weightx = 1;
                gbc2_small.weighty = 0.2;
                gbc2_small.gridwidth = 2;
                gbc2_small.gridheight = 1;
                gbc2_small.gridx = 0;
                gbc2_small.gridy = 2;
                gbl2_small.setConstraints(label2_233, gbc2_small);
                panel2_23.add(label2_233);
            }


            panel2_21.add(button2);

            String image_name2= ef2.IMG(flight2);
            ImageIcon image2 = new ImageIcon("resource/"+image_name2);
            image2.setImage(image2.getImage().getScaledInstance(315, 154, Image.SCALE_DEFAULT));
            label2_22.setIcon(image2);
            label2_22.setSize(315, 154);

            panel.add(panel2);
        }

        JPanel panel3 = new JPanel();
        JButton button3 = new JButton();
        button3.setBackground(new Color(96 ,84, 146));

        /* To judge if there is a third flight.
        if there has, create the tiny-size panels and labels just as the first one
        */
        if(num>=3) {
            /*This part is just as the part of the first flight.
            To make codes clean and clear, less important and repetitive annotations won't be shown here.
            Any questions please reference to the annotations in the part of flight.
             */
            flight flight_object3 = new flight();
            flight3_order = po.findOrder(idNum,3);
            flight3 = flight_object3.findFlight(flight3_order);

            panel3.setBounds(150,120,900,220);
            panel3.setBackground(new Color(72,46,20));

            GridBagLayout gbl3 = new GridBagLayout();
            GridBagConstraints gbc3 = new GridBagConstraints();
            panel3.setLayout(gbl3);
            gbc3.fill=GridBagConstraints.BOTH;


            BorderLayout f3_11 = new BorderLayout();
            JPanel panel3_11 = new JPanel(f3_11);
            JLabel label3_11 = new JLabel("FLIGHT 3",SwingConstants.CENTER);
            label3_11.setPreferredSize(new Dimension(135,66));
            label3_11.setFont(new Font(Font.DIALOG,Font.PLAIN,28));
            label3_11.setForeground(Color.white);
            label3_11.setOpaque(true);
            label3_11.setBackground(new Color(220 ,66, 75));
            panel3_11.add(label3_11,BorderLayout.CENTER);
            gbc3.weightx=0.15;
            gbc3.weighty=0.3;
            gbc3.gridwidth=1;
            gbc3.gridheight=1;
            gbc3.gridx=0;
            gbc3.gridy=0;
            gbl3.setConstraints(panel3_11,gbc3);
            panel3.add(panel3_11);

            eachFlight ef3 = new eachFlight();

            BorderLayout f3_12 = new BorderLayout();
            JPanel panel3_12 = new JPanel(f3_12);

            String flight3_company= ef3.COMPANY(flight3);
            String flight3_boardingTime= ef3.boardingTime(flight3);
            String flight3_gate= ef3.GATE(flight3);
            JLabel label3_12 = new JLabel(flight3+" "+flight3_company+"         "+"BOARDING TIME  "+flight3_boardingTime+"         "+"GATE"+flight3_gate,SwingConstants.CENTER);
            label3_12.setPreferredSize(new Dimension(765,66));
            label3_12.setFont(new Font(Font.DIALOG,Font.BOLD,23));
            label3_12.setForeground(Color.white);
            label3_12.setOpaque(true);
            label3_12.setBackground(new Color(56, 53, 53));
            panel3_12.add(label3_12,BorderLayout.CENTER);
            gbc3.weightx=0.85;
            gbc3.weighty=0.3;
            gbc3.gridwidth=2;
            gbc3.gridheight=1;
            gbc3.gridx=1;
            gbc3.gridy=0;
            gbl3.setConstraints(panel3_12,gbc3);
            panel3.add(panel3_12);

            BorderLayout f3_21 =new BorderLayout();
            JPanel panel3_21 = new JPanel(f3_21);
            JLabel label3_21 = new JLabel();
            label3_21.setPreferredSize(new Dimension(135,154));
            label3_21.setOpaque(true);
            label3_21.setBackground(new Color(213, 213, 12));
            panel3_21.add(label3_21,BorderLayout.CENTER);
            gbc3.weightx=0.15;
            gbc3.weighty=0.7;
            gbc3.gridwidth=1;
            gbc3.gridheight=1;
            gbc3.gridx=0;
            gbc3.gridy=1;
            gbl3.setConstraints(panel3_21,gbc3);
            panel3.add(panel3_21);

            BorderLayout f3_22 = new BorderLayout();
            JPanel panel3_22 = new JPanel(f3_22);
            JLabel label3_22 = new JLabel();
            label3_22.setPreferredSize(new Dimension(315,154));
            label3_22.setOpaque(true);
            label3_22.setBackground(new Color(6, 39, 206));
            panel3_22.add(label3_22,BorderLayout.CENTER);
            gbc3.weightx=0.35;
            gbc3.weighty=0.7;
            gbc3.gridwidth=1;
            gbc3.gridheight=1;
            gbc3.gridx=1;
            gbc3.gridy=1;
            gbl3.setConstraints(panel3_22,gbc3);
            panel3.add(panel3_22);


            JPanel panel3_23 = new JPanel();
            panel3_23.setPreferredSize(new Dimension(450,154));
            gbc3.weightx=0.5;
            gbc3.weighty=0.7;
            gbc3.gridwidth=1;
            gbc3.gridheight=1;
            gbc3.gridx=2;
            gbc3.gridy=1;
            gbl3.setConstraints(panel3_23,gbc3);
            panel3.add(panel3_23);

            GridBagLayout gbl3_small = new GridBagLayout();
            GridBagConstraints gbc3_small = new GridBagConstraints();
            panel3_23.setLayout(gbl3_small);
            gbc3_small.fill=GridBagConstraints.BOTH;

            String flight3_departure = ef3.DEPARTURE(flight3);
            JLabel label3_2311 = new JLabel(flight3_departure,SwingConstants.CENTER);
            label3_2311.setPreferredSize(new Dimension(180,66));
            label3_2311.setFont(new Font(Font.DIALOG,Font.BOLD,18));
            label3_2311.setForeground(Color.black);
            label3_2311.setOpaque(true);
            label3_2311.setBackground(new Color(245, 240, 240));
            gbc3_small.weightx=0.4;
            gbc3_small.weighty=0.4;
            gbc3_small.gridwidth=1;
            gbc3_small.gridheight=1;
            gbc3_small.gridx=0;
            gbc3_small.gridy=0;
            gbl3_small.setConstraints(label3_2311,gbc3_small);
            panel3_23.add(label3_2311);

            String flight3_departs = ef3.DEPARTS(flight3);
            JLabel label3_2312 = new JLabel("DEPARTS:"+flight3_departs,SwingConstants.CENTER);
            label3_2312.setPreferredSize(new Dimension(270,66));
            label3_2312.setFont(new Font(Font.DIALOG,Font.BOLD,18));
            label3_2312.setForeground(Color.black);
            label3_2312.setOpaque(true);
            label3_2312.setBackground(new Color(233 ,233, 233));
            gbc3_small.weightx=0.6;
            gbc3_small.weighty=0.4;
            gbc3_small.gridwidth=1;
            gbc3_small.gridheight=1;
            gbc3_small.gridx=1;
            gbc3_small.gridy=0;
            gbl3_small.setConstraints(label3_2312,gbc3_small);
            panel3_23.add(label3_2312);

            String flight3_destination = ef3.DESTINATION(flight3);
            JLabel label3_2321 = new JLabel(flight3_destination,SwingConstants.CENTER);
            label3_2321.setPreferredSize(new Dimension(180,66));
            label3_2321.setFont(new Font(Font.DIALOG,Font.BOLD,18));
            label3_2321.setForeground(Color.black);
            label3_2321.setOpaque(true);
            label3_2321.setBackground(new Color(245, 240, 240));
            gbc3_small.weightx=0.4;
            gbc3_small.weighty=0.4;
            gbc3_small.gridwidth=1;
            gbc3_small.gridheight=1;
            gbc3_small.gridx=0;
            gbc3_small.gridy=1;
            gbl3_small.setConstraints(label3_2321,gbc3_small);
            panel3_23.add(label3_2321);

            String flight3_arrives = ef3.ARRIVES(flight3);
            JLabel label3_2322 = new JLabel("ARRIVES:"+flight3_arrives,SwingConstants.CENTER);
            label3_2322.setPreferredSize(new Dimension(270,66));
            label3_2322.setFont(new Font(Font.DIALOG,Font.BOLD,18));
            label3_2322.setForeground(Color.black);
            label3_2322.setOpaque(true);
            label3_2322.setBackground(new Color(213 ,213, 213));
            gbc3_small.weightx=0.6;
            gbc3_small.weighty=0.4;
            gbc3_small.gridwidth=1;
            gbc3_small.gridheight=1;
            gbc3_small.gridx=1;
            gbc3_small.gridy=1;
            gbl3_small.setConstraints(label3_2322,gbc3_small);
            panel3_23.add(label3_2322);

            String flight3_delay = ef3.DELAY(flight3);
            if (flight3_delay.equals("1")) {
                JLabel label3_233 = new JLabel("ON TIME", SwingConstants.CENTER);
                label3_233.setPreferredSize(new Dimension(270, 66));
                label3_233.setFont(new Font(Font.DIALOG, Font.BOLD, 21));
                label3_233.setForeground(Color.white);
                label3_233.setOpaque(true);
                label3_233.setBackground(new Color(104, 210, 128));
                gbc3_small.weightx=1;
                gbc3_small.weighty=0.2;
                gbc3_small.gridwidth=2;
                gbc3_small.gridheight=1;
                gbc3_small.gridx=0;
                gbc3_small.gridy=2;
                gbl3_small.setConstraints(label3_233,gbc3_small);
                panel3_23.add(label3_233);
            }
            if (flight3_delay.equals("0")) {
                JLabel label3_233 = new JLabel("DELAY", SwingConstants.CENTER);
                label3_233.setPreferredSize(new Dimension(270, 66));
                label3_233.setFont(new Font(Font.DIALOG, Font.BOLD, 21));
                label3_233.setForeground(Color.white);
                label3_233.setOpaque(true);
                label3_233.setBackground(new Color(222, 227, 108));
                gbc3_small.weightx=1;
                gbc3_small.weighty=0.2;
                gbc3_small.gridwidth=2;
                gbc3_small.gridheight=1;
                gbc3_small.gridx=0;
                gbc3_small.gridy=2;
                gbl3_small.setConstraints(label3_233,gbc3_small);
                panel3_23.add(label3_233);
            }



            panel3_21.add(button3);

            String image_name3= ef3.IMG(flight3);
            ImageIcon image3 = new ImageIcon("resource/"+image_name3);
            image3.setImage(image3.getImage().getScaledInstance(315,154,Image.SCALE_DEFAULT));
            label3_22.setIcon(image3);
            label3_22.setSize(315,154);

            panel.add(panel3);
        }


        JPanel panel4 = new JPanel();
        JButton button4 = new JButton();
        button4.setBackground(new Color(96 ,84, 146));


        /* To judge if there is a second flight.
        if there has, create the tiny-size panels and labels just as the first one
        */
        if(num>=4) {
            /*This part is just as the part of the first flight.
            To make codes clean and clear, less important and repetitive annotations won't be shown here.
            Any questions please reference to the annotations in the part of flight.
            */
            panel4.setBounds(150, 360, 900, 220);
            panel4.setBackground(new Color(20, 100, 115));



            flight flight_object4 = new flight();
            flight4_order = po.findOrder(idNum,4);
            flight4 = flight_object4.findFlight(flight4_order);

            GridBagLayout gbl4 = new GridBagLayout();
            GridBagConstraints gbc4 = new GridBagConstraints();
            panel4.setLayout(gbl4);
            gbc4.fill = GridBagConstraints.BOTH;


            BorderLayout f4_11 = new BorderLayout();
            JPanel panel4_11 = new JPanel(f4_11);
            JLabel label4_11 = new JLabel("FLIGHT 4", SwingConstants.CENTER);
            label4_11.setPreferredSize(new Dimension(135, 66));
            label4_11.setFont(new Font(Font.DIALOG, Font.PLAIN, 28));
            label4_11.setForeground(Color.white);
            label4_11.setOpaque(true);
            label4_11.setBackground(new Color(220 ,66, 75));
            panel4_11.add(label4_11, BorderLayout.CENTER);
            gbc4.weightx = 0.15;
            gbc4.weighty = 0.3;
            gbc4.gridwidth = 1;
            gbc4.gridheight = 1;
            gbc4.gridx = 0;
            gbc4.gridy = 0;
            gbl4.setConstraints(panel4_11, gbc4);
            panel4.add(panel4_11);


            BorderLayout f4_12 = new BorderLayout();
            JPanel panel4_12 = new JPanel(f4_12);
            eachFlight ef4 = new eachFlight();
            String flight4_company= ef4.COMPANY(flight4);
            String flight4_boardingTime= ef4.boardingTime(flight4);
            String flight4_gate= ef4.GATE(flight4);
            JLabel label4_12 = new JLabel(flight4+" "+flight4_company+"         "+"BOARDING TIME  "+flight4_boardingTime+"         "+"GATE "+flight4_gate,SwingConstants.CENTER);
            label4_12.setPreferredSize(new Dimension(765, 66));
            label4_12.setFont(new Font(Font.DIALOG, Font.BOLD, 20));//设置文字字体
            label4_12.setForeground(Color.white);//设置文字的颜色
            label4_12.setOpaque(true);
            label4_12.setBackground(new Color(56, 53, 53));
            panel4_12.add(label4_12, BorderLayout.CENTER);
            gbc4.weightx = 0.85;    // 指定组件的分配区域
            gbc4.weighty = 0.3;
            gbc4.gridwidth = 2;
            gbc4.gridheight = 1;
            gbc4.gridx = 1;
            gbc4.gridy = 0;
            gbl4.setConstraints(panel4_12, gbc4);
            panel4.add(panel4_12);

            BorderLayout f4_21 = new BorderLayout();
            JPanel panel4_21 = new JPanel(f4_21);
            JLabel label4_21 = new JLabel();
            label4_21.setPreferredSize(new Dimension(135, 154));
            label4_21.setOpaque(true);
            label4_21.setBackground(new Color(213, 213, 12));
            panel4_21.add(label4_21, BorderLayout.CENTER);
            gbc4.weightx = 0.15;    // 指定组件的分配区域
            gbc4.weighty = 0.7;
            gbc4.gridwidth = 1;
            gbc4.gridheight = 1;
            gbc4.gridx = 0;
            gbc4.gridy = 1;
            gbl4.setConstraints(panel4_21, gbc4);
            panel4.add(panel4_21);

            BorderLayout f4_22 = new BorderLayout();
            JPanel panel4_22 = new JPanel(f4_22);
            JLabel label4_22 = new JLabel();
            label4_22.setPreferredSize(new Dimension(315, 154));
            label4_22.setOpaque(true);
            label4_22.setBackground(new Color(6, 39, 206));
            panel4_22.add(label4_22, BorderLayout.CENTER);
            gbc4.weightx = 0.35;    // 指定组件的分配区域
            gbc4.weighty = 0.7;
            gbc4.gridwidth = 1;
            gbc4.gridheight = 1;
            gbc4.gridx = 1;
            gbc4.gridy = 1;
            gbl4.setConstraints(panel4_22, gbc4);
            panel4.add(panel4_22);


            JPanel panel4_23 = new JPanel();
            panel4_23.setPreferredSize(new Dimension(450, 154));
            gbc4.weightx = 0.5;    // 指定组件的分配区域
            gbc4.weighty = 0.7;
            gbc4.gridwidth = 1;
            gbc4.gridheight = 1;
            gbc4.gridx = 2;
            gbc4.gridy = 1;
            gbl4.setConstraints(panel4_23, gbc4);
            panel4.add(panel4_23);

            GridBagLayout gbl4_small = new GridBagLayout();
            GridBagConstraints gbc4_small = new GridBagConstraints();
            panel4_23.setLayout(gbl4_small);
            gbc4_small.fill = GridBagConstraints.BOTH;

            String flight4_departure= ef4.DEPARTURE(flight4);
            JLabel label4_2311 = new JLabel(flight4_departure, SwingConstants.CENTER);
            label4_2311.setPreferredSize(new Dimension(180, 66));
            label4_2311.setFont(new Font(Font.DIALOG, Font.BOLD, 18));
            label4_2311.setForeground(Color.black);
            label4_2311.setOpaque(true);
            label4_2311.setBackground(new Color(245, 240, 240));
            gbc4_small.weightx = 0.4;
            gbc4_small.weighty = 0.4;
            gbc4_small.gridwidth = 1;
            gbc4_small.gridheight = 1;
            gbc4_small.gridx = 0;
            gbc4_small.gridy = 0;
            gbl4_small.setConstraints(label4_2311, gbc4_small);
            panel4_23.add(label4_2311);

            String flight4_departs= ef4.DEPARTS(flight4);
            JLabel label4_2312 = new JLabel("DEPARTS:"+flight4_departs, SwingConstants.CENTER);
            label4_2312.setPreferredSize(new Dimension(270, 66));
            label4_2312.setFont(new Font(Font.DIALOG, Font.BOLD, 18));
            label4_2312.setForeground(Color.black);
            label4_2312.setOpaque(true);
            label4_2312.setBackground(new Color(233 ,233, 233));
            gbc4_small.weightx = 0.6;
            gbc4_small.weighty = 0.4;
            gbc4_small.gridwidth = 1;
            gbc4_small.gridheight = 1;
            gbc4_small.gridx = 1;
            gbc4_small.gridy = 0;
            gbl4_small.setConstraints(label4_2312, gbc4_small);
            panel4_23.add(label4_2312);

            String flight4_destination= ef4.DESTINATION(flight4);
            JLabel label4_2321 = new JLabel(flight4_destination, SwingConstants.CENTER);
            label4_2321.setPreferredSize(new Dimension(180, 66));
            label4_2321.setFont(new Font(Font.DIALOG, Font.BOLD, 18));
            label4_2321.setForeground(Color.black);
            label4_2321.setOpaque(true);
            label4_2321.setBackground(new Color(245, 240, 240));
            gbc4_small.weightx = 0.4;
            gbc4_small.weighty = 0.4;
            gbc4_small.gridwidth = 1;
            gbc4_small.gridheight = 1;
            gbc4_small.gridx = 0;
            gbc4_small.gridy = 1;
            gbl4_small.setConstraints(label4_2321, gbc4_small);
            panel4_23.add(label4_2321);

            String flight4_arrives= ef4.ARRIVES(flight4);
            JLabel label4_2322 = new JLabel("ARRIVES:"+flight4_arrives, SwingConstants.CENTER);
            label4_2322.setPreferredSize(new Dimension(270, 66));
            label4_2322.setFont(new Font(Font.DIALOG, Font.BOLD, 18));
            label4_2322.setForeground(Color.black);
            label4_2322.setOpaque(true);
            label4_2322.setBackground(new Color(213 ,213, 213));
            gbc4_small.weightx = 0.6;
            gbc4_small.weighty = 0.4;
            gbc4_small.gridwidth = 1;
            gbc4_small.gridheight = 1;
            gbc4_small.gridx = 1;
            gbc4_small.gridy = 1;
            gbl4_small.setConstraints(label4_2322, gbc4_small);
            panel4_23.add(label4_2322);

            String flight4_delay= ef4.DELAY(flight4);
            if(flight4_delay.equals("1")) {
                JLabel label4_233 = new JLabel("ON TIME", SwingConstants.CENTER);
                label4_233.setPreferredSize(new Dimension(270, 66));
                label4_233.setFont(new Font(Font.DIALOG, Font.BOLD, 21));
                label4_233.setForeground(Color.white);//设置文字的颜色
                label4_233.setOpaque(true);
                label4_233.setBackground(new Color(104, 210, 128));
                gbc4_small.weightx = 1;
                gbc4_small.weighty = 0.2;
                gbc4_small.gridwidth = 2;
                gbc4_small.gridheight = 1;
                gbc4_small.gridx = 0;
                gbc4_small.gridy = 2;
                gbl4_small.setConstraints(label4_233, gbc4_small);
                panel4_23.add(label4_233);
            }
            if(flight4_delay.equals("0")){
                JLabel label4_233 = new JLabel("DELAYED", SwingConstants.CENTER);
                label4_233.setPreferredSize(new Dimension(270, 66));
                label4_233.setFont(new Font(Font.DIALOG, Font.BOLD, 21));
                label4_233.setForeground(Color.white);
                label4_233.setOpaque(true);
                label4_233.setBackground(new Color(222, 227, 108));
                gbc4_small.weightx = 1;
                gbc4_small.weighty = 0.2;
                gbc4_small.gridwidth = 2;
                gbc4_small.gridheight = 1;
                gbc4_small.gridx = 0;
                gbc4_small.gridy = 2;
                gbl4_small.setConstraints(label4_233, gbc4_small);
                panel4_23.add(label4_233);
            }


            panel4_21.add(button4);

            String image_name4= ef4.IMG(flight4);
            ImageIcon image4 = new ImageIcon("resource/"+image_name4);
            image4.setImage(image4.getImage().getScaledInstance(315, 154, Image.SCALE_DEFAULT));
            label4_22.setIcon(image4);
            label4_22.setSize(315, 154);

            panel.add(panel4);
        }

        panel.add(panel0); // Add the title panel
        panel.add(panel1); // Add the first flight panel

        // For the number of flight is less than 3, make panel 3 and 4 invisible
        panel3.setVisible(false);
        panel4.setVisible(false);

        flight_num=flight1;    // Set flight number as flight 1's number
        flight_order=flight1_order; // Set flight order as flight 1's order

        //Set buttons' background color
        button1.setBackground(new Color(104, 210, 128));
        button2.setBackground(new Color(96 ,84, 146));
        button3.setBackground(new Color(96 ,84, 146));
        button4.setBackground(new Color(96 ,84, 146));


        // Set "Next" button
        jButtonNext.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {


                // When page =1, we are at page two, page add one
                if(page==1){
                    jButtonLast.setVisible(true); // "Last" button on the left is Visible
                    jButtonNext.setVisible(false); //  "Next" button on the right is Invisible
                    panel1.setVisible(false);// Panel for flight 1 is Invisible
                    panel2.setVisible(false);// Panel for flight 2 is Invisible
                    panel3.setVisible(true);// Panel for flight 3 is Visible
                    panel4.setVisible(true);// Panel for flight 4 is Visible
                    page++;
                }

            }
        });

        // Set "Last" button
        jButtonLast.addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(page==2){ // when page = 2, we are at page one, page will be 1 again.
                    jButtonNext.setVisible(true);//  "Next" button on the right is Visible
                    jButtonLast.setVisible(false);//  "Last" button on the left is Invisible
                    panel3.setVisible(false);// Panel for flight 3 is Invisible
                    panel4.setVisible(false);// Panel for flight 4 is Invisible
                    panel1.setVisible(true);// Panel for flight 1 is Visible
                    panel2.setVisible(true);// Panel for flight 2 is Visible
                    page--;

                }

            }
        });

        // Set button 1
        button1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                //Set buttons' background color
                button1.setBackground(new Color(104, 210, 128));
                button2.setBackground(new Color(96 ,84, 146));
                button3.setBackground(new Color(96 ,84, 146));
                button4.setBackground(new Color(96 ,84, 146));

                flight_num=flight1;// Set flight number as flight 1's number
                flight_order=flight1_order;// Set flight order as flight 1's order
            }


        });

        // Set button 2
        button2.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                //Set buttons' background color
                button2.setBackground(new Color(104, 210, 128));
                button1.setBackground(new Color(96 ,84, 146));
                button3.setBackground(new Color(96 ,84, 146));
                button4.setBackground(new Color(96 ,84, 146));
                flight_num=flight2;// Set flight number as flight 2's number
                flight_order=flight2_order;// Set flight order as flight 2's order
            }


        });

        // Set button 3
        button3.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                //Set buttons' background color
                button3.setBackground(new Color(104, 210, 128));
                button1.setBackground(new Color(96 ,84, 146));
                button2.setBackground(new Color(96 ,84, 146));
                button4.setBackground(new Color(96 ,84, 146));
                flight_num=flight3;// Set flight number as flight 3's number
                flight_order=flight3_order;// Set flight order as flight 3's order
            }


        });

        // Set button 4
        button4.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                //Set buttons' background color
                button4.setBackground(new Color(104, 210, 128));
                button1.setBackground(new Color(96 ,84, 146));
                button2.setBackground(new Color(96 ,84, 146));
                button3.setBackground(new Color(96 ,84, 146));
                flight_num=flight4; // Set flight number as flight 4's number
                flight_order=flight4_order;// Set flight order as flight 4's order
            }


        });

    }
}


