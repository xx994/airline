package frame;

import frame.UserPanel.*;

import model.*;
import utils.BackGroundImagePanle;
import utils.GBC;
import utils.MyLayout;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.util.ArrayList;
import java.util.Objects;
/**
 * Title             : Main_Frame.java
 * Description : This class is a template for building the user interface.
 */
public class Main_Frame extends JFrame {
    public static int pageNum=0;

    public static String orderNum="1326854870743";//订单号
    public static String IDNum="360124200102040017";//ID号
    public static String flightNum="BA0570";//航班号
    public static String foodType="0";//食物种类
    public static String temp="";//id值
    public static boolean flag = false;
    public static String req="NOTE";//需求


    public Check_in_Panel check_in;
    public Check_in_Panel flights;
    public Check_in_Panel foods;
    public Check_in_Panel seats;
    public Check_in_Panel print;
    public Check_in_Panel bookingNum;
    public Check_in_Panel idNum;
    public Check_in_Panel scanNum;
    public Check_in_Panel confirm;

    public JPanel panelMiddle;
    public JButton jButtonRight;
    public JButton jButtonLeft;




    public void buildMainFrame(CardLayout cardLayout){
        JFrame frame = new JFrame("Heathrow");
        try{
            UIManager.setLookAndFeel(UIManager.getCrossPlatformLookAndFeelClassName());
        }catch(Exception e){
            e.printStackTrace();
        }
        frame.setBounds(300,80,1200,900);
        frame.setResizable(false);

        //设置布局
        frame.setLayout(new BorderLayout());
        //设置上层panel
        JPanel panelTop = new JPanel();
        panelTop.setBackground(new Color(96,62,151));
        //设置中层panel
        this.panelMiddle = new JPanel(cardLayout);
        //设置下层panel
        JPanel panelBottom = new JPanel();
        panelBottom.setBackground(new Color(96,62,151));

        //设置按钮
        Dimension preferredSize = new Dimension(170,100);
        this.jButtonLeft = new JButton("HELP");
        jButtonLeft.setPreferredSize(preferredSize);

        this.jButtonRight = new JButton("CONTINUE");
        jButtonRight.setPreferredSize(preferredSize);


        //设置卡片布局
        JPanel main = new JPanel();
        main.setBackground(new Color(72,46,115));

        Check_in_Panel check_in = new Check_in_Panel(this.panelMiddle,this.jButtonRight);
        Flights_Panel flights = new Flights_Panel();
        Foods_Panel foods = new Foods_Panel();
        //Seats_Panel seats = new Seats_Panel();
        Print_Panel print = new Print_Panel();
        Check_in_bookingNum_Panel bookingNum = new Check_in_bookingNum_Panel();
        Check_in_idNum_Panel idNum = new Check_in_idNum_Panel();
        Check_in_scanNum_Panel scanNum = new Check_in_scanNum_Panel();
        //Confirm_Panel confirm = new Confirm_Panel();
        Pay_Panel payPanel = new Pay_Panel();
        Video_Panel videoPanel = new Video_Panel();

        main.setLayout(new MyLayout(main));
        //左侧的颜色
        JPanel Panel1 = new JPanel();
        Panel1.setBackground(new Color(72,46,115));
        main.add(Panel1,new GBC(0,0,1,1).
                setFill(GBC.BOTH).setIpad(60, 680).setWeight(0, 10));

        //中间的图片
        BackGroundImagePanle PanelDangerous= new BackGroundImagePanle("Dangerous.png");
        main.add(PanelDangerous,new GBC(1,0,1,1).
                setFill(GBC.BOTH).setIpad(1050, 680).setWeight(10, 0));

        //右侧的颜色
        JPanel Panel3 = new JPanel();
        Panel3.setBackground(new Color(72,46,115));
        main.add(Panel3,new GBC(2,0,1,1).
                setFill(GBC.BOTH).setIpad(50, 680).setWeight(0, 10));


        //映射卡片布局
        panelMiddle.add(main,"0");
        panelMiddle.add(check_in,"1");
        panelMiddle.add(bookingNum,"2");
        panelMiddle.add(idNum,"3");
        panelMiddle.add(scanNum,"4");
        panelMiddle.add(flights,"5");
        //panelMiddle.add(seats,"6");
        panelMiddle.add(foods,"7");
        //panelMiddle.add(confirm,"8");
        panelMiddle.add(payPanel,"9");
        panelMiddle.add(print,"10");
        panelMiddle.add(videoPanel,"11");


        //添加三层panel至布局
        frame.add(panelTop,BorderLayout.NORTH);
        frame.add(panelMiddle,BorderLayout.CENTER);
        frame.add(panelBottom,BorderLayout.SOUTH);

        //设置布局位置
        panelTop.setPreferredSize(new Dimension(1200,110));
        panelMiddle.setPreferredSize(new Dimension(1200,680));
        panelBottom.setPreferredSize(new Dimension(1200,110));

        //设置关闭事件
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

        //设置顶层菜单布局
        panelTop.setLayout(new BorderLayout());
        //设置左上角logo
        BackGroundImagePanle PanelLogo= new BackGroundImagePanle("logo_heathrow.png");

        JPanel panelMenu = new JPanel();
        panelMenu.setBackground(new Color(96,62,151));

        //添加到顶层菜单布局
        panelTop.add(PanelLogo,BorderLayout.WEST);
        panelTop.add(panelMenu,BorderLayout.EAST);
        //添加到左上角
        PanelLogo.setPreferredSize(new Dimension(300,110));
        panelMenu.setPreferredSize(new Dimension(900,110));

        //创建右上角按钮
        JLabel label1 = new JLabel(" CHECK IN",JLabel.CENTER);
        JLabel label2 = new JLabel("FLIGHTS",JLabel.CENTER);
        JLabel label3 = new JLabel("SEATS",JLabel.CENTER);
        JLabel label4 = new JLabel("FOODS",JLabel.CENTER);
        JLabel label5 = new JLabel("PRINT",JLabel.CENTER);

        //设置标签字体
        label1.setFont(new Font (Font.DIALOG, Font.BOLD, 28));
        label1.setForeground(Color.white);//设置文字的颜色
        label2.setFont(new Font (Font.DIALOG, Font.BOLD, 28));
        label2.setForeground(Color.white);//设置文字的颜色
        label3.setFont(new Font (Font.DIALOG, Font.BOLD, 28));
        label3.setForeground(Color.white);//设置文字的颜色
        label4.setFont(new Font (Font.DIALOG, Font.BOLD, 28));
        label4.setForeground(Color.white);//设置文字的颜色
        label5.setFont(new Font (Font.DIALOG, Font.BOLD, 28));
        label5.setForeground(Color.white);//设置文字的颜色

        panelMenu.setLayout(new GridLayout(1, 5));
        panelMenu.add(label1);
        panelMenu.add(label2);
        panelMenu.add(label3);
        panelMenu.add(label4);
        panelMenu.add(label5);

        //设置box布局
        Box b1=Box.createHorizontalBox();    //创建横向Box容器
        panelBottom.add(b1);
        b1.add(Box.createVerticalStrut(10));
        b1.add(jButtonLeft);
        b1.add(Box.createHorizontalStrut(570));
        b1.add(jButtonRight);    //添加按钮2



        //设置按钮字体
        jButtonLeft.setFont(new Font (Font.DIALOG, Font.PLAIN, 25));
        jButtonLeft.setForeground(Color.white);
        jButtonLeft.setBackground(new Color(218,65,71));

        jButtonRight.setFont(new Font (Font.DIALOG, Font.PLAIN, 25));
        jButtonRight.setForeground(Color.white);
        jButtonRight.setBackground(new Color(218,65,71));


        jButtonLeft.setVisible(true);
        jButtonRight.setVisible(true);

        //设置按钮事件
        jButtonRight.addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(pageNum<12){
                pageNum++;
                }
                if(pageNum==0){
                    cardLayout.show(panelMiddle,"0");
                    label1.setBackground(new Color(96,62,151));

                }else if(pageNum==1){
                    String temperature;
                    double d=10000;
                    temperature =JOptionPane.showInputDialog(null,"Please enter your temperature, e.g. 36.8");
                    if (temperature!=null&&temperature.length()!=0){
                        for (int i = 0;i<temperature.length();i++){
                            if (!Character.isDigit(temperature.charAt(i))){
                                if(temperature.charAt(i)!='.'){
                                    temperature="false";
                                }
                            }
                        }
                        if(!temperature.equals("false")){
                            d=Double.parseDouble(temperature);
                        }
                    }
                    //if (temperature!=null){
                    //    d=Double.parseDouble(temperature);
                    //}
                    if (d<=37.3&&d>=35.5){
                        label1.setOpaque(true);
                        label2.setBackground(new Color(96,62,151));
                        label1.setBackground(new Color(72,46,115));
                        cardLayout.show(panelMiddle,"1");
                        panelBottom.setBackground(new Color(72,46,115));
                        jButtonLeft.setText("RETURN");
                        jButtonRight.setVisible(false);
                    }else if(d==10000){
                        JOptionPane.showMessageDialog(null, "You have" +
                                " entered incorrect information.", "warning", JOptionPane.INFORMATION_MESSAGE);
                        pageNum--;
                    } else {
                        JOptionPane.showMessageDialog(null, "Your temperature is abnormal. " +
                                "Please contact the airport staff immediately.", "warning", JOptionPane.INFORMATION_MESSAGE);
                        pageNum--;
                    }


                }else if(pageNum==2){
                    label1.setOpaque(true);
                    label2.setBackground(new Color(96,62,151));
                    label1.setBackground(new Color(72,46,115));


                }else if(pageNum==3){
                    label1.setBackground(new Color(96,62,151));
                    label2.setBackground(new Color(72,46,115));
                    //输入了正确的id，直接进入下一页
                    if(order.isOrder(Check_in_bookingNum_Panel.IDnum)==1){

                        System.out.println(Check_in_bookingNum_Panel.IDnum);

                        temp=passenger_order.find_id_num(Check_in_bookingNum_Panel.IDnum);
                        Flights_Panel flights = new Flights_Panel();
                        pageNum=4;
                        panelMiddle.add(flights,"5");
                        cardLayout.show(panelMiddle,"5");
                        pageNum++;
                        temp="";

                        System.out.println("Welcome Boarding!");

                    }
                    //输入了错误的id，重新输入
                    else {

                        System.out.println(Check_in_bookingNum_Panel.IDnum);
                        System.out.println("Wrong booking number!");

                        JOptionPane.showMessageDialog(null, "The information you ente" +
                                "red is incorrect. Please confirm and re-enter.", "warning", JOptionPane.INFORMATION_MESSAGE);
                        Flights_Panel.idNum="";
                        Check_in_bookingNum_Panel bookingNum = new Check_in_bookingNum_Panel();
                        panelMiddle.add(bookingNum,"2");
                        cardLayout.show(panelMiddle,"2");
                        pageNum--;
                    }

                }else if(pageNum==4){
                    label1.setBackground(new Color(96,62,151));
                    label2.setBackground(new Color(72,46,115));
                    //情况一：第一次输入了正确的id，则flag为真
                    
                        // System.out.println("Please input ID!");

                    if(passenger.isPassenger(Check_in_idNum_Panel.IDnum)==1&&flag == false){
                        
                        temp=Check_in_idNum_Panel.IDnum;

                            System.out.println(temp);

                        JOptionPane.showMessageDialog(null, "Now please enter your name", "tips", JOptionPane.INFORMATION_MESSAGE);
                        flag = true;
                        Check_in_idNum_Panel idNum = new Check_in_idNum_Panel();
                        panelMiddle.add(idNum,"3");
                        cardLayout.show(panelMiddle,"3");
                        pageNum--;

                            System.out.println("Please input Surname!");

                        //情况二：在flag为真，即正确输入了id时，姓名也正确，成功登陆
                    }else if(flag == true&&passenger_order.isSurname(temp,Check_in_idNum_Panel.IDnum)==1){

                        System.out.println(Check_in_idNum_Panel.IDnum);

                        Flights_Panel flights1 = new Flights_Panel();
                        panelMiddle.add(flights1,"5");
                        cardLayout.show(panelMiddle,"5");
                        jButtonRight.setText("CONTINUE");
                        jButtonRight.setBackground(new Color(218,65,71));
                        pageNum++;
                        temp="";
                        flag = false;
                        Check_in_idNum_Panel idNum = new Check_in_idNum_Panel();
                        panelMiddle.add(idNum,"3");

                        System.out.println("Welcome Boarding!");

                        //情况三：在flag为真，即正确输入了id时，仍然在输入id，则报错
                    }else if(passenger.isPassenger(Check_in_idNum_Panel.IDnum)==1&&flag == true){

                        System.out.println(Check_in_idNum_Panel.IDnum);

                        JOptionPane.showMessageDialog(null, "You just need to enter your name", "tips", JOptionPane.INFORMATION_MESSAGE);
                        Check_in_idNum_Panel idNum = new Check_in_idNum_Panel();
                        panelMiddle.add(idNum,"3");
                        cardLayout.show(panelMiddle,"3");
                        pageNum--;

                        System.out.println("Wrong Surname!");

                        //情况四：在flag为真，输错姓名，报错
                    }else if(passenger_order.isSurname(temp,Check_in_idNum_Panel.IDnum)==0&&flag == true){

                        System.out.println(Check_in_idNum_Panel.IDnum);

                        JOptionPane.showMessageDialog(null, "Your name is entered incorrectly." +
                                " Please re-enter it.", "warning", JOptionPane.INFORMATION_MESSAGE);
                        Check_in_idNum_Panel idNum = new Check_in_idNum_Panel();
                        panelMiddle.add(idNum,"3");
                        cardLayout.show(panelMiddle,"3");
                        pageNum--;

                        System.out.println("Wrong SurName!");

                        //情况五：第一次就输错了id
                    } else if(passenger.isPassenger(Check_in_idNum_Panel.IDnum)==0&&flag == false){

                        System.out.println(Check_in_idNum_Panel.IDnum);

                        JOptionPane.showMessageDialog(null, "Your ID is entered incorrectly." +
                                " Please re-enter it.", "warning", JOptionPane.INFORMATION_MESSAGE);
                        Check_in_idNum_Panel idNum = new Check_in_idNum_Panel();
                        Flights_Panel.idNum="";
                        panelMiddle.add(idNum,"3");
                        cardLayout.show(panelMiddle,"3");
                        pageNum--;

                        System.out.println("Wrong ID!");

                    }


                }else if(pageNum==5){
                    label2.setOpaque(true);
                    label1.setBackground(new Color(96,62,151));
                    label2.setBackground(new Color(72,46,115));
                    temp="360124200102040016";
                    Flights_Panel flights1 = new Flights_Panel();
                    panelMiddle.add(flights1,"5");
                    cardLayout.show(panelMiddle,"5");
                    temp="";
                    jButtonRight.setText("CONTINUE");
                    jButtonRight.setBackground(new Color(218,65,71));

                    Flights_Panel flights = new Flights_Panel();
                    panelMiddle.add(flights,"5");





                }else if(pageNum==6){

                    if (!Objects.equals(Flights_Panel.flight_num, "")){
                        int n = JOptionPane.showConfirmDialog(null, "Are" +
                                " you sure you chose: "+ Flights_Panel.flight_num + "?", "CHECK",JOptionPane.YES_NO_OPTION);//返回的是按钮的index  i=0或者1
                        if (n==0){
                            if(!flight.isBoarding(Flights_Panel.flight_num,Flights_Panel.idNum)){
                                label3.setOpaque(true);
                                label2.setBackground(new Color(96,62,151));
                                label3.setBackground(new Color(72,46,115));
                                Seats_Panel seats = new Seats_Panel();
                                panelMiddle.add(seats,"6");
                                cardLayout.show(panelMiddle,"6");
                            }else {
                                JOptionPane.showMessageDialog(null, "You have already boarded the plane." +
                                        " You can't board again.", "warning", JOptionPane.INFORMATION_MESSAGE);
                                pageNum--;
                            }
                        }else {
                            pageNum--;
                        }
                    }else {pageNum--;
                        JOptionPane.showMessageDialog(null, "Please choose" +
                                " your flight first.", "Tips", JOptionPane.INFORMATION_MESSAGE);
                    }


                }else if(pageNum==7){

                    if (!Objects.equals(Seats_Panel.letter, "0")){
                        int n = JOptionPane.showConfirmDialog(null, "Are" +
                                " you sure you chose: "+ Seats_Panel.letter +" "
                                +Seats_Panel.number+"?", "CHECK",JOptionPane.YES_NO_OPTION);//返回的是按钮的index  i=0或者1
                        if (n==0){
                            label4.setOpaque(true);
                            label3.setBackground(new Color(96,62,151));
                            label4.setBackground(new Color(72,46,115));
                            Foods_Panel foods = new Foods_Panel();
                            panelMiddle.add(foods,"7");
                            cardLayout.show(panelMiddle,"7");
                        }else {
                            pageNum--;
                        }
                    }else {pageNum--;
                        JOptionPane.showMessageDialog(null, "Please choose" +
                                " your seat first.", "Tips", JOptionPane.INFORMATION_MESSAGE);
                    }


                }else if(pageNum==8) {
                    if (!Objects.equals(Foods_Panel.food, "")) {
                        label5.setOpaque(true);
                        label4.setBackground(new Color(96, 62, 151));
                        label5.setBackground(new Color(72, 46, 115));
                        Confirm_Panel confirm = new Confirm_Panel();
                        panelMiddle.add(confirm, "8");
                        cardLayout.show(panelMiddle, "8");
                        jButtonRight.setText("CONFIRM");
                        jButtonRight.setBackground(new Color(132, 177, 132));
                    }else {pageNum--;
                        JOptionPane.showMessageDialog(null, "Please choose" +
                                " your food first.", "Tips", JOptionPane.INFORMATION_MESSAGE);
                    }
                }else if(pageNum==9){//进入支付

                    int n = JOptionPane.showConfirmDialog(null, "Are you sure all" +
                            " the information is correct?", "CHECK",JOptionPane.YES_NO_OPTION);//返回的是按钮的index  i=0或者1
                    if (n==0){
                        Pay_Panel payPanel = new Pay_Panel();
                        panelMiddle.add(payPanel,"9");
                        label5.setOpaque(true);
                        label1.setBackground(new Color(96,62,151));
                        label5.setBackground(new Color(72,46,115));
                        cardLayout.show(panelMiddle,"9");
                        jButtonRight.setText("CONTINUE");
                        jButtonRight.setBackground(new Color(218,65,71));
                    }else {
                        pageNum--;
                    }

                }else if(pageNum==10){//进入打印
                    Print_Panel print = new Print_Panel();
                    panelMiddle.add(print,"10");
                    //保存座位
                    eachFlight.seat_choose(flightNum,Seats_Panel.letter,Seats_Panel.number);
                    flight.add_seat(Flights_Panel.flight_order,Seats_Panel.letter,Seats_Panel.number);
                    //保存食物
                    flight.add_food(Flights_Panel.flight_order,Foods_Panel.food);
                    //保存已登机
                    flight.boarding(Flights_Panel.flight_order);
                    //保存注释
                    flight.add_note(Flights_Panel.flight_order,req);
                    cardLayout.show(panelMiddle,"10");

                }else if(pageNum==11){//进入展示动画
                    cardLayout.show(panelMiddle,"11");
                    jButtonRight.setText("FINISH");
                }else if(pageNum==12){//进入展示动画
                    System.exit(0);
                }
            }
        });

        jButtonLeft.addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (pageNum>=0){
                pageNum--;
                }
                if (pageNum==-1){
                            JOptionPane.showMessageDialog(null, "Please wait a moment. " +
                                    "We have called for manual help for you.", "Manual help", JOptionPane.INFORMATION_MESSAGE);
                    pageNum++;

                }else if(pageNum==0){
                    cardLayout.show(panelMiddle,"0");
                    label1.setBackground(new Color(96,62,151));
                    jButtonLeft.setText("HELP");
                    panelBottom.setBackground(new Color(96,62,151));
                    jButtonRight.setVisible(true);
                    jButtonRight.setBackground(new Color(218,65,71));

                }else if(pageNum==1){
                    temp="";
                    flag = false;
                    Check_in_bookingNum_Panel bookingNum = new Check_in_bookingNum_Panel();
                    panelMiddle.add(bookingNum,"2");
                    label1.setOpaque(true);
                    label2.setBackground(new Color(96,62,151));
                    label1.setBackground(new Color(72,46,115));
                    cardLayout.show(panelMiddle,"1");
                    jButtonRight.setVisible(false);
                    jButtonRight.setBackground(new Color(218,65,71));


                }else if(pageNum==2){
                    temp="";
                    flag = false;
                    Check_in_idNum_Panel idNum = new Check_in_idNum_Panel();
                    panelMiddle.add(idNum,"3");
                    label1.setOpaque(true);
                    label2.setBackground(new Color(96,62,151));
                    label1.setBackground(new Color(72,46,115));
                    pageNum--;
                    jButtonRight.setVisible(false);
                    cardLayout.show(panelMiddle,"1");

                }else if(pageNum==3){
                    temp="";
                    flag = false;
                    label1.setOpaque(true);
                    label2.setBackground(new Color(96,62,151));
                    label1.setBackground(new Color(72,46,115));
                    pageNum=pageNum-2;
                    jButtonRight.setVisible(false);
                    cardLayout.show(panelMiddle,"1");


                }else if(pageNum==4){
                    temp="";
                    flag = false;
                    Check_in_bookingNum_Panel bookingNum = new Check_in_bookingNum_Panel();
                    panelMiddle.add(bookingNum,"2");
                    Check_in_idNum_Panel idNum = new Check_in_idNum_Panel();
                    panelMiddle.add(idNum,"3");
                    label1.setOpaque(true);
                    label2.setBackground(new Color(96,62,151));
                    label1.setBackground(new Color(72,46,115));
                    cardLayout.show(panelMiddle,"1");
                    pageNum=pageNum-3;
                    jButtonRight.setVisible(false);



                }else if(pageNum==5){
                    label2.setOpaque(true);
                    label3.setBackground(new Color(96,62,151));
                    label2.setBackground(new Color(72,46,115));
                    cardLayout.show(panelMiddle,"5");

                }else if(pageNum==6){
                    label3.setOpaque(true);
                    label4.setBackground(new Color(96,62,151));
                    label3.setBackground(new Color(72,46,115));
                    cardLayout.show(panelMiddle,"6");

                }else if(pageNum==7){
                    label4.setOpaque(true);
                    label5.setBackground(new Color(96,62,151));
                    label4.setBackground(new Color(72,46,115));
                    cardLayout.show(panelMiddle,"7");
                    jButtonRight.setText("CONTINUE");
                    jButtonRight.setBackground(new Color(218,65,71));

                }else if(pageNum==8){
                    label4.setOpaque(true);
                    cardLayout.show(panelMiddle,"8");
                    jButtonRight.setText("CONFIRM");
                    jButtonRight.setBackground(new Color(132, 177, 132));


                }
                else if(pageNum==9){
                    cardLayout.show(panelMiddle,"9");

                }
                else if(pageNum==10){
                    cardLayout.show(panelMiddle,"10");
                    jButtonRight.setText("CONTINUE");

                }
                else if(pageNum==11){
                    cardLayout.show(panelMiddle,"11");
                    jButtonRight.setText("CONTINUE");
                }
            }
        });


        //设置窗口可见
        frame.setVisible(true);
    }

}
