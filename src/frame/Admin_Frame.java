package frame;

import frame.AdminPanel.Admin_Flights_Panel;
import frame.AdminPanel.Admin_Information;
import frame.UserPanel.Check_in_bookingNum_Panel;
import model.*;
import utils.BackGroundImagePanle;
import utils.MyLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.WindowEvent;

/**
 * Title             : Admin_Frame.java
 * Description : This class is a template for building the administrator interface.
 */
public class Admin_Frame extends JFrame{
    public JPanel panelMiddle;
    public static int pageNumber=0;
    public static String IDnum="";
    public static boolean flag;
    String temp=null;
    public void build_Admin_Frame(){
        JFrame frameAdmin = new JFrame("Heathrow Admin");
        try{
            UIManager.setLookAndFeel(UIManager.getCrossPlatformLookAndFeelClassName());
        }catch(Exception e){
            e.printStackTrace();
        }
        frameAdmin.setBounds(320,90,1200,900);
        frameAdmin.setResizable(false);

        //设置布局
        frameAdmin.setLayout(new BorderLayout());
        //设置上层panel
        JPanel panelTop = new JPanel();
        panelTop.setBackground(new Color(96,62,151));
        //设置中层panel
        CardLayout cardLayout = new CardLayout();
        this.panelMiddle = new JPanel(cardLayout);
        //设置下层panel
        JPanel panelBottom = new JPanel();
        panelBottom.setBackground(new Color(96,62,151));

        //设置卡片布局
        JPanel main = new JPanel();
        main.setLayout(null);
        main.setBackground(new Color(72,46,115));
        Admin_Flights_Panel flights = new Admin_Flights_Panel();
        temp = Admin_Flights_Panel.flight_num;
        Admin_Information Information = new Admin_Information(temp);
        panelMiddle.add(main,"0");
        panelMiddle.add(flights,"1");
        panelMiddle.add(Information,"2");

        //添加三层panel至布局
        frameAdmin.add(panelTop,BorderLayout.NORTH);
        frameAdmin.add(panelMiddle,BorderLayout.CENTER);
        frameAdmin.add(panelBottom,BorderLayout.SOUTH);

        //设置布局位置
        panelTop.setPreferredSize(new Dimension(1200,110));
        panelMiddle.setPreferredSize(new Dimension(1200,680));
        panelBottom.setPreferredSize(new Dimension(1200,110));

        //设置顶层菜单布局
        panelTop.setLayout(null);
        //设置左上角logo
        BackGroundImagePanle PanelLogo= new BackGroundImagePanle("logo_heathrow.png");
        PanelLogo.setBounds(0,0,300,110);
        panelTop.add(PanelLogo);

        //设置右上角文字
        JLabel jLabelAdmin = new JLabel("Administrator system");
        jLabelAdmin.setFont(new Font (Font.DIALOG, Font.BOLD, 38));
        jLabelAdmin.setForeground(Color.white);//设置文字的颜色
        jLabelAdmin.setBounds(740,0,400,110);
        panelTop.add(jLabelAdmin);


        JTextField inputArea = new JTextField("", 30);
        //外观设计
        inputArea.setBounds(284,105,632,50);
        inputArea.setFont(new Font (Font.DIALOG,Font.BOLD, 40));
        inputArea.setHorizontalAlignment(JTextField.CENTER);
        inputArea.setForeground(Color.WHITE);
        inputArea.setBackground(new Color(72,46,115));
        inputArea.setEditable(false);
        inputArea.setBorder(BorderFactory.createLineBorder(Color.WHITE, 2));



        JLabel BookingNumHint = new JLabel("");
        if (!flag){
            BookingNumHint.setText("PLEASE ENTER THE ADMINISTRATOR ACCOUNT");
        }else {
            BookingNumHint.setText("PLEASE ENTER YOUR PASSWORD");
        }
        BookingNumHint.setBounds(184,45,816,50);
        //外观设计
        BookingNumHint.setFont(new Font(Font.DIALOG,Font.BOLD,29));//设置文字字体
        BookingNumHint.setForeground(Color.white);//设置文字的颜色
        BookingNumHint.setOpaque(true);//设置透明效果
        BookingNumHint.setBackground(new Color(72,46,115));//设置背景颜色
        BookingNumHint.setPreferredSize(new Dimension(800,80));//设置长宽
        BookingNumHint.setHorizontalAlignment(JLabel.CENTER);//设置对齐方式

        //创建0-9和delete按钮
        JButton button1 = new JButton("1");
        JButton button2 = new JButton("2");
        JButton button3 = new JButton("3");
        JButton button4 = new JButton("4");
        JButton button5 = new JButton("5");
        JButton button6 = new JButton("6");
        JButton button7 = new JButton("7");
        JButton button8 = new JButton("8");
        JButton button9 = new JButton("9");
        JButton button0 = new JButton("0");
        JButton delete = new JButton("Delete");

        //按钮外观设计
        button1.setBounds(485,215,75,75);
        button1.setBackground(new Color(96,62,151));//设置背景颜色
        button1.setForeground(Color.white);//设置文字颜色
        button1.setFont(new Font (Font.DIALOG, Font.PLAIN, 45));//设置字体大小
        button1.setBorder(BorderFactory.createLineBorder(Color.WHITE, 2));//设置边框
        //以下同理

        button2.setBounds(575,215,75,75);
        button2.setBackground(new Color(96,62,151));
        button2.setForeground(Color.white);
        button2.setFont(new Font (Font.DIALOG, Font.PLAIN, 45));
        button2.setBorder(BorderFactory.createLineBorder(Color.WHITE, 2));

        button3.setBounds(665,215,75,75);
        button3.setBackground(new Color(96,62,151));
        button3.setForeground(Color.white);
        button3.setFont(new Font (Font.DIALOG, Font.PLAIN, 45));
        button3.setBorder(BorderFactory.createLineBorder(Color.WHITE, 2));

        button4.setBounds(485,305,75,75);
        button4.setBackground(new Color(96,62,151));
        button4.setForeground(Color.white);
        button4.setFont(new Font (Font.DIALOG, Font.PLAIN, 45));
        button4.setBorder(BorderFactory.createLineBorder(Color.WHITE, 2));

        button6.setBounds(665,305,75,75);
        button6.setBackground(new Color(96,62,151));
        button6.setForeground(Color.white);
        button6.setFont(new Font (Font.DIALOG, Font.PLAIN, 45));
        button6.setBorder(BorderFactory.createLineBorder(Color.WHITE, 2));

        button5.setBounds(575,305,75,75);
        button5.setBackground(new Color(96,62,151));
        button5.setForeground(Color.white);
        button5.setFont(new Font (Font.DIALOG, Font.PLAIN, 45));
        button5.setBorder(BorderFactory.createLineBorder(Color.WHITE, 2));

        button7.setBounds(487,395,75,75);
        button7.setBackground(new Color(96,62,151));
        button7.setForeground(Color.white);
        button7.setFont(new Font (Font.DIALOG, Font.PLAIN, 45));
        button7.setBorder(BorderFactory.createLineBorder(Color.WHITE, 2));

        button8.setBounds(575,395,75,75);
        button8.setBackground(new Color(96,62,151));
        button8.setForeground(Color.white);
        button8.setFont(new Font (Font.DIALOG, Font.PLAIN, 45));
        button8.setBorder(BorderFactory.createLineBorder(Color.WHITE, 2));

        button9.setBounds(665,395,75,75);
        button9.setBackground(new Color(96,62,151));
        button9.setForeground(Color.white);
        button9.setFont(new Font (Font.DIALOG, Font.PLAIN, 45));
        button9.setBorder(BorderFactory.createLineBorder(Color.WHITE, 2));

        button0.setBounds(575,485,75,75);
        button0.setBackground(new Color(96,62,151));
        button0.setForeground(Color.white);
        button0.setFont(new Font (Font.DIALOG, Font.PLAIN, 45));
        button0.setBorder(BorderFactory.createLineBorder(Color.WHITE, 2));

        delete.setBounds(790,215,126,75);
        delete.setBackground(new Color(96,62,151));
        delete.setForeground(Color.white);
        delete.setFont(new Font (Font.DIALOG, Font.PLAIN, 30));
        delete.setBorder(BorderFactory.createLineBorder(Color.WHITE, 2));

        //将按钮添加到panel上
        main.add(button1);
        main.add(button3);
        main.add(button2);
        main.add(button4);
        main.add(button5);
        main.add(button6);
        main.add(button7);
        main.add(button8);
        main.add(button9);
        main.add(button0);
        main.add(delete);
        main.add(inputArea);
        main.add(BookingNumHint);

        //为按钮添加监听事件
        button0.addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {

            }
        });

        button0.addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                IDnum=IDnum+"0";//实时更新IDnum
                inputArea.setText(IDnum);//更新输入框里的内容，以下同理

            }
        });

        button1.addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                IDnum=IDnum+"1";
                inputArea.setText(IDnum);
            }
        });

        button2.addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                IDnum=IDnum+"2";
                inputArea.setText(IDnum);
            }
        });

        button3.addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                IDnum=IDnum+"3";
                inputArea.setText(IDnum);
            }
        });

        button4.addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                IDnum=IDnum+"4";
                inputArea.setText(IDnum);
            }
        });

        button5.addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                IDnum=IDnum+"5";
                inputArea.setText(IDnum);
            }
        });

        button6.addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                IDnum=IDnum+"6";
                inputArea.setText(IDnum);
            }
        });

        button7.addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                IDnum=IDnum+"7";
                inputArea.setText(IDnum);
            }
        });

        button8.addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                IDnum=IDnum+"8";
                inputArea.setText(IDnum);
            }
        });

        button9.addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                IDnum=IDnum+"9";
                inputArea.setText(IDnum);
            }
        });

        delete.addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(IDnum.length() > 0){
                    //若文本框内字符串长度大于一，则执行删除命令
                    IDnum=IDnum.substring(0,IDnum.length()-1);
                    inputArea.setText(IDnum);
                }
                else;//若没有内容，则不执行删除
            }
        });


        //设置按钮
        Dimension preferredSize = new Dimension(170,100);
        JButton jButtonLeft = new JButton("RETURN");
        jButtonLeft.setPreferredSize(preferredSize);

        JButton jButtonRight = new JButton("CONTINUE");
        jButtonRight.setPreferredSize(preferredSize);

        //设置box布局
        Box b1=Box.createHorizontalBox();//创建横向Box容器
        panelBottom.add(b1);
        b1.add(Box.createVerticalStrut(10));
        b1.add(jButtonLeft);
        b1.add(Box.createHorizontalStrut(770));
        b1.add(jButtonRight);//添加按钮2

        //设置按钮字体
        jButtonLeft.setFont(new Font (Font.DIALOG, Font.PLAIN, 25));
        jButtonLeft.setForeground(Color.white);
        jButtonLeft.setBackground(new Color(218,65,71));

        jButtonRight.setFont(new Font (Font.DIALOG, Font.PLAIN, 25));
        jButtonRight.setForeground(Color.white);
        jButtonRight.setBackground(new Color(218,65,71));

        jButtonLeft.setVisible(true);
        jButtonRight.setVisible(true);

        jButtonRight.addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(pageNumber<3) {
                    pageNumber++;
                }
                if(pageNumber==0){
                    cardLayout.show(panelMiddle,"0");
                }else if(pageNumber==1){
                    if (!flag && IDnum.equals("123456")){
                        BookingNumHint.setText("PLEASE ENTER YOUR PASSWORD");
                        JOptionPane.showMessageDialog(null, "Now please enter the" +
                                " administrator password.", "warning", JOptionPane.INFORMATION_MESSAGE);
                        inputArea.setText("");
                        flag=true;
                        IDnum="";
                        pageNumber=0;
                    }else if(flag && IDnum.equals("000000")){
                        inputArea.setText("");
                        IDnum="";
                        flag=false;
                        cardLayout.show(panelMiddle,"1");
                    }else if(!flag){
                        JOptionPane.showMessageDialog(null, "Your account number is entered" +
                                " incorrectly. Please re-enter it.", "warning", JOptionPane.INFORMATION_MESSAGE);
                        inputArea.setText("");
                        IDnum="";
                        pageNumber=0;
                    }else{
                        JOptionPane.showMessageDialog(null, "Your password is entered" +
                                " incorrectly. Please re-enter it.", "warning", JOptionPane.INFORMATION_MESSAGE);
                        inputArea.setText("");
                        IDnum="";
                        pageNumber=0;
                    }

                }else if(pageNumber==2){
                    temp = Admin_Flights_Panel.flight_num;
                    flight f = new flight();
                    if(f.isFlight(temp)==1) {
                        Admin_Information Information = new Admin_Information(temp);
                        panelMiddle.add(Information, "2");
                        cardLayout.show(panelMiddle, "2");
                        jButtonRight.setText("EXIT");
                    }else if(f.isFlight(temp)==0){
                        pageNumber--;
                        JOptionPane.showMessageDialog(null, "Flight is selected incorrectly." +
                                " Please re-enter it.", "warning", JOptionPane.INFORMATION_MESSAGE);

                    }

                }else if(pageNumber==3){
                    System.exit(0);
                }

            }
        });

        jButtonLeft.addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(pageNumber>=0) {
                    pageNumber--;
                }
                if(pageNumber==-1){
                    pageNumber=0;
                    System.exit(0);
                }else if(pageNumber==0) {
                    BookingNumHint.setText("PLEASE ENTER THE ADMINISTRATOR ACCOUNT");
                    cardLayout.show(panelMiddle, "0");
                }else if(pageNumber==1){
                    cardLayout.show(panelMiddle,"1");
                    jButtonRight.setText("CONTINUE");
                }else if(pageNumber==2){
                    cardLayout.show(panelMiddle,"2");
                }

            }
        });


        //设置关闭事件
        frameAdmin.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        frameAdmin.setVisible(true);
    }
}
